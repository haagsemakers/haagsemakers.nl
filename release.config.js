module.exports = {
	branches: [{ name: 'main' }],

	repositoryUrl: 'https://gitlab.com/urbanlink/openpublicatie-frontend',

	debug: true,

	plugins: [
		'@semantic-release/commit-analyzer',
		'@semantic-release/release-notes-generator',
		'@semantic-release/changelog',
		[
			'@semantic-release/npm',
			{
				npmPublish: false,
			},
		],
		[
			'@semantic-release/gitlab',
			{
				gitlabUrl: 'https://gitlab.com/urbanlink/openpublicatie-frontend',
				assets: 'build',
			},
		],
		[
			'@semantic-release/changelog',
			{
				changelogFile: 'CHANGELOG.md',
			},
		],
		[
			'@semantic-release/git',
			{
				assets: ['package.json', 'yarn.lock', 'CHANGELOG.md'],
				message:
					'chore(release): ${nextRelease.version} [skip ci]\n\n${nextRelease.notes}',
			},
		],
	],
};
