const path = require('path'); 

exports.createPages = async ({ actions, graphql, reporter }) => {
  
  const { createPage } = actions

  // Fetch all markdownpages
  const result = await graphql(`
    {
      allMarkdownRemark (
        sort: {fields: frontmatter___date, order: DESC}, 
        filter: { 
          frontmatter: { 
            type: { eq: "workshop" },
            draft: { ne: true }
          }
        }
      ) {
        edges {
          node {
            id
            timeToRead
            excerpt(pruneLength: 80)
            html
            frontmatter {
              title
              path
              date
              excerpt
              intro
              start
              end
              objectives
              locations {
                id
              }
              featuredImage {
                childImageSharp {
                fluid(maxWidth: 2800) {
                   base64
                   tracedSVG
                   aspectRatio
                   src
                   srcSet
                   srcWebp
                   srcSetWebp
                   sizes
                 }
                }
              }
              organizers {
                id
                name
                website
                email
                logo {
                  childImageSharp {
                    fluid(maxWidth: 2800) {
                        base64
                        tracedSVG
                        aspectRatio
                        src
                        srcSet
                        srcWebp
                        srcSetWebp
                        sizes
                      }
                    }
                }
              }
              venue {
                id
                name
              }
              author {
                id
                bio
                twitter
                linkedin
                author_id
              }
            }
          }
        }
      }
    }`
  )

  // Handle errors
  if (result.errors) {
    reporter.panicOnBuild(`Error while running GraphQL query.`)
    return
  }
  
  // Create pages
  const posts = result.data.allMarkdownRemark.edges
  
  const postTemplate = path.resolve(`./src/templates/workshop.js`);

  posts.forEach((post, index) => {
    
    const previous = index === posts.length - 1 ? null : posts[index + 1].node
    const next = index === 0 ? null : posts[index - 1].node
    
    let a = createPage({
      path: post.node.frontmatter.path, 
      component: postTemplate,
      context: {
        id: post.node.id,
        previous, 
        next,
      }
    })
  })
  
  const postsPerPage = 10
  const numPages = Math.ceil(posts.length / postsPerPage)
  
  const pageTemplate = path.resolve('./src/templates/workshop-list-template.js')
  
  Array.from({ length: numPages }).forEach((_, i) => {
    
    const path = i === 0 ? `/workshops` : `/workshops/${i + 1}`; 
    const skip = i * postsPerPage; 
    const limit = postsPerPage; 
    const p = {
      path,
      component: pageTemplate,
      context: {
        limit,
        skip,
        numPages,
        currentPage: i + 1,
      }
    }
    createPage(p); 

  })
}