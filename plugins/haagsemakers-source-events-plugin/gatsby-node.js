const {createPages} = require('./create-pages');

if (process.env.NODE_ENV === 'development') {
  require("dotenv").config({
    path: `.env.${process.env.NODE_ENV}`,
  })
}

exports.createPages = createPages;

// https://www.gatsbyjs.org/packages/gatsby-source-filesystem/#createfilepath
exports.onCreateNode = ({ node, getNode, actions }) => {
  // const { createNodeField } = actions
  // console.log(node.internal.type);
  // if (node.componentChunkName==='component---src-templates-event-js') {
  //   console.log('add event from markdown file ')
  //   createNodeField({
  //     node,
  //     name: 'isFuture',
  //     value: true
  //   })
  // }
}

// // https://github.com/gatsbyjs/gatsby/issues/17159
// // https://www.gatsbyjs.org/docs/schema-customization/ 
// exports.createSchemaCustomization = ({ actions, schema, getNode }) => {
  
//   const { createTypes } = actions
  
//   const typeDefs = [
//     schema.buildObjectType({
//       name: 'MarkdownRemark',
//       interfaces: ['Node'],
//       fields: {
//         isFuture: {
//           type: 'Boolean!',
//           resolve: source => new Date(source.end) > new Date(),
//         },
//       },
//     })
//   ]
//   createTypes(typeDefs)
// }