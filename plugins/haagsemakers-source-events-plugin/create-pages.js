const path = require('path'); 
const moment = require('moment'); 

exports.createPages = async ({ actions, graphql, reporter }) => {
  
  const { createPage } = actions
  
  // Fetch all markdownpages
  const result = await graphql(`
    {
      allMarkdownRemark (
        sort: {fields: frontmatter___start, order: DESC}, 
        filter: { 
          frontmatter: { 
            type: { eq: "event" },
            draft: { ne: true }
          }
        }
      ) {
        edges {
          node {
            id
            timeToRead
            excerpt(pruneLength: 80)
            html
            frontmatter {
              title
              path
              date
              excerpt
              start
              end
              locations {
                id
              }
              featuredImage {
                childImageSharp {
                  fluid(maxWidth: 2800) {
                    base64
                    tracedSVG
                    aspectRatio
                    src
                    srcSet
                    srcWebp
                    srcSetWebp
                    sizes
                  }
                }
              }
              organizers {
                id
                name
                website
                email
                logo {
                  childImageSharp {
                    fluid(maxWidth: 800) {
                      base64
                      tracedSVG
                      aspectRatio
                      src
                      srcSet
                      srcWebp
                      srcSetWebp
                      sizes
                    }
                  }
                }
              }
              locations {
                id
                name
                website
                logo {
                  childImageSharp {
                    fluid(maxHeight: 40) {
                      base64
                      tracedSVG
                      aspectRatio
                      src
                      srcSet
                      srcWebp
                      srcSetWebp
                      sizes
                    }
                  }
                }
              }
              venue {
                id
                name
              }
              author {
                id
                bio
                twitter
                linkedin
                author_id
              }
            }
          }
        }
      }
    }`
  )

  // Handle errors
  if (result.errors) {
    console.warn(result.errors);
    reporter.panicOnBuild(`Error while running GraphQL query.`)
    return
  }
   
  // Create pages
  const posts = result.data.allMarkdownRemark.edges
  const postTemplate = path.resolve(`./src/templates/event.js`);

  posts.forEach((post, index) => {
    
    const previous = index === posts.length - 1 ? null : posts[index + 1].node
    const next = index === 0 ? null : posts[index - 1].node
    
    createPage({
      path: post.node.frontmatter.path, 
      component: postTemplate,
      context: {
        id: post.node.id,
        previous, 
        next
      }
    })
  })
  
  const postsPerPage = 12
  const numPages = Math.ceil(posts.length / postsPerPage)
  const pageTemplate = path.resolve('./src/templates/event-list-template.js')
  
  Array.from({ length: numPages }).forEach((_, i) => {
  
    createPage({
      path: i === 0 ? `/agenda/archief` : `/agenda/archief/${i + 1}`,
      component: pageTemplate,
      context: {
        limit: postsPerPage,
        skip: i * postsPerPage,
        numPages,
        currentPage: i + 1,
      }
    })
  })
}