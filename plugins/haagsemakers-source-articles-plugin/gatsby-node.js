const {createPages} = require('./create-pages');

if (process.env.NODE_ENV === 'development') {
  require("dotenv").config({
    path: `.env.${process.env.NODE_ENV}`,
  })
}

exports.createPages = createPages;
