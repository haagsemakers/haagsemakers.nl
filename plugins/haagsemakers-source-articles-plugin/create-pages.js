const path = require('path'); 

exports.createPages = async ({ actions, graphql, reporter }) => {
  
  const { createPage } = actions

  // Fetch all markdownpages
  const result = await graphql(`
    {
      allMarkdownRemark (
        sort: {fields: frontmatter___date, order: DESC}, 
        filter: { 
          frontmatter: { 
            type: { eq: "article" },
            draft: { ne: true }
          }
        }
      ) {
        edges {
          node {
            id
            timeToRead
            excerpt(pruneLength: 80)
            html
            frontmatter {
              type
              title
              path
              date(
                formatString: "D MMMM YYYY",
                locale:"nl-NL"
              )
              excerpt
              author {
                author_id
                name
                id
              }
            }
          }
        }
      }
    }`
  )

  // Handle errors
  if (result.errors) {
    console.warn(result.errors);
    reporter.panicOnBuild(`Error while running GraphQL query.`)
    return
  }
   
  // Create pages
  const posts = result.data.allMarkdownRemark.edges
  const articlePostTemplate = path.resolve(`./src/templates/article.js`);

  posts.forEach((post, index) => {
    
    const previous = index === posts.length - 1 ? null : posts[index + 1].node
    const next = index === 0 ? null : posts[index - 1].node
    
    createPage({
      path: post.node.frontmatter.path, 
      component: articlePostTemplate,
      context: {
        id: post.node.id,
        previous, 
        next,
      }
    })
  })
  
  const postsPerPage = 12
  const numPages = Math.ceil(posts.length / postsPerPage)
  const articlePageTemplate = path.resolve('./src/templates/article-list-template.js')
  
  Array.from({ length: numPages }).forEach((_, i) => {
  
    createPage({
      path: i === 0 ? `/artikelen` : `/artikelen/${i + 1}`,
      component: articlePageTemplate,
      context: {
        limit: postsPerPage,
        skip: i * postsPerPage,
        numPages,
        currentPage: i + 1,
      }
    })
  })
}