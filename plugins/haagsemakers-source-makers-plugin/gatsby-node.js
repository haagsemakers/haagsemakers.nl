const crypto = require('crypto');
const fetch = require('node-fetch');
const queryString = require('query-string');
const path = require('path');
if (process.env.NODE_ENV === 'development') {
  require("dotenv").config({
    path: `.env.${process.env.NODE_ENV}`,
  })
}

// Define the fields for a maker profile.
exports.createSchemaCustomization = ({ actions }) => {

  const { createTypes } = actions

  const typeDefs = `
    type MakerProfile implements Node {
      internalId: Int
      username: String
      slug: String
      picture: String
      account_plan: String
      bio: String
      firstname: String
      lastname: String
    }
  `
  createTypes(typeDefs)
}

// Fetch the maker profiles from the API
exports.sourceNodes = ({ actions, createNodeId }, configOptions) => {
  console.log('source makers plugin - sourceNodes');
  const { createNode } = actions;

  // Gatsby adds a configOption that's not needed for this plugin, delete it
  delete configOptions.plugins;

  // Process the events
  const processMakerProfile = makerProfile => {
    makerProfile.internalId = makerProfile.id
    const nodeId = createNodeId(`makerprofile-${makerProfile.id}`)
    const nodeContent = JSON.stringify(makerProfile)
    const nodeContentDigest = crypto
     .createHash('md5')
     .update(nodeContent)
     .digest('hex')

    const nodeData = Object.assign({}, makerProfile, {
     id: nodeId,
     parent: null,
     children: [],
     internal: {
       type: `MakerProfile`,
       content: nodeContent,
       contentDigest: nodeContentDigest,
     },
    })

    return nodeData
  }
  // Convert the options object into a query string
  const apiOptions = queryString.stringify(configOptions)

  // Join apiOptions with the demo.onehippo API URL
  const apiUrl = `${process.env.GATSBY_HAAGSEMAKERS_API_URL}/user`;

  // Gatsby expects sourceNodes to return a promise
  return (
    // Fetch a response from the apiUrl
    fetch(apiUrl)
      // Parse the response as JSON
      .then(response => response.json())
      // Process the JSON data into a node
      .then(data => {

        data.forEach(makerProfile => {

          const nodeData = processMakerProfile(makerProfile)
          // Use Gatsby's createNode helper to create a node from the node data
          createNode(nodeData)
        })
      }).catch(err => {
        console.warn('data fetch error', err);
      })
    )
};

exports.createPages = async ({ actions, graphql}) => {
  console.log('source makers plugin - createPages');
  const { createPage } = actions

  const result = await graphql(`{
    allMakerProfile {
      edges {
        node {
          id
          internalId
          username
          slug
          picture
          account_plan
          bio
          firstname
          lastname
        }
      }
    }
  }`);

  // Handle errors
  if (result.errors) {
    console.warn(result.errors);
   return
  }

  const eventTemplate = path.resolve(`./src/templates/profile.js`)
  const events = result.data.allMakerProfile.edges

  // Iterate over the array of posts
  result.data.allMakerProfile.edges.forEach(({ node }) => {

    createPage({
      path: `/${node.slug}/`,
      component: eventTemplate,
      context: {
        id: node.id,
      }
    })
  })
}
