
var fs = require('fs');
const rimraf = require('rimraf');
const makeDir = require('make-dir');
const request = require('request');
const xml2js = require('xml2js');
const parser = new xml2js.Parser();

class Processattachments {

  filePath = './data/haagsemakers.WordPress.2019-09-30.xml';
  outPath = './out/attachments.json';

  posts = [];
  attachments = [];

  constructor() {
    console.log('process attachments');
    this.processFile();
  }

  processFile = async () => {
    let fileData;
    try {
      fileData = fs.readFileSync(this.filePath)
    } catch(err) {
      console.warn(err);
    }

    console.log('Starting to parse data');
    try {
      let parseResult = await parser.parseStringPromise(fileData);
      this.posts = parseResult.rss.channel[0].item;
      console.log(`Found ${this.posts.length} posts`);
    } catch (err) {
      console.warn('Error parsing xml: ', err);
    }

    await this.parseAttachments();
    // console.log(this.attachments);

    await this.writeFile();
    console.log('done writing file');
  }

  parseAttachments = async () => {

    return new Promise((resolve, reject) => {

      this.posts.forEach((post, key) => {
        const postType = post['wp:post_type'][ 0];

        if (postType === 'attachment') {
          this.attachments.push({
            wp_id: post['wp:post_id'][ 0],
            src: post['wp:attachment_url'][ 0]
          })
        }
      })

      console.log('done parsing attachments')
      return resolve();
    });
  }

  writeFile = async () => {
    try {
      fs.writeFileSync(this.outPath, JSON.stringify(this.attachments, null, 2))
    } catch (err) {
      console.warn(err);
    }
    return;
  }
}

new Processattachments();
