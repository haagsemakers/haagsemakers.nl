
var fs = require('fs');
const rimraf = require('rimraf');
const makeDir = require('make-dir');
const request = require('request');
const xml2js = require('xml2js');
const parser = new xml2js.Parser();

class ProcessOrganizers {

  filePath = './data/haagsemakers.WordPress.2019-09-30.xml';
  outPath = './out/organizations.json';

  posts = [];
  organizers = [];

  constructor() {
    console.log('process organizers');
    this.processFile();
  }

  processFile = async () => {
    let fileData;
    try {
      fileData = fs.readFileSync(this.filePath)
    } catch(err) {
      console.warn(err);
    }

    console.log('Starting to parse data');
    try {
      let parseResult = await parser.parseStringPromise(fileData);
      this.posts = parseResult.rss.channel[0].item;
      console.log(`Found ${this.posts.length} posts`);
    } catch (err) {
      console.warn('Error parsing xml: ', err);
    }

    await this.parseOrganizers();
    console.log(`${this.organizers.length} processed`);

    await this.writeFile();
    console.log('done writing file');
  }

  parseOrganizers = async () => {

    return new Promise((resolve, reject) => {

      this.posts.forEach((post, key) => {
        const postType = post['wp:post_type'][ 0];

        if (postType === 'tribe_organizer') {

          let item = {
            organization_id: post['wp:post_name'][ 0].replace(/-/g,'_'),
            name: post.title[ 0]
          };

          post['wp:postmeta'].forEach((value, key) => {

            const metakey = value['wp:meta_key'][ 0];
            const metavalue = value['wp:meta_value'][ 0]

            if ( (metakey === '_OrganizerWebsite') && (metavalue)) {
              item.website = metavalue;
            } else if ( (metakey === '_OrganizerEmail') && (metavalue)) {
              item.email = metavalue;
            } else if ( (metakey === '_OrganizerPhone') && (metavalue)) {
              item.phone = metavalue;
            }
          });

          item.wp_id = post['wp:post_id'][ 0];

          this.organizers.push(item);
        }
      })

      console.log('done parsing organizers')
      return resolve();
    });
  }

  writeFile = async () => {
    try {
      fs.writeFileSync(this.outPath, JSON.stringify(this.organizers, null, 2))
    } catch (err) {
      console.warn(err);
    }
    return;
  }
}

new ProcessOrganizers();
