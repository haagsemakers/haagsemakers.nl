const TurndownService = require('turndown');
const async = require('async');
const xml2js = require('xml2js');
const fs = require('fs');
const rimraf = require('rimraf');
const makeDir = require('make-dir');
const request = require('request');
const moment = require('moment');
const slugify = require('@sindresorhus/slugify');

const turndownOptions = {}
const turndownService = new TurndownService(turndownOptions);
const parser = new xml2js.Parser();

const attachments = require('./../out/attachments');
const locations = require('./../out/locations');
const organizers = require('./../out/organizations');

class ProcessEvents {

  filePath = './data/haagsemakers.WordPress.2019-09-30.xml';
  outPath = './out/events/';

  posts = [];
  events = [];

  constructor() {
    rimraf.sync('./out/events');
    this.processFile();
  }

  //
  processFile = async () => {
    let fileData;
    try {
      fileData = fs.readFileSync(this.filePath)
    } catch(err) {
      console.warn(err);
      return;
    }

    console.log('Starting to parse data');
    try {
      let parseResult = await parser.parseStringPromise(fileData);
      this.posts = parseResult.rss.channel[0].item;
      console.log(`Found ${this.posts.length} posts`);
    } catch (err) {
      console.warn('Error parsing xml: ', err);
      return;
    }

    await this.parseEvents();
    console.log(`${this.events.length} processed`);
  }

  //
  parseEvents = async () => {

    return new Promise(async (resolve, reject) => {

      await this.asyncForEach(this.posts, async (post) => {

        const postType = post['wp:post_type'][ 0];

        if (postType === 'tribe_events') {

          let postName = post['wp:post_name'][ 0];
          if (!postName) { postName = slugify(post.title[ 0]); }

          let item = {
            frontmatter: {
              layout: 'event',
              title: post.title[ 0],
              path: `/event/${postName}`
            },
            published: new Date(post['wp:post_date'][ 0]),
            postData: post['content:encoded'][0]
          }

          // Merge categories and tags into tags
        	if (post.category !== undefined) {
            post.category.forEach((value, key) => {
              let cat = value[ '_'];
        			if (cat !== 'Uncategorized') {
                if (!item.frontmatter.categories) {
                  item.frontmatter.categories = [];
                }
        				item.frontmatter.categories.push(cat);
              }
        		})
        	}

          // handle post metadata
          post['wp:postmeta'].forEach((value, key) => {

            const metakey = value['wp:meta_key'][ 0];
            const metavalue = value['wp:meta_value'][ 0];

            if ( ( metakey === '_EventStartDate') && metavalue) { item.frontmatter.start = metavalue; }
            else if ( ( metakey === '_EventEndDate') && metavalue) { item.frontmatter.end = metavalue; }
            else if ( ( metakey === '_EventURL') && metavalue) { item.frontmatter.event_url = metavalue; }

            else if ( ( metakey === '_EventOrganizerID') && metavalue) {
              if (!item.frontmatter.organizers) {
                item.frontmatter.organizers = []
              }
              item.frontmatter.organizers.push(metavalue);
            }

            else if ( ( metakey === '_EventVenueID') && metavalue) {
              if (!item.frontmatter.locations) {
                item.frontmatter.locations = [];
              }
              item.frontmatter.locations.push(metavalue);
            }

            else if ( ( metakey === '_thumbnail_id') && metavalue ) {
              item.frontmatter.featuredImage = metavalue;
            }

          })

          if (!item.frontmatter.start) {
            // console.log('no start date: ', item)
            return;
          }
          if (!item.frontmatter.end) {
            item.frontmatter.end = moment(item.frontmatter.start, 'YYYY-MM-DD HH:mm').add('2','hours').format('YYYY-MM-DD HH:mm');
          }
          // Create path based on startdate
          const filePath = './out/events/' + moment(item.frontmatter.start).format('YYYY') + '/' + moment(item.frontmatter.start).format('MM') + '/' + moment(item.frontmatter.start).format('DD') + '/' + postName;

          const fullPath = await makeDir(filePath);

          // Handle featured image
          if (item.frontmatter.featuredImage) {
            try {
              const featuredImageResult = await this.handleFeaturedImage(item.frontmatter.featuredImage, fullPath);
              // console.log('Featured image result: ', item.frontmatter.featuredImage, featuredImageResult)
              if (featuredImageResult) {
                // console.log('Adding featured image to frontmatter');
                item.frontmatter.featuredImage = featuredImageResult;
              }
            } catch(err) {
              console.warn(err);
            }
          }

          // Fetch organizer
          if (item.frontmatter.organizers) {
            item.frontmatter.organizers = await this.handleOrganizers(item.frontmatter.organizers);
          }
          // Fetch locations
          if (item.frontmatter.locations) {
            item.frontmatter.locations = this.handleLocations(item.frontmatter.locations)
          }
          // rewrite images in postdata
          if (item.postData) {
            item.postData = await this.handlePostdataImages(item.postData, fullPath);
          }

          const header = this.createMarkdownHeader(item);
          let markdown = '';
          if (item.postData) { markdown = turndownService.turndown(item.postData); }

          console.log('events header', header);

          try {
            await fs.writeFileSync(fullPath + '/index.md', header + markdown);
          } catch(err) {
            console.warn(err);
          }

        }
      })

      return resolve();
    })
  }

  //
  handleFeaturedImage = async (imgId, filePath) => {
    if (!imgId) { return null; }
    // find the attachment based on the featuredImage
    const attachment = attachments.find(attachment => attachment.wp_id === imgId);
    // create the path for the file
    if (attachment) {
      let urlParts = attachment.src.split('/');
      let imageName = urlParts[urlParts.length - 1];
      let imageFragments = imageName.split('.')
      if (imageFragments.length > 1) {
        let extension = imageFragments[ imageFragments.length -1];
        let basename = imageFragments[ imageFragments.length -2];
        basename = slugify(basename);
        imageName = basename + '.' + extension;
      }
      filePath = filePath + '/' + imageName;
      // fetch the image from wordpress source
      try {
        await this.downloadFile(attachment.src, filePath);
        return(imageName);
      } catch(err) {
        console.warn(err);
        return(null);
      }
    }
  }

  //
  handleOrganizers = async (items) => {
    if (!items) { return null; }
    items.forEach( (value, key) => {
      const o = organizers.find(item => item.wp_id == value);
      if (o) { items[ key] = o.organization_id; }
    })
    return items;
  }

  //
  handleLocations = (items) =>  {
    if (!items) { return null; }
    items.forEach((value, key) => {
      const l = locations.find(item => item.wp_id === value);
      if (l) { items[ key] = l.location_id; }
    })
    return items;
  }

  // Find all images
  handlePostdataImages = async (postData, filePath) => {

    if (!postData || !filePath) { return null; }

    let patt = new RegExp("(?:src=\"(.*?)\")", "gi");

    let m;
    let matches = [];
    while((m = patt.exec(postData)) !== null) {
    	matches.push(m[1]);
    }

    if (matches != null && matches.length > 0) {

      await this.asyncForEach(matches, async (url) => {

        url = url.replace('https://www.haagsemakers.nl/', 'http://old.haagsemakers.nl/')
        url = url.replace('http://haagsemakers.nl/', 'http://old.haagsemakers.nl/')

        let urlParts = url.split('/');
        let imageName = urlParts[urlParts.length - 1];
        let imageFragments = imageName.split('.')
        if (imageFragments.length >1) {
          let extension = imageFragments[ imageFragments.length -1];
          let basename = imageFragments[ imageFragments.length -2];
          basename = slugify(basename);
          imageName = basename + '.' + extension;
        }
        let currentFilePath = filePath + '/' + imageName;

        try {
          await this.downloadFile(url, currentFilePath);
          // Make the image name local relative in the markdown
          postData = postData.replace(url, imageName);
        } catch(err) {
          console.warn('err', err);
        }
      })
    }

    return postData;
  }

  //
  createMarkdownHeader = (item) => {

  	var header = ``;
  	header += `---\n`;
  	header += `layout: ${ item.frontmatter.layout }\n`;
    header += `path: ${ item.frontmatter.path }\n`;
  	header += `title: '${ item.frontmatter.title }'\n`;
  	header += `date: ${ moment(item.frontmatter.published).format('YYYY-MM-DD') }\n`;

    if (item.frontmatter.categories && (item.frontmatter.categories.length > 0)) {
  		header += `tags: ${ JSON.stringify(item.frontmatter.categories) }\n`;
    }
    header += `start: ${ item.frontmatter.start } \n`;
    header += `end: ${ item.frontmatter.end } \n`;
    if (item.frontmatter.event_url) {
      header += `event_url: ${ item.frontmatter.event_url } \n`;
    }
    if (item.frontmatter.organizers && (item.frontmatter.organizers.length > 0)) {
  		header += `organizers: ${ JSON.stringify(item.frontmatter.organizers) }\n`;
    }
    if (item.frontmatter.locations && (item.frontmatter.locations.length > 0)) {
  		header += `locations: ${ JSON.stringify(item.frontmatter.locations) }\n`;
    }
    if (item.frontmatter.featuredImage) {
  		header += `featuredImage: ${ item.frontmatter.featuredImage }\n`;
    }
  	header += `---\n`;
  	header += `\n`;

    return header;
  }

  //
  slugifyFilename = (filname) => {
    let fragments = filename.split('.');
    let result = null;
    if (fragments.length > 1) {
      let base = slugify(fragments[ fragments.length -2]);
      let ext = fragments[ fragments.length -1];
      result = base + '.' + ext;
    }
    return result;
  }

  //
  downloadFile = async (url, path) => {

    return new Promise((resolve, reject) => {

      if ( url.indexOf(".jpg") >=0 || url.indexOf(".jpeg") >=0 || url.indexOf(".png") >=0 || url.indexOf(".png") >=0) {

        const file = fs.createWriteStream(path);
        const sendReq = request.get(url);
        // verify response code
        sendReq.on('response', (response) => {
          if (response.statusCode !== 200) {
            // console.log(url);
            // console.warn('Response status was ', response.statusCode);
            return reject(response.statusCode);
          }

          sendReq.pipe(file);
        });

        // close() is async, call cb after close completes
        file.on('finish', () => {
          // console.log('file download finished', url);
          file.close(() => {
            // console.log('File download closed');
            return resolve();
          });
        });

        // check for request errors
        sendReq.on('error', (err) => {
          fs.unlink(path);
          // console.warn(err)
          return reject(err.message);
        });

        file.on('error', (err) => { // Handle errors
          fs.unlink(path); // Delete the file async. (But we don't check the result)
          // console.warn(url, err.message);
          return reject(err.message);
        });

        // request(url)
        //   .pipe(fs.createWriteStream(path))
        //   .on('close', function() {
        //     return resolve();
        //   })
        //   .on('error', (err) => {
        //     console.warn('There was an error downloading the file', url, err);
        //     return reject(err);
        //   });
       } else {
         return reject(url + ' is not an image file');
       }
    });
  }

  //
  asyncForEach = async (array, callback) => {
    for (let index = 0; index < array.length; index++) {
      await callback(array[index], index, array);
    }
  }
}

new ProcessEvents();
