---
type: event
path: /event/awesome-the-hague-pitch-night-5
title: 'Awesome The Hague Pitch Night #5'
date: 2019-10-01
tags: ["Events","Haagse Makers"]
start: 2017-07-17 19:00:00 
end: 2017-07-17 21:00:00 
organizers: ["haagse_makers"]
featuredImage: awesome-header.png
---

HEB JIJ EEN TE GEK IDEE OM DEN HAAG NÓG MEER AWESOME TE MAKEN? Iedere maand geven we iemand een donatie van €1000 als bijdrage om een te gek en briljant idee te realizeren voor onze stad. Let's make The Hague even more Awesome! Present your idea and win €1000,- in cash, no strings attached! De registratie is geopend! [http://www.awesomefoundation.org/en/submissions/new?chapter=thehague](http://www.awesomefoundation.org/en/submissions/new?chapter=thehague) Vind je het leuk om awesome ideeën te ondersteunen? Er zijn nog een paar plekken vrij voor Trustees, meld je meteen aan via https:/haagsemakers.nl/awesome Voor meer informatie, check [https://haagsemakers.nl/awesome](https://haagsemakers.nl/awesome)