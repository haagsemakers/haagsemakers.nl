---
type: event
path: /event/all-about-food-festival-transitiecinema/
title: 'All about food Festival: TransitieCinema'
date: 2019-10-01
tags: ["food"]
start: 2019-10-04 20:00:00
end: 2019-10-04 22:00:00
event_url: https://nutshuis.nl/events/all-about-food-festival-transitiecinema/
organizers: ["het_nutshuis", "juni_cafe", "impact_kitchen", "duurzaam_den_haag", "transitie_cinema", "conscious_kitchen"]
venue: het_nutshuis
featuredImage: TCsoloenhappy.png
---

Een veganistische maaltijd van Juni Café, een vertoning van een in het oog springende documentaire en een Q&A met gasten. Dat is het vertrouwde TransitieCinema-concept waarvan de bezoeker ook tijdens het All About Food Festival kan genieten.

In de film Just Eat it: A Food Waste Story (2014) eet het Canadese filmmakerskoppel Rustemeyer en Baldwin een half jaar lang voedsel dat anders zou worden weggegooid – met alle schokkende en (grafisch vormgegeven) inzichten als resultaat.

### Over het All About Food Festival
De gemiddelde Nederlander gooit vijftig kilo goed voedsel per jaar weg en is daarmee een van de grootste voedselverspillers van de wereld. Hoewel iedereen het ermee eens zal zijn dat dit moet veranderen, zit het probleem natuurlijk niet alleen bij de consument. In alle schakels van grond tot en met gft-afval gaat enorm veel voedsel verloren.

In de eerste week van oktober staat het All About Food Festival in Het Nutshuis helemaal in het teken van voedselverspilling. Aan het woord komen duurzame en sociale ondernemers en Haagse pioniers die het tij succesvol proberen te keren. Op het filmscherm leven mensen een half jaar van voedselafval. In de tuin gaan kinderen op ontdekkingsreis met hun stempelkaart. Aan tafel serveren we ‘geredde gerechten’ waarvan ook jíj (blind) kunt proeven. Praat, kijk, denk en eet je mee?

_Het All About Food Festival is een samenwerking tussen Het Nutshuis, Juni Café, Impact Kitchen, Duurzaam Den Haag, TransitieCinema en Conscious Kitchen._
