---
type: event
path: /event/social-breakfast-club-20191110
title: Social Breakfast Club
date: 2019-10-08
tags: ["social"]
start: 2019-10-11 07:45:00
end: 2019-10-11 10:00:00
event_url: https://www.eventbrite.co.uk/e/social-breakfast-club-tickets-74401423661
organizers: ["social_business_club"]
venue: het_koorenhuis
featuredImage: 53684491_190118462188_1_original.jpg
---

11 oktober: tijd voor Social Breakfast Club, een reeks ontbijtsessies vol inspiratie, nieuwe contacten en gezelligheid. Met Social Breakfast Club heb je gegarandeerd een energieke start van de dag.

Social Breakfast Club vindt eens per 2 maanden op de tweede vrijdag van die maand plaats. [Social Club Den Haag](https://socialclubdenhaag.nl/) en [In het Korenhuis](https://inhetkoorenhuis.nl/) organiseren deze ontbijtsessies om sociale en impact gedreven ondernemers te inspireren en te helpen. Onder het genot van een heerlijk ontbijt bieden wij jou een inspirerende spreker en verschillende tafels, waarbij je zelf kiest waar je aanschuift in het Korenhuis.

Breakfastclub op vrijdag 11 oktober staat in het teken van de PSO 30+. De Prestatieladder Socialer Ondernemen (PSO) van TNO maakt sinds 2012 zichtbaar wat een organisatie bijdraagt aan de arbeidsparticipatie van kwetsbare groepen. De PSO is een meetinstrument en keurmerk dat objectief bepaalt in welke mate een organisatie een sociale werkgever is (gericht op arbeidsparticipatie). De gemeente Den Haag gaat samen met Rotterdam de PSO30+ gebruiken als officieel instrument om sociale ondernemingen te (h)erkennen. Kortom, er liggen veel nieuwe kansen voor sociale ondernemingen die het PSO30+ certificaat kunnen krijgen.

Onze spreker op 11 oktober is [Annelies Goedbloed](https://haagsezwam.nl/over-ons/) van [Haagse Zwam](https://haagsezwam.nl/). Annelies startte in 2016 de Haagse Zwam, een circulaire urban oesterzwamkwekerij in Den Haag. Op koffiedik worden in het oude philipsgebouw oesterzwammen gekweekt. Zo is er geen restafval en oesterzwammen zijn goede vleesvervangers vol proteïnen. Annelies zet mensen in die afstand hebben tot de arbeidsmarkt. Zo geeft ze hen de mogelijkheid om weer ritme en werkervaring op te doen. Daarom koos ze ervoor om de PSO 30+ te implementeren, dit proces gaat ze uitleggen.

Wil je meer leren van onze spreker? Schuif dan na zijn verhaal aan bij de **Thema-tafel**.

Of heb je een idee dat je graag wilt testen of een uitdaging waar jij als maatschappelijk betrokken ondernemer mee worstelt? Of help je anderen graag verder? Dan is de tafel **Durf te vragen** wat voor jou.

Heb je gewoon zin om even bij te praten met gelijkgestemden, dan kan dat ook.

Elk half uur kun je gewoon wisselen van tafel(genoten) zodat je nooit om gespreksstof verlegen zit en je op een laagdrempelige manier met nieuwe mensen in contact komt. Kortom, er is voor elk wat wils.

Ben jij er ook bij?

  * Locatie: In Het Koorenhuis, Prinsegracht 27, Den Haag
  * Kosten: € 7,50 voor leden (1 kaart per lidmaatschap), € 12,50 voor niet-leden

**Meld jouw uitdaging of vraag voor de "Durf te vragen"-tafel aan via info@socialclubdenhaag.nl. Laat het ons ook weten als jij een inspirerend verhaal te vertellen hebt en je een keer wilt spreken bij de "thema-tafel".**

_Er zullen tijdens het event foto's gemaakt worden voor op onze social media-kanalen. Laat het ons van tevoren even weten als jij niet gefotografeerd wilt worden._

INLOOP VANAF 7:45u. START PROGRAMMA: 8:00u. ZORG DAT JE OP TIJD BINNEN BENT!
