---
type: event
path: /event/all-about-food-festival-change-of-waste
title: 'All about food Festival: Change of waste'
date: 2019-10-01
tags: ["food"]
start: 2019-10-03 20:00:00
end: 2019-10-03 22:00:00
event_url: https://nutshuis.nl/events/all-about-food-festival-change-of-waste/
organizers: ["het_nutshuis", "juni_cafe", "impact_kitchen", "duurzaam_den_haag", "transitie_cinema", "conscious_kitchen"]
venue: het_nutshuis
featuredImage: all_about_food_web_v2-1.jpg
---

_“Hagenaar zamelt slechts tien kilo gft-afval in.”_ Dit stond vorig jaar geschreven in de kranten die vervolgens vast door velen met groenteresten en fruitschillen erin werden weggegooid. De gemiddelde Nederlander buiten Den Haag zamelt 86 kilo in. Hoog tijd dat Den Haag een inhaalslag maakt! Op donderdagavond 3 oktober komen Haagse beleid- en gangmakers samen in Het Nutshuis in Den Haag om hun vruchtbare plannen te pitchen en jóu in beweging te brengen om bewuster om te gaan met je organische afval. Tussen de voedzame tips, verhalen en filmpjes door valt er ook veel te proeven.

## Programma Change of Waste
Masterstudenten **Rombout Huisman** en **Cis Huinink** van de studie Industriële Ecologie aan de **TU Delft** onderzochten samen met vier medestudenten de beste manier om gft-afval te scheiden in een stedelijke omgeving. Zij keken naar Leiden, in opdracht van de stad zelf. Ter lering en inspiratie voor Den Haag delen zij de belangrijkste kansen en tips.

In hoeverre deelt Den Haag de ambities van Leiden, welke tips uit het TU-onderzoek kan de stad meenemen en welke stappen worden op dit moment gezet? Antwoorden op die vragen zal **Annet Beltman**, Contractbeheerder huishoudelijk afval van de gemeente Den Haag, komen geven.

Natuurlijk is de rol van de gemeente onmisbaar in dit verhaal. Maar de burger kan ook zeker zelf in actie komen en een bijdrage leveren aan een circulaire stad. Dat laten **Mark van Duijn**, **Kitty Hinkenkemper** en **Menno Swaak** zien. Mark van Duijn, die deel uitmaakt van de top-100 Duurzame Ondernemers, draagt met zijn initiatief Smaak voor Groen bij aan een groener Den Haag. In ‘Change of Waste’ trakteert hij het publiek op een spoedworkshop Bokashi, de techniek waarmee je keukenresten kunt fermenteren. Kitty Hinkenkemper (Den Haag in Transitie) en Menno Swaak (Permacultuur Den Haag) laten de wonderlijke werking van wormencompost en -hotels zien en de manieren waarop je hiermee ook thuis aan de slag kunt.

Fermentatietechnieken kunnen ook weer leiden tot het heerlijkste eten. Tussen de voedzame verhalen en inspirerende tips door schotelt **Juni Café** het publiek Kimchi-hapjes en een gefermenteerd drankje van Instock voor. Als we het over circulariteit en eten hebben, mag Haagse Zwam niet ontbreken. Terwijl de bezoekers hun tanden zetten in de Haagse Zwam-bitterbal, zal oprichter **Annelies Goedbloed** vertellen over haar succesvolle kweekproducten op koffiedik.

Vooraf en na afloop van het programma draait de **Afvalbioscoop**. In deze ludieke bioscoop – een rolcontainer met beeldscherm en kliko’s met ingebouwde speakers – maakt de gemeente op een beeldende manier duidelijk hoe afvalscheiding en -hergebruik op dit moment zijn georganiseerd.

### Over het All About Food Festival
De gemiddelde Nederlander gooit vijftig kilo goed voedsel per jaar weg en is daarmee een van de grootste voedselverspillers van de wereld. Hoewel iedereen het ermee eens zal zijn dat dit moet veranderen, zit het probleem natuurlijk niet alleen bij de consument. In alle schakels van grond tot en met gft-afval gaat enorm veel voedsel verloren.

In de eerste week van oktober staat het All About Food Festival in Het Nutshuis helemaal in het teken van voedselverspilling. Aan het woord komen duurzame en sociale ondernemers en Haagse pioniers die het tij succesvol proberen te keren. Op het filmscherm leven mensen een half jaar van voedselafval. In de tuin gaan kinderen op ontdekkingsreis met hun stempelkaart. Aan tafel serveren we ‘geredde gerechten’ waarvan ook jíj (blind) kunt proeven. Praat, kijk, denk en eet je mee?

_Het All About Food Festival is een samenwerking tussen Het Nutshuis, Juni Café, Impact Kitchen, Duurzaam Den Haag, TransitieCinema en Conscious Kitchen._
