---
type: event
path: /event/how-to-start-lean-and-fund-lean
title: How to start lean and fund lean
date: 2019-10-04
tags: [ "startup" ]
start: 2019-10-18 14:00:00
end: 2019-10-18 17:30:00
event_url: https://www.eventbrite.co.uk/e/how-to-start-lean-and-fund-lean-tickets-71669594687
organizers: ["start_lean_funding"]
featuredImage: leanfunding.jpg
---

How to start lean and fund lean - What do you need to do before going to the crowd

## About this Event
The way we start initiatives and business has changed, we are starting lean. During this workshop you will learn why and how to fund lean before going to the crowd.

Increase the chance of succes, reduce the consequences of failures and stay in control of your business.

This session will use examples from practice, e.g. domotica.
