---
type: event
path: /event/goed-bezig-dh-festival
title: Goed Bezig DH Festival
date: 2019-10-09
tags: [ "duurzaam" ]
start: 2019-10-13 13:00:00
end: 2019-10-13 16:30:00
event_url: https://www.denhaag.nl/nl/in-de-stad/stadsdelen/escamp/activiteitenkalender-stadsdeel-escamp/goed-bezig-dh-festival.htm
organizers: ["gemeente_den_haag"]
venue: stadsboerderij_herweijerhoeve
featuredImage: 5471340f-1306-4363-b2d4-be9bd1b5f6b0_image6349930471376286466.png
---

Met het #Goed Bezig DH Festival viert Natuur- en Milieueducatie haar 100-jarig bestaan. Op zondag 13 oktober 2019 ontdekt u hoe u zelf iets goeds kunt doen voor de natuur. Dat is vaak makkelijker dan u denkt.

Op het #Goed Bezig DH Festival kunt u ontdekken hoe leuk het is om goed bezig te zijn voor de natuur en het milieu.

Tijdens het festival zijn er verschillende activiteiten voor jong en oud zoals:

  * zelf compost leren maken
  * een wildplukwandeling
  * kennis maken met alternatieve manieren van eten
  * kennis maken met nieuwe energiebronnen
  * een rondje lopen door het Zuiderpark met de afvalbok
  * de bekendmaking van de winnaar van de [pompoenwedstrijd](https://stadslandbouwdenhaag.nl/index.php/pompoenwedstrijd/)

Ook kunt u tijdens het festival met de gemeente in gesprek gaan over het nieuwe beleid voor bomen en natuur in de stad of meedoen aan het straatspel.

Naast het festival organiseert de gemeente ook wandelingen langs Haagse bomen met deskundigen die zich iedere dag met bomen bezig houden. U kunt zich nu al opgeven. Kijk voor [www.hethaagsegroen.nl](https://www.hethaagsegroen.nl/default.aspx) voor data en tijden.

Voor wie: Hagenaars (jong en oud) met interesse in natuur en milieu.

Toegang Gratis

Waar: Stadsboerderij De Herweijerhoeve, Anna Polakweg 7.
