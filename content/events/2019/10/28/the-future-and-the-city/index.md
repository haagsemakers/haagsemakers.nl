---
type: event
path: /event/the-future-and-the-city-kick-off-speculative-futures-the-hague
title: The Future and The City. Kick-off Speculative Futures The Hague
date: 2019-10-10
tags: ["food"]
start: 2019-10-28 19:30:00
end: 2019-10-28 22:00:00
event_url: https://www.meetup.com/nl-NL/Speculative-Futures-The-Hague/events/265450070/
organizers: ["speculative_futures_the_hague"]
venue: kivi
featuredImage: future.png
---

We are delighted to announce that the kickoff of these series of meetups will take place on October 28th, 2019 from 19.30h to 22h!

This meetup will revolve around “The Future and the City”. The arrival of 5G networks is imminent, and it promises to change city landscapes in many ways. This is leading to heated debates and opinions!

During this meetup, we want to discuss the future of the city and the impact of technology on our lives. We will have interesting speakers presenting the opportunities that 5G brings, as well as the potential controversies associated with wireless technology in the city. All of it, with a discussion about the role of speculation and future thinking in this exciting area.

Come and join us! This event is supported by the Stichting Toekomstbeeld der Techniek (STT) and it will be hosted at the KIVI (Koninklijk Instituut van Ingenieurs).

Address: Prinsessegracht 23, 2514 AP Den Haag.

Interested in articles about the city and 5G? Have a look:

- The New Yorker about 5g: https://www.newyorker.com/news/annals-of-communications/the-terrifying-potential-of-the-5g-network

- The Hague and 5G: https://www.trouw.nl/binnenland/5g-is-vergelijkbaar-met-wifi-toch-is-er-angst-en-protest~b82b1c5e0/
