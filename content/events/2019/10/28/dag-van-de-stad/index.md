---
type: event
path: /event/dag-van-de-stad-2019
title: Dag van de Stad 2019
date: 2019-10-10
tags: ["stedelijke-ontwikkeling"]
start: 2019-10-28 10:00:00
end: 2019-10-28 18:00:00
event_url: https://www.dedagvandestad.nl/
organizers: [ "agenda_stad", "g40", "g4", "ipo", "netwerk_kennissteden", "platform31", "rijksoverheid", "vng", "gemeente_den_haag" ]
venue: world_forum
featuredImage: stad-denhaag.jpg
---

De stad lééft en staat prominent op de agenda. Ook bij jou? Kom dan naar de Dag van de Stad 2019 in Den Haag. Tijdens dit jaarlijkse evenement komen alle stedelijke professionals samen voor inspiratie en kennisuitwisseling voor economisch sterke, duurzame, toekomstbestendige én leefbare steden.

Onder de titel 'Wie durft?!' geven we het podium aan moedige ondernemers, architecten, theatermakers, wetenschappers, vertegenwoordigers van maatschappelijke organisaties, actieve inwoners én bestuurders. Wat al deze mensen bindt is hun lef, cojones en bravoure. De Dag van de Stad biedt een warm welkom aan stedelijke visionairs die verder durven kijken dan hun neus lang is. Aan mensen die vastgeroeste denkpatronen loswrikken, met een boog om ingesleten olifantenpaden heenlopen en juist nieuwe routes uitstippelen. Het tonen van lef is ook durven staan voor jezelf in moeilijke omstandigheden, bijvoorbeeld als je wordt bedreigd, geïntimideerd of bespot. Hoe blijf je dan koersvast als professional én als mens?
