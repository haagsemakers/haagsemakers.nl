---
type: event
path: /event/paagman-dennis-storm
title: Dennis Storm bij Paagman
date: 2019-10-09
tags: ["food"]
start: 2019-10-31 19:00:00
end: 2019-10-31 21:00:00
event_url: https://www.paagman.nl/dennis-storm
organizers: ["paagman"]
venue: paagman_centrum
featuredImage: dennis-storm-paagman.jpg
---
Op donderdag 31 oktober is Dennis Storm om 19:00 uur te gast bij Paagman, vestiging Centrum (Lange Poten 41, Den Haag). Hij zal naar aanleiding van zijn boek DIT. Do It Together geïnterviewd worden. Aansluitend zal hij signeren.

DIT.
Met het succesvolle Weg ermee heeft Dennis Storm duizenden geïnspireerd minimalistisch te leven. Nu neemt Dennis Storm ons mee in het volgende avontuur, het creëren van je eigen omgeving met stukken die je tijd en aandacht waard zijn. We kunnen veel meer dan we denken als we er maar tijd voor maken. Bovendien… we hoeven niet alles zelf te kunnen. Waarom Do it yourself als je juist kunt samenwerken met anderen? Do it together! Op Bali heeft Dennis Storm 22 meubels en items gemaakt door samenwerkingen te zoeken met lokale hobbyisten en vakmannen. Sommige projecten in het boek zijn laagdrempelig (DIY), voor andere zijn meer dan twee handen nodig (DIT). Maar alles is uitvoerbaar als je er maar tijd en ruimte voor maakt en als je aanklopt bij mensen die een vaardigheid beheersen die jij misschien (nog) niet in je vingers hebt. En het leuke is, dat kan jij met dit boek zelf thuis ook. Een creatieve wereld gaat voor je open zodra je niet enkel je eigen talenten maar ook die van je buren, vrienden en familie aanspreekt.

Dennis Storm
Dennis Storm (1985) was jarenlang programmamaker en presentator van verschillende reisprogramma’s. Hij is een groot liefhebber van design en architectuur en heeft een passie voor minimalisme.
