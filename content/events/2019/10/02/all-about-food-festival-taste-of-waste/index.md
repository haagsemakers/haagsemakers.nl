---
type: event
path: /event/all-about-food-festival-taste-of-waste
title: 'All about food Festival: Taste of waste'
date: 2019-10-01
tags: ["e"]
start: 2019-10-02 18:30:00
end: 2019-10-02 21:30:00
event_url: https://nutshuis.nl/events/all-about-food-festival-taste-of-waste/
organizers: ["het_nutshuis", "juni_cafe", "impact_kitchen", "duurzaam_den_haag", "transitie_cinema", "conscious_kitchen"]
venue: het_nutshuis
featuredImage: all_about_food_web_v2-1.jpg
---

Taste of Waste is een avondvullend diner met blinde proeverijen, ‘geredde gerechten’ en een bord vol verhalen. Impact Kitchen, Juni Café en Instock bundelen hun krachten en laten je proeven én ervaren hoe voedselverspilling op een culinaire en creatieve manier een halt toegeroepen kan worden. Geniet van vier verrassende gangen. Elke gang wordt ingeleid met een blinde proeverij. Na het scherpen van je smaakzintuig krijg je het verhaal achter het geproefde product te horen. Wat is de afkomst, waarom zou deze groente of dit fruit verspild worden en waarom is dit geen afdankertje maar juist een dankbaar ingrediënt van een heerlijk gerecht? Het antwoord op die laatste vraag ontdek je direct. Van ‘blind tasting’ naar ‘shared dining’: schuif je ook aan?

### Over de initiatieven achter Taste of Waste:
**Stichting Impact** Kitchen gelooft in de kracht van eten. Wat we eten, hoe we eten, waar ons eten vandaan komt en wat we met onze overschotten en resten doen, heeft impact op onze maatschappij en milieu. Door het bevorderen en stimuleren van een duurzame en eerlijke voedselproductie en -consumptie probeert het enthousiaste Impact Kitchen-team bij te dragen aan een positieve verandering van ons voedselsysteem. De projecten van Impact Kitchen lopen uiteen van de ontwikkeling van educatieve spellen tot diners met een twist.

**Juni Café** staat bekend om haar kleurige, verrassende en elke dag wisselende menu’s. Salades, soepen, quiches, verse sapjes en homemade zoete lekkernijen. Een feest op je bord en nog gezond ook! De groente is biologisch, het vlees komt van dieren die lekker hebben rondgescharreld en de vis is zo diervriendelijk mogelijk gevangen. Zo veel mogelijk producten koopt Juni bij kleine ondernemers in de buurt.

**Instock**, een belangrijke leverancier van Juni, zet voedselverspilling letterlijk op de kaart en heeft inmiddels al meer dan 700.000 kilo eten gered van verspilling. Met drie succesvolle restaurants – in Den Haag, Utrecht en Amsterdam -, eigen biermerken en granola, kookboeken en lespakketten weet Instock steeds meer mensen te bereiken en inspireren.

Koop je kaartje voor Taste of Waste via de ticketbutton op deze pagina of kom langs bij de receptie in Het Nutshuis (uitsluitend pinbetaling). Voertaal van dit programma is Nederlands.

### Over het All About Food Festival
De gemiddelde Nederlander gooit vijftig kilo goed voedsel per jaar weg en is daarmee een van de grootste voedselverspillers van de wereld. Hoewel iedereen het ermee eens zal zijn dat dit moet veranderen, zit het probleem natuurlijk niet alleen bij de consument. In alle schakels van grond tot en met gft-afval gaat enorm veel voedsel verloren.

In de eerste week van oktober staat het All About Food Festival in Het Nutshuis helemaal in het teken van voedselverspilling. Aan het woord komen duurzame en sociale ondernemers en Haagse pioniers die het tij succesvol proberen te keren. Op het filmscherm leven mensen een half jaar van voedselafval. In de tuin gaan kinderen op ontdekkingsreis met hun stempelkaart. Aan tafel serveren we ‘geredde gerechten’ waarvan ook jíj (blind) kunt proeven. Praat, kijk, denk en eet je mee?

_Het All About Food Festival is een samenwerking tussen Het Nutshuis, Juni Café, Impact Kitchen, Duurzaam Den Haag, TransitieCinema en Conscious Kitchen._
