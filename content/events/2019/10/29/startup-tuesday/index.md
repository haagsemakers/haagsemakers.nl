---
type: event
path: /event/startup-tuesday-291019
title: 'Startup Tuesday The Hague: We Share | Sea Ranger Service'
date: 2019-10-04
tags: [ "startup" ]
start: 2019-10-29 17:00:00
end: 2019-10-29 19:00:00
event_url: https://www.startuptuesdaythehague.com/event-info/we-share-sea-ranger-service
organizers: ["startup_tuesday_the_hague", "we_share_ventures"]
venue: "the_hague_tech"
featuredImage: startup.png
---

How can social entrepreneurship save the oceans? What are the funding opportunities for such startups? Founder of the Sea Ranger Service, Wietse van der Werf, will talk about ocean threats, youth unemployment, maritime jobs, shipbuilding, veterans and how, by combining these issues and resources, they have developed an innovative business case to turn the tide for sustainable ocean use.

_Startup Tuesday The Hague is an initiative of World Startup Factory, The Hague Tech, Apollo 14, YES!Delft The Hague and The Hague Humanity Hub, all proudly part of ImpactCity. And supported by, We Share Ventures and Hotelschool The Hague._
