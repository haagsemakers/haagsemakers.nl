---
type: event
path: /event/platform-stad-stadsgesprek-mobiliteit
title: 'Stadgesprek: niet minder, maar anders'
date: 2019-10-03
tags: ["stedelijke ontwikkeling"]
start: 2019-10-29 16:30:00
end: 2019-10-29 18:00:00
event_url: https://www.platformstad.nl/agenda/mobiliteit-niet-minder-maar-anders/
organizers: ["platform_stad"]
venue: atrium_den_haag
featuredImage: mobiliteitCvdK-oe6yjciqoj4g5itf0lwzo1aqhkxfg5ftter5q27l8y.jpg
---

Hoe ziet jouw ideale stad eruit? Veel Hagenaars zouden graag meer groen of speelplekken in de stad willen, maar nu de stad hard groeit is ruimte schaars. Slim omgaan met de beschikbare ruimte is de uitdaging. Het Schenkviaduct, de Zevensprong en de Gedempte Gracht zijn drie bekende verkeersknelpunten in Den Haag waar zowel de leefbaarheid als de bereikbaarheid onder druk staan. Hoe zouden die plekken eruit kunnen zien als we andere mobiliteitskeuzes maken?

Professionals en ontwerpers zien kansen om ruimte te vinden, door het autogebruik te verminderen. Volgens Daan Zandbelt, Rijksadviseur voor de Fysieke leefomgeving ‘zijn vier wielen in de stad er eigenlijk twee te veel’. Fietsen en lopen nemen immers veel minder ruimte in, zijn gezonder en maken daarnaast de leefomgeving aantrekkelijker. Op plekken in de stad waar wordt gekozen voor schone en slimme mobiliteit ontstaat ruimte voor groen, speelplaatsen, terrassen of wonen.

Platform STAD organiseert een STADverkenning waarin ontwerpers voor drie verkeersknooppunten toekomstscenario’s verbeelden. Hoe verandert de leefkwaliteit als we overstappen naar andere vormen van mobiliteit. De ontwerpers **Léon Emmen** (Smartland landschapsarchitectuur), **Cor Simon** (Bosch Slabbers landschapsarchitecten), **Wendy van Kessel** (Urhahn Stedenbouw en strategie), **Melvin Kaersenhout** (Studio Maek) lichten hun keuzes toe tijdens het STADgesprek. Met hen en onder meer **David van Keulen** o.v.b. (hoofd mobiliteit gemeente Den Haag) voeren we het gesprek over de consequenties voor zowel de inrichting van de locaties, als het mobiliteitsgedrag van mensen.

_De toekomstbeelden uit de STADverkenning zijn vanaf 29 oktober 2019 tot 20 januari 2020 te zien in het Atrium in het stadhuis. Het STADgesprek vindt plaats in de week van Ruimte en Mobiliteit ter gelegenheid van de opening van de tentoonstelling. De toekomstscenario’s worden aangeboden aan de gemeente Den Haag.
foto boven: Christian van der Kooy_
