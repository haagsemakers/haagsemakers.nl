---
type: event
path: /event/itchy-tongue-royal-music-festival-2019
title: Itchy Tongue Royal Music Festival 2019
date: 2019-10-04
tags: [ "music" ]
start: 2019-10-19 16:30:00
end: 2019-10-20 01:00:00
event_url: https://www.eventbrite.nl/e/tickets-itchy-tongue-royal-music-festival-2019-70833431701
organizers: ["itchy_tongue", "submarine_den_haag", "de_besturing"]
venue: 'de_besturing'
featuredImage: itchy-tongue.jpg
---

When art and music merge, something fantastic and unforeseen happens. Now in the city of The Hague over the past years a great art/postartschool scene has been born and it keeps on growing. For a second time around we are show what we’re worth!

We give a stage to new initiatives and want bring together the music scene that developed from the art scene. For the existing music scene there’s lots of new things to explore!

Join us on this glorious day and do bring all your friends!
