---
type: event
path: /event/paagman-lekker-fred
title: Lekker Fred bij Paagman
date: 2019-10-09
tags: ["books"]
start: 2019-10-26 19:00:00
end: 2019-10-26 21:00:00
event_url: https://www.paagman.nl/lekker-fred
organizers: ["paagman"]
venue: paagman_fred
featuredImage: lekker-fred-paagman.jpg
---
Op zaterdag 26 oktober is Freddy Tratlehner (a.k.a. Vjeze Fur) om 15:00 uur te gast bij Paagman, vestiging Centrum (Lange Poten 41, Den Haag). Hij zal naar aanleiding van zijn kookboek Lekker Fred geïnterviewd worden door Janneke Vreugdenhil. Aansluitend zal hij signeren.

Lekker Fred
Schreeuwend van de jeuk liep ik terug naar huis over de Oostenrijkse zoutvlaktes. Ik moet een jaar of zes zijn geweest toen ik na anderhalf uur lopen eindelijk huilend thuiskwam. Ik was aan 't spelen in de bos en was meerdere malen geprikt door de Reuzelmug. Door de bulten gloeide m'n hele rug als een kaassoufflé! Vlug pakte mijn moeder een blok boter en smeerde een dikke laag van het smeerbare goud over mijn kapot geprikte lichaampje. Al snel nam de jeuk af en kwam ik weer een beetje bij zinnen. Zonder dat blok boter was ik hier misschien wel nooit geweest en had ik dit boek vol helende recepten nooit kunnen schrijven! En dat zijn het, helende recepten. Niet alleen voor het lichaam, maar bovenal voor de ziel. Want zeg nou zelf, wat werkt er beter dan na een lange dag zwoegen in de kaasmijnen thuiskomen en genieten van een heerlijk bord verse pasta met paramagiano? Een grote hap paddestoeljenrisotto en u waant zich in een herfstig bos vol dansende dieren. Effe toch proberen nou eindelijk eens onder de honderd kilo te komen? Een heerlijke fitkech curry, zonder calorieën, waardig voor welke yogaknot dan ook! Kortom, een boek voor jou. De moderne mens, de klassieke mens, hij die zich eens wil uitsloven voor zijn vriendinnen, zij die zichzelf eens lekker wil verwennen. Ga mee op zoektocht naar je culinaire zelf. Thuiskomen was nog nooit zo lekker.

Freddy Tratlehner
Freddy zat op de Rietveld Academie in Amsterdam. Met muziekformatie De Jeugd van Tegenwoordig bracht hij o.a. het nummer Watskeburt?! uit. De passie voor eten heeft hij van huis uit meegekregen, zijn vader is chef-kok geweest. Freddy houdt ervan om te koken en te eten. Op zijn Instagram maakt hij regelmatig in eigen stijl hilarische kookvideo's genaamd Lekker Fred.
