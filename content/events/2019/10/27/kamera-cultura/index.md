---
type: event
path: /event/kamera-kultura-27okt2019
title: Kamera Kultura
date: 2019-10-01
tags: ["food"]
start: 2019-10-27 11:00:00
end: 2019-10-27 13:00:00
event_url: https://nutshuis.nl/events/kamera-kultura-27okt2019/
organizers: ["het_nutshuis"]
venue: het_nutshuis
featuredImage: Femme-ter-Haar-film-Uitzicht_JPG.jpg
---

Vertrouwd én verrassend: dat is ons maandelijkse programma Kamera Kultura. **Jellie Brouwer** (radiopresentator NTR Kunststof) ontvangt uiteenlopende gasten en Juni Café maakt de beste brunches.

Elk jaar wordt op 20 november de Internationale Dag van de Rechten van het Kind (Universal Children’s Day) gevierd. UNICEF en de Verenigde Naties hebben hiervoor dertig jaar geleden een verdrag aangenomen waarin de basisrechten van alle kinderen ter wereld zijn vastgelegd. Inmiddels is het verdrag door 191 van de 193 landen op de wereld ondertekend. Toch leven in Nederland te veel kinderen in armoede, groeien te veel kinderen op in een onveilige situatie of krijgen ze niet het onderwijs of de jeugdhulp die ze zouden moeten krijgen. Jellie gaat hierover in gesprek met **Carla van Os**, universitair docent kinderen en recht bij de vakgroep orthopedagogiek van de Rijksuniversiteit Groningen.

**Femke Sleegers** werkte als copywriter voor banken en verzekeraars, maar het werk ging botsen met haar overtuigingen. Het leidde tot een abrupt besluit. In februari 2016 stopte ze radicaal met haar goedbetaalde werk om als onbezoldigd klimaatstrijder de barricaden op te gaan. Jellie gaat met Femke in gesprek over Den Haag Fossielvrij, de brede burgerbeweging waarvoor zij als coördinator werkt. Met aanstekelijke en verrassend effectieve campagnes spoort de beweging de gemeente Den Haag aan tot een ambitieus klimaatbeleid.

“Er wordt over eenzaamheid vaak gepraat alsof het iets is dat ver van je af staat. Het is juist goed om het te omarmen dat die gevoelens in je zitten. Ik hoop dat de film een deurtje opent en vragen aan de kijker stelt: Ben ik eenzaam en waar komt dat door?” Aldus de jonge maker **Femme ter Haar**, die in de zomer van 2018 aan de HKU afstudeerde met haar geanimeerde documentaire Uitzicht. In de geprezen film onderzoekt, bevraagt en verbeeldt Femme het begrip eenzaamheid op een unieke manier en vanuit uiteenlopende perspectieven. In Kamera Kultura vertonen we de film van dertien minuten in zijn geheel en voert Jellie een kort gesprek met de maker.

De afgelopen jaren veroverde **Margriet Sjoerdsma** Nederland met een tribute aan Eva Cassidy. Meer dan tachtig keer speelde ze voor uitverkochte zalen, waaronder North Sea Jazz festival en een afgeladen Concertgebouw. In 2019 is Margriet een nieuwe weg ingeslagen en keerde ze terug naar haar eerste liefde: eigen liedjes maken. De liedjes kwamen tot leven in Odelien, een huisje in een overrompelend landschap in Noorwegen. Voor Margriet het ideale decor om te schrijven. In haar eentje schreef ze er drie maanden lang over tijdloze thema’s als loslaten, durven springen, eenzaamheid, liefde en verwondering. Te beluisteren tijdens Kamera Kultura!
