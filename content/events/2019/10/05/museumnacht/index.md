---
type: event
path: /event/museumnacht-2019
title: Museumnacht 2019
date: 2019-10-04
tags: ["art"]
start: 2019-10-05 20:00:00
end: 2019-10-06 02:00:00
event_url: https://www.museumnachtdenhaag.nl
organizers: ["museumnacht_den_haag"]
featuredImage: opening-son-mieux.jpg
---

Yeaaah! Museumnacht Den Haag bestaat 10 jaar!
En daarom is het tijd voor een groot cultureel feest. De jubileumeditie van Museumnacht Den Haag vindt plaats op zaterdag 5 oktober 2019. Met 47 deelnemende Haagse musea is het de grootste editie ooit! Deze jubileumeditie wordt extra verrassend en zal je doen verbazen. 
