---
type: event
path: /event/paagman-knapsack
title: Knapsack bij Paagman
date: 2019-10-09
tags: ["books"]
start: 2019-10-23 19:00:00
end: 2019-10-23 21:00:00
event_url: https://www.paagman.nl/knapsack
organizers: ["paagman"]
venue: paagman_fred
featuredImage: knapsack-paagman.jpg
---
Op woensdag 23 oktober  is Esther Vlasveld om 15:00 uur te gast bij Paagman, vestiging Fred (Frederik Hendriklaan 217, Den Haag). Zij zal haar boek Knapsack presenteren. Aansluitend zal zij signeren en is er een knutselworkshop door Museumjuf Merel.

Knapsack
Met Knapsack wil iedereen wel naar buiten, dankzij de praktische tips en vrolijke beelden die Esther Vlasveld voor jou verzamelde. Het is een doe-boek voor avonturiers, voor jagers en verzamelaars, voor uitvinders en verbinders. En voor iedereen die dat wil worden. Een saai stukje wandelen wordt een zoektocht en een blaadje een kunstwerk. Ga je mee op ontdekkingstocht?

[Meer informatie over Knapsack](http://www.knapsackavonturen.nl/)

Tip: geniet na het evenement van een avondmaaltijd in ons Kicking Horse Café. Iedere doordeweekse avond tussen 17:00 - 20:00 uur kunt u genieten van een heerlijke warme maaltijd voor slechts €14,50. Bekijk hier het wisselende weekmenu!
