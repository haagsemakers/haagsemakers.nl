---
type: event
path: /event/startup-tuesday-221019
title: 'Startup Tuesday The Hague: HSD edition'
date: 2019-10-04
tags: [ "startup" ]
start: 2019-10-22 17:00:00
end: 2019-10-22 19:00:00
event_url: https://www.startuptuesdaythehague.com/event-info/hsd-edition
organizers: ["startup_tuesday_the_hague", "campus_the_hague_security_delta"]
venue: "campus_the_hague_security_delta"
featuredImage: hsd.png
---

Programme TBA

_Startup Tuesday The Hague is an initiative of World Startup Factory, The Hague Tech, Apollo 14, YES!Delft The Hague and The Hague Humanity Hub, all proudly part of ImpactCity. And supported by, We Share Ventures and Hotelschool The Hague._
