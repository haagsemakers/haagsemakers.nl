---
type: event
path: /event/finale-stadslab-im-binck-investeringsmaatschappij-binckhorst
title: 'Finale Stadslab I’M BINCK, InvesteringsMaatschappij Binckhorst'
date: 2019-10-01
tags: ["binckhorst","Events","im binck"]
start: 2019-04-18 14:30:00 
end: 2019-04-18 18:00:00 
event_url: www.imbinck.nl 
organizers: [null]
featuredImage: stadslab-11-2018-low-res-04.jpg
---

93 unieke waardevolle plekken en geluiden, een zwembad gevuld met overtollig regenwater, de BinckBank voor initiatieven, de PraktijkAcademie Binckhorst_,_ een BinckedIn, een dienstenfootprint en veel meer. Acht I’M BINCK Stadslabs hebben een rijke oogst opgeleverd aan ideeën en concrete resultaten om de _Kernwaarden Binckhorst 2017-2030_ in de toekomstige gebiedsontwikkeling mee te nemen.

De oogst deelt I’M BINCK graag op 18 april van 14.30 – 18.00 uur met partners en geïnteresseerden. U bent van harte welkom!

**Datum: 18 april 14.00 uur inloop > 14.30 uur begin programma > 18.00 uur,** **aansluitend borrel** **Locatie MOOOF,** Binckhorstlaan 135, 2516 BA Den Haag **Graag aanmelden via:** **[info@optrekbinckhorst.nl](mailto:info@optrekbinckhorst.nl)**

Het programma bestaat uit twee delen:

deel 1 waarin de resultaten worden toegelicht en het proces van het stadslab.  In deel 2 gaan [Tom Bade](http://www.tripleee.nl/)(Triple E) Jurgen van der Heijden (AT Osborn) en [Theo Stauttener](https://www.stadkwadraat.nl/team) (Stadkwadraat) dieper in op mogelijke organisatievormen en financieringsconstructies voor I’M BINCK, de InvesteringsMaatschappij Binckhorst.

Hoe kan deze organisatie, die nu vooral als platform fungeert, een vervolgstap maken en concreet bijdragen aan de realisatie van een aantal van de ideeën uit de Stadslabsessies? En met behulp van meervoudige businesscases ook daadwerkelijke investeringen mogelijk maken?

We gaan met de deelnemers in op kansen, valkuilen en lessen uit de acht sessies die we hebben georganiseerd, en sluiten af met een borrel. U bent van harte uitgenodigd deel te nemen aan de bijeenkomst op 18 april!

_Het Stadslab I’M BINCK is mede mogelijk gemaakt door het Stimuleringsfonds Creatieve Industrie en MAEX. Doel is antwoorden te krijgen op de volgende vragen: Hoe de rauwe, authentieke kracht en identiteit van de Binckhorst vast te houden en slim met de toekomstige ontwikkelingen te versterken? Hoe de unieke gebiedskernwaarden collectief te borgen zodat zij voor alle partijen leidende principes zijn voor de visievorming en de concrete ontwikkeling van het gebied? En hoe dit alles te onderbouwen met een maatschappelijke businesscase?_