---
type: event
path: /event/politiek-cafe-duurzaam-voedsel
title: "Politiek Café: Duurzaam Voedsel"
tags:
  - Food
start: 2019-12-13T19:45
end: 2019-12-13T22:00
event_url: https://www.utopie.nl/p/posts/politiek-cafe-duurzaam-voedsel-295.php
venue: utopie
featuredImage: voedsel5d6b86392eda7.jpg
---

Mes en vork zijn belangrijke wapens in de strijd tegen klimaatverandering. De gemeente heeft hier een voorbeeldfunctie, maar vervult ze die wel goed? Op vrijdag 13 december a.s. organiseren de gemeenteraadsfracties van de Partij voor de Dieren Den Haag, de Haagse Stadspartij en de PvdA Den Haag een politiek café over deze en andere vragen omtrent duurzaam voedsel. Waarom is het belangrijk dat voedsel duurzamer wordt? Wat kan de gemeente doen en hoe helpen en stimuleren we burgers en organisaties om de transitie te maken naar een duurzamer, eerlijker en plantaardiger consumptiepatroon?

Tijdens het café zullen de 3 politieke partijen hun gezamenlijke initiatiefvoorstel ‘Duurzaam voedsel; nog een tandje bijzetten’ kort toelichten. Daarna zal professor Henriette Prast , expert op het gebied van de gedragseconomie, een lezing geven over het door haar bedachte concept ‘Carnivoor? Geef het door!’ Liane Lankreijer, van het landelijke kernteam van de Voedsel Anders beweging, komt vervolgens vertellen over de ervaringen van bottom-up voedselinitiatieven. En daarna gaan we graag met alle aanwezigen in gesprek.

Er is nog een wereld te winnen in Den Haag voor het klimaat. Het zal een informatieve en gezellige avond worden. Komen jullie ook?

Het Politiek Café vindt plaats in Grand café Utopie (Waldeck Pyrmontkade 116) om 19:45 uur (inloop vanaf 19:30 uur).
