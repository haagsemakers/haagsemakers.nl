---
type: event
path: /event/celebrating-impact
title: Annual Conference 2019 - Celebrating Impact
date: 2019-10-04
tags: [ "startup" ]
start: 2019-11-05 09:00:00
end: 2019-11-07 18:00:00
event_url: https://www.aanmelder.nl/evpaconference/subscribe
organizers: ["evpa", "impact_city"]
venue: "fokker_terminal"
featuredImage: celebrating-impact.jpg
---

The EVPA Annual Conference is the ultimate interactive event for foundations and impact funds. Organised in partnership with the ImpactFest by the city of The Hague, at this anniversary edition you will have the chance to:

  - Network and collaborate with 1000+ world class practitioners and experts in the field from 50+ countries
  - Discover new global trends on investing for impact and deepen your knowledge to support social change
  - Use the unique opportunity to bring your investees to the Impact Fest and showcase their work
  - Meet 300+ social enterprises from all over the world

Join us and be part of the largest EVPA gathering in a unique and interactive setting!
