---
type: event
path: /event/impactfest-2019
title: ImpactFest 2019
date: 2019-10-04
tags: [ "startup" ]
start: 2019-11-05 11:00:00
end: 2019-11-05 21:00:00
event_url: https://www.aanmelder.nl/evpaconference/subscribe
organizers: ["impact_city", "evpa"]
venue: "fokker_terminal"
featuredImage: impactfest.png
---

IMPACT FEST: Europe's largest impact meetup
5 November 2019 - Tickets are live!
In Collaboration with EVPA

Roundabouts, meetups and ticket info is live on www.impactfest.nl

Driven by the motto ‘doing good & doing business’, more than 1000 impact-makers from all over Europe will join forces during the 4th edition of ImpactFest. At The Fokker Terminal in The Hague, they will show that innovations can make the world a better place to live.
ImpactFest will once again demonstrate that social impact and economic success go hand in hand.

November 5th:
- Meet up sessions - Open for all.
Make use of our giant partner network and take a deep dive together.

- Roundabouts - Open for all.
Highly popular and effective to learn and make powerful connections: the Roundabout sessions.
Engage in an in-depth discussion on specific topics within the table host’s expertise. You can consider these Roundabout sessions as intimate and interactive small-scale workshops in which participants will not only learn from the host, but from all participants at the table. In three consecutive rounds we will host 100 different Roundabouts.

- Impact sessions - Invite only or win your seat!
Bringing together impact investors, funds, purpose driven corporations and organizations together with impact start-ups and scale-ups to sharpen their story and explore potential leads and deals. Matchmaking is key. As such, these sessions are very desirable and carefully curated and on the basis of invitation only.
Keep an eye on our social media channels for your chance to be invited!

- - -

Tickets are on through www.impactfest.nl
Regular tickets: €45
Startup tickets: €15

ImpactFest 2019 partners:
EVPA - European Venture Philanthropy Association - European Commission - Euclid Network - Rabobank Regio Den Haag - B Corporation - The Hague Humanity Hub - HortiHeroes - World Startup Factory - Partos - Hivos - Rainmaking Innovation - Get in the Ring - Stichting DOEN - Unreasonable - Startup in Residence The Hague - Gemeente Den Haag - HiiL - Foodstars
