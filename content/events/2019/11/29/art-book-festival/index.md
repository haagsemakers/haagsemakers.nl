---
type: event
path: /event/the-other-book-art-book-festival
title: The Other Book - Art Book Festival
date: 2019-10-04
tags: [ "art" ]
start: 2019-11-29T18:00
end: 2019-12-01T22:30
event_url: http://www.theotherbook.eu/index.html
organizers: ["the_other_book"]
featuredImage: book.jpg
---

Festival The Other Book is a unique celebration of the art book in all its facets. It is the first biennial event in The Hague (NL) that combines an art book fair with exhibitions, workshops, talks, presentations, demonstrations, films, and performances at various locations. Each edition focuses on a different kind of ‘other book’. The first edition of The Other Book (29 November 2019 – 1 December 2019) is devoted to the Riso revolution: art books produced with Risograph and Mimeograph stencil duplicators.

Over the course of three days, The Other Book provides a platform for artists, publishers, print rooms, collectives, art libraries, collectors and book enthusiasts. By partnering with local artists and initiatives, and providing a stage for both local and (inter)national artists, The Other Book serves as a place of exchange. The extensive program explores artistic practices, conceptual publications and the seemingly endless possibilities of the art book.

The programming is largely compiled by The Other Book, supplemented with a few program components from our partner initiatives in The Hague that link up with the festival. The entire program consists of four elements that complement and reinforce each other:
Exhibitions (investigative)
Workshops (doing)
Talks (depth)
Art book fair (exchange) on Sunday 1 December at The Grey Space in the Middle

Various events such as demonstrations and performances by Sergej Vutuc, Topp Dubio and Jip Piet will also take place during The Other Book.

_
Locations:
Quartair Contemporary Art Initiatives
Grafische Werkplaats Den Haag
billytown
Page Not Found
The Grey Space in the Middle
West
_
Opening:
Friday 29 November 2019, 19H00 at Quartair Contemporary Art Initiatives. This will also be the start (location) of Hoogtij#59, the cultural route of The Hague

_
Supported by Stroom Den Haag
Posterdesign by Julian Sirre
