---
type: event
path: /event/hoogtij-59
title: "Hoogtij #59"
date: 2019-11-14T23:00:00Z
tags:
- rondgang
- kunst
- hoogtij
start: 2019-11-29 18:00
end: 2019-11-29 10:00
event_url: http://www.hoogtij.net/
featuredImage: hoogtij59.png

---
Hoogtij, De Haagse hedendaagse kunstroute

Vr 29 november 2019 19:00-23:00 u.

Tijdens HOOGTIJ#59 kun je meer dan 20 locaties kunstlocaties bezoeken: van white cure tot underground; van gevestigde kunst in de galeries en instellingen tot installaties en performances bij de kunstenaarsinitiatieven.

Start 18:00-19:00 u.: [Quartair](http://www.hoogtij.net/galerie/quartair/) (Toussaintkade 55)

De diverse gratis rondleidingen voor wie het wil, starten hier om 19:00 u. Ga hier naar de rondleidingen.
