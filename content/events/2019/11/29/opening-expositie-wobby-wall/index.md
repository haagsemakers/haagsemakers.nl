---
type: event
path: /event/expositie-wobby-wall
title: "Expositie Wobby Wall"
tags:
  - grafische werkplaats
  - expositie
start: 2019-11-29T20:00
end: 2020-02-10T23:00
locations: 
- de_grafische_werkplaats
organizers:
- grafische_werkplaats
featuredImage: 59886634-b1ab-48b6-aca0-fafcf5c61a3e.jpg
event_url: "https://grafischewerkplaats.nl/exposities/"
---
## Wobby Wall
Expositie 29 november- 7 februari 2020

Met: Lisa Blaauwbroek, Bas de Geus, Jip Hilhorst, Jelmer Konjo, Alexandra Martens en Andrew Tseng

Ter gelegenheid van de eerste editie van The Other Book,
een Haags festival rond kunstenaarsboeken organiseren de Grafische Werkplaats samen met de makers van het magazine Wobby het Wobby Wall Project. De eerste editie van The Other Book staat in het teken van de risograaf.

Zes makers uit de regio Haaglanden die een bijdrage aan Wobby hebben geleverd of binnenkort een bijdrage leveren, nodigen we in samenwerking met de makers van Wobby uit om nu niet alleen een design aan te leveren, maar ook om zelf achter de risograaf te staan. Zo verschijnen er verschillende Wobby Walls in de werkplaats.

*On the occasion of the first edition of The Other Book, a festival based on artists’ books in The Hague, the Grafische Werkplaats and the makers of the Wobby magazine are organizing the Wobby Wall Project. The first edition of The Other Book is all about the risograph. In collaboration with the makers of Wobby, six makers from the Haaglanden region who have made a contribution to Wobby or who will soon be making a contribution, invite not only to provide a design, but also to stand behind the risograph. For example, various Wobby Walls appear in the wertkplaats.*