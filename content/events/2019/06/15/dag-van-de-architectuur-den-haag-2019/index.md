---
type: event
path: /event/dag-van-de-architectuur-den-haag-2019
title: 'Dag van de Architectuur Den Haag 2019'
date: 2019-10-01
tags: ["Dag van de architectuur"]
start: 2019-06-15 10:00:00
end: 2019-06-15 18:00:00
event_url: https://www.dvda-denhaag.nl/
organizers: ["dag_van_de_architectuur_den_haag"]
featuredImage: dvda-jump-freerun-a.jpg
---

In 2019 kijkt de Dag van de Architectuur met andere ogen naar architectuur en de stad.

### **Door andere ogen.. **

De ervaring van architectuur en de stad wordt bepaald door wie je bent. Als schoonmaker ken je alle intieme plekjes van het gebouw die je als passant nooit ziet. Als kind ervaar je de speeltuin zo anders dan als ouder. In het programma op 15 juni laten we zoveel mogelijk ervaringen zien door speciale rondleidingen en tours. Ook zijn er eenmalig gebouwen open die anders nooit opengesteld zijn.

### **Ga mee**

Ervaar en beleef de rijkdom van architectuur vanuit de gebruiker, de eigenaar en de ontwerper. Kom op zaterdag 15 juni naar Den Haag en bezoek bijzondere gebouwen die anders niet toegankelijk zijn. Architectuurgidsen en gebruikers verzorgen gratis rondleidingen. Ook zijn er tours verzorgd door de bewoners van de Schilderswijk, de kunstenaars van Annastate en de gebruikers van de Binckhorst. Of doe mee met een van de vele activiteiten.

### **Programma**

Begin juni laten we via deze nieuwsbrief weten wanneer de inschrijving voor de verschillende activiteiten opengaat. Vervolgens verschijnt op 6 juni in samenwerking met Den Haag Centraal de festivalkrant met artikelen rondom het thema en het complete programmaoverzicht. Op 15 juni vindt ook de **Dag van de Bouw** plaats. Kijk via de [website](https://dvda-denhaag.us8.list-manage.com/track/click?u=ad77148078830cbadbe4509eb&id=4e715bdbec&e=f11e9c6387) welke bouwplaatsen er open zijn in Den Haag en omgeving. Hou ook onze website in de gaten voor bijzondere samenwerkingsprogramma's. Of kijk regelmatig op de website.

### **Doe mee**

Wil je ook jouw unieke kijk op de stad laten zien? Heb je een bijzonder verhaal bij een gebouw? Wil je helpen als vrijwilliger of als sponsor? Laat het ons weten via [www.dvda-denhaag.nl](https://dvda-denhaag.us8.list-manage.com/track/click?u=ad77148078830cbadbe4509eb&id=ad8b41ff87&e=f11e9c6387)
