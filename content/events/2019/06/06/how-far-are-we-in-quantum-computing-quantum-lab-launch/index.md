---
type: event
path: /event/how-far-are-we-in-quantum-computing-quantum-lab-launch
title: 'How far are we in Quantum Computing? - Quantum Lab Launch'
date: 2019-10-01
tags: ["quantum computing"]
start: 2019-06-06 18:00:00 
end: 2019-06-06 22:00:00 
event_url: https://www.meetup.com/nl-NL/QuantumLab/events/260858598/ 
organizers: [null]
featuredImage: highres-480690885.png
---

For our launch, we are hosting a two-part event. We will start with a presentation by Stephanie Wehner from QuTech on Quantum Internet and where we are in its development. After a short break for pizza and beer, we'll continue with a panel discussion the foreseen effects of Quantum Internet and computing on business and society. Agenda: 18:00 - 18:30 Walk-in 18:30 - 18:45 Overview & Lab 18:45 - 19:45 Stephanie Wehner on QuTech & Quantum Internet Current state and what is it? 19:45 - 20:15 Pizza Break 20:15 - 21:00 Panel, Q&A 21:00 - Drinks and Networking