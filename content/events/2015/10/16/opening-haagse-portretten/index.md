---
type: event
path: /event/opening-haagse-portretten
title: 'Opening Haagse Portretten'
date: 2019-10-01
start: 2015-10-16 20:00:00 
end: 2015-10-16 22:00:00 
event_url: grafischewerkplaats.nl 
organizers: ["grafische_werkplaats"]
locations: ["de_grafische_werkplaats"]
---

Graag nodigen we je uit voor de opening van: HAAGSE PORTRETTEN op vrijdagavond 16 oktober om 20.00 uur (tijdens Hoogtij#42) door Geert-Jan Borgstein, werkzaam in het Mauritshuis en betrokken bij de tentoonstelling Hollandse zelfportretten - Selfies uit de Gouden Eeuw. Met werk van Philip Akkerman, Melle de Boer, Kees Koomen, Ondine de Kroon, Toyin Loye, Jos de L’Orme, Ewoud van Rijn, Ilse Versluijs, Babette Wagenvoort en Jerney de Wilde en 80 Haagse amateurs. Haagse Portretten is een initiatief van de Grafische Werkplaats en verbindt op een creatieve manier de stad met haar bewoners, stimuleert het werken met grafiek en combineert analoge en digitale technieken met elkaar. Een expositie uit twee delen Haagse Portretten bestaat uit een expositie van zelfportretten van tien Haagse of in Den Haag werkende beeldend kunstenaars. Zij maakten voor deze expositie nieuw werk in linoleum. Het tweede deel is een kleurrijke mobiele groei-expositie in de vorm van een huisje met daarop selfies gedrukt, die Haagse bewoners van jong tot oud in verschillende workshops maakten.