---
type: event
path: /event/films-made-in-the-hague-theaterdakota
title: 'Films made in The Hague @TheaterDakota'
date: 2019-10-01
tags: ["dakota","Events","film","made in the hague"]
start: 2015-10-27 20:00:00 
end: 2015-10-27 23:00:00 
event_url: http://www.theaterdakota.nl/agenda/films-made-hague/ 
organizers: ["made_in_the_hague"]
locations: ["theater_dakota"]
featuredImage: cee9c1c198ce6ff6bae384b97154e41c-f1416.jpg
---

**Haagse historische bedrijfsfilms**
------------------------------------

De Haagse industrie heeft op vele manieren sporen nagelaten, zowel in nog bestaande fabrieken als producten, maar ook in reclames.  Van een beperkt aantal fabrieken bestaan bewegende beelden. In deze bedrijfsfilms, die meestal voor reclamedoeleinden werden gemaakt, worden beelden getoond van het productieproces, de eigen bedrijfswinkels en uiteraard de producten die de fabriek vervaardigde. In het kader van Made in The Hague krijgt u een aantal korte en langere historische bedrijfsfilms te zien van de bekende Haagse firma’s als Vredestein en Van der Heem. Ook minder bekende bedrijven, zoals de haardenfabriek Jan Jaarsma en de firma Van den Broek van de overbekende automatische suikerpot zijn in het programma opgenomen. Van de in Den Haag geproduceerde Solex is een kort filmpje te zien uit de periode van de introductie van dit nieuwe fenomeen – ‘fietsen zonder trappen’. Bij de films wordt een korte toelichting gegeven over de bedrijven en hun producten met foto- en reclamemateriaal en er is een mogelijkheid om vragen te stellen aan de industrieel erfgoed specialisten.

[www.madeinthehague.eu](http://www.madeinthehague.eu/)

**Feiten**
----------

Dinsdag 27 oktober 2015 Aanvang 20.00 uur, zaal open om 19.30 uur Theater Dakota, Studio Zuidlarenstraat 57, Den Haag Kosten: kaartjes á € 5,- per persoon, incl. koffie/thee in de pauze