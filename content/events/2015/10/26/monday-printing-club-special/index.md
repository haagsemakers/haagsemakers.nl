---
type: event
path: /event/monday-printing-club-special
title: 'Monday printing club special'
date: 2019-10-01
tags: ["Events","grafische werkplaats"]
start: 2015-10-26 19:30:00 
end: 2015-10-26 21:30:00 
event_url: http://grafischewerkplaats.nl/event-registration/?ee=28 
organizers: ["grafische_werkplaats"]
locations: ["de_grafische_werkplaats"]
featuredImage: 7bb87b32-86a3-4e25-b8ad-eb02e681bd32.jpg
---

Ter gelegenheid van de expositie [**Haagse Portretten**](http://grafischewerkplaats.nl/tijdens-hoogtij42-opening-haagse-portretten/) is er op maandagavond 26 oktober een avond met drie korte presentaties over zelfportretten in de kunst met Philip Akkerman, Babette Wagenvoort en Femke Egas, verbonden aan het Mauritshuis. Je kan dan gelijk de expositie bekijken, waaraan Philip en Babette meewerken. De avond is gratis toegankelijk, reserveren verplicht.[Hier reserveren](http://grafischewerkplaats.nl/event-registration/?ee=28).

Monday Printing Club Special is vier keer per jaar, geeft verdieping en inspiratie. Een programma met drie korte lezingen, presentaties of interviews in relatie met de expositie die op dat moment in de werkplaats te zien is.