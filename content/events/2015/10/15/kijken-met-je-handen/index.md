---
type: event
path: /event/kijken-met-je-handen
title: 'Kijken met je handen'
date: 2019-10-01
start: 2015-10-15 12:00:00 
end: 2015-10-15 17:00:00 
organizers: ["studio_vrugt"]
locations: ["edith_stein_college"]
---

_Kijken met je handen_ is een kunstproject van Sara Vrugt op het Haagse Edith Stein College. Samen met twintig leerlingen is een 100 jaar oude eik gekapt en een ontwerp voor een houten paviljoen (van diezelfde eik) gemaakt. De bouw op het schoolterrein is inmiddels afgerond.

Het paviljoen wordt in samenwerking met Slagwerk Den Haag en de leerlingen met een klein spektakel geopend. Verwacht 64 drumstokken, iets met kettingzagen en een heuse vuurspuwer.

Iedereen is welkom en klimmen mag.