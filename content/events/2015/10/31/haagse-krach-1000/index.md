---
type: event
path: /event/haagse-krach-1000
title: 'Haagse Krach 1000'
date: 2019-10-01
start: 2015-10-31 00:00:00 
end: 2015-10-31 23:59:59 
event_url: http://www.hk1000.nl 
organizers: ["duurzaam_den_haag"]
locations: ["fokker_terminal"]
featuredImage: hk1000-fotos-6.jpg
---

Krachten bundelen voor een mooier Den Haag:   Wij geloven dat mensen de drijvende kracht zijn achter de duurzame ontwikkeling van Den Haag. Wij noemen dat Haagse Krach. Op zaterdag 31 oktober organiseren we een bijzondere bijeenkomst: de Haagse Krach 1000. Een dag waarop we krachten van 1000 Hagenaars en Hagenezen bundelen en ideeën delen over hoe we Den Haag samen nog duurzamer, mooier, schoner en leefbaarder kunnen maken. Geloof jij ook in de kracht van mensen? En wil je jouw ideeën delen? Haak dan aan bij de HK1000!

### **Wat is de HK1000**

De Haagse Krach 1000 vindt plaats op zaterdag 31 oktober tussen 10.00 en 17.00 uur in de Fokker Terminal aan de Binckhorstlaan 249. Die dag komen duizend Haagse bewoners en ondernemers bij elkaar om hun dromen en ideeën voor hun stad uit te wisselen. En om samen plannen te bedenken om deze dromen en ideeën waar te maken.

### **Loting**

Om iedereen een gelijke kans op deelname te geven worden uitnodigingen verstuurd naar inwoners en ondernemers die zijn geloot uit het kies- en handelsregister. Zij ontvangen in de eerste week van oktober 2015 een uitnodiging voor de HK1000. Wil je niet wachten tot je (misschien) een uitnodiging krijgt? Plaats jezelf dan op de [reservelijst](http://www.hk1000.nl/doe-mee/). Of meld je aan als[vrijwilliger](http://www.hk1000.nl/doe-mee/). Dan weet je zeker dat je erbij bent.

### **Tafelschikking**

Zo ziet het er straks uit tijdens de HK1000: in de Fokker Terminal staan 100 tafel met elk 10 personen, waar iedereen zijn of haar dromen en ideeën voor de stad deelt. Vervolgens bepalen wij gezamenlijk welke dromen en ideeën verder worden uitgewerkt tot een concreet plan. Elke tafel is als volgt ingedeeld:

*   6 ingelote burgers
*   1 ingelote ondernemer
*   1 dialoogbegeleider
*   1 vrijdenker die de discussie scherp houdt
*   1 beleidsmaker die vanuit de gemeente meedenkt

Wil je op de hoogte blijven van de HK1000? Like onze [Facebook pagina](https://www.facebook.com/HaagseKrach1000), [word lid van onze Facebook groep](http://bit.ly/haagsekrach "word lid van onze Facebook groep"), volg ons op [Twitter](https://www.twitter.com/HaagseKrach1000) of schrijf je in voor de [nieuwsbrief](http://www.hk1000.nl/nieuwsbrief/).

### **Wie zijn wij?**

Wij zijn een groep Hagenaars en Hagenezen die zijn geïnspireerd door de G1000-bijeenkomsten die overal in het land ontstaan. Aan deze burgertoppen willen we een flinke portie Haagse daadkracht toevoegen, zodat er ook echt iets moois gaat gebeuren in de stad. We zijn in februari 2015 begonnen met een groep van 20. Op 9 april kwamen we met zo’n 100 Hagenaars en Hagenezen samen tijdens de startbijeenkomst in DeeDriemz op de Witte de Withstraat en op 13 juni draaiden we warm met de HK1000 Warming Up, in Buurthuis De Mussen in de Schilderswijk.

### **Wil je meer weten?**

Wil je meer weten over de HK1000? Mail dan naar info@hk1000.nl of bel ons op 070 – 364 30 70.