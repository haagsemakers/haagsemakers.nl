---
type: event
path: /event/social-club-investor-day-deadline
title: 'Social Club Investor Day Deadline'
date: 2019-10-01
tags: ["Deadlines","pitch","sociaal ondernemen","social business","startup"]
start: 2015-10-31 12:00:00 
end: 2015-10-31 13:00:00 
event_url: http://www.socialclubdenhaag.nl/idee-inbrengen/ 
organizers: ["social_business_club"]
locations: ["0"]
featuredImage: logo1.png
---

De Social Business Club organiseert het Social Club Investor Days event op 23 november i.s.m. Fonds 1818. [http://www.socialclubdenhaag.nl/idee-inbrengen/](http://www.socialclubdenhaag.nl/idee-inbrengen/)

> Hiervoor zijn we op zoek naar sociale business plannen die een financieringsbehoefte tussen de 200.000 euro en 1 miljoen euro nodig hebben. In onderstaand formulier kan je een document toevoegen waarin deze financieringsbehoefte wordt verantwoord. Fonds 1818 biedt ondernemers met kansrijke sociale businessplannen de mogelijkheid van een lening of participatie van 200.000 tot 1 miljoen. De financieringsvorm ligt niet vast, het kan een rentedragende lening, aandelenovereenkomst of elke vorm daartussen zijn. Het gaat om een op-maat-financiering, overeen te komen tussen Fonds 1818 en de ondernemer(s) zelf. Bij de beslissing over de financiering wordt rekening gehouden met criteria, zoals: - de sociale doelstellingen die worden nagestreefd - de haalbaarheid van het plan - de risico's - de kwaliteit en ervaring van de ondernemer(s). De financieringsvoorwaarden zijn vergelijkbaar maar zeker niet scherper dan bij een financiële instelling als een bank.