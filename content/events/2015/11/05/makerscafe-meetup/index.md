---
type: event
path: /event/makerscafe-meetup
title: 'Makerscafé Meetup #5'
date: 2019-10-01
tags: ["Events","Haagse Makers","maker cafe","makerscafe"]
start: 2015-11-05 19:00:00 
end: 2015-11-05 22:00:00 
event_url: http://www.meetup.com/haagsemakers/events/226001362/ 
organizers: ["haagse_makers"]
locations: ["icx"]
featuredImage: makerscafe.png
---

The next Makerscafe will be the first since the Haagse makers festival! It will be an epic collaboration between OSHWA (Open Source Hardware Association), Permanent Future Lab and Haagse Makers! The full agenda is TBA but here's what we have planned so far: - A hackers corner for prototyping - Maker's Show and Tell - Special guest speakers on recent Open Hardware wins! Do you have a great project or plan, and you want to share the maker love? Let us know in the comments below, and bring your projects on the night! [Aanmelden via Meetup](http://www.meetup.com/haagsemakers/events/226001362/)!