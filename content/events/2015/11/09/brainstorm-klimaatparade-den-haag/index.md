---
type: event
path: /event/brainstorm-klimaatparade-den-haag
title: 'Brainstorm Klimaatparade Den Haag'
date: 2019-10-01
tags: ["brainstorm","den haag in transitie","Events","klimaatparade"]
start: 2015-11-09 21:00:00 
end: 2015-11-09 23:00:00 
event_url: denhaagintransitie.nl 
organizers: ["den_haag_in_transitie"]
locations: ["nutshuis"]
featuredImage: klimaatparade-homepage-dit-willen-wij-01.png
---

 

> Op 29 november gaan overal ter wereld mensen de straat op om politici op te roepen tot een serieuze aanpak van klimaatverandering. Wij als Haagse buurtinitiatieven, groene bedrijven, kritische kunstenaar en vele andere Hagenezen hebben visie voor een duurzame toekomst! Kom mee brainstormen over hoe we die samen gaan laten zien op 29 november tijdens de Klimaatparade in Amsterdam!