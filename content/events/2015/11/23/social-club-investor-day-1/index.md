---
type: event
path: /event/social-club-investor-day-1
title: 'Social Club Investor Day 1'
date: 2019-10-01
tags: ["Events","pitch","sociaal ondernemen","social business","startup"]
start: 2015-11-23 17:00:00 
end: 2015-11-23 20:30:00 
event_url: http://www.socialclubdenhaag.nl/investordays/ 
organizers: ["social_business_club"]
locations: ["nutshuis"]
featuredImage: logo1.png
---

### Meld jouw business idee aan

[http://www.socialclubdenhaag.nl/idee-inbrengen/ ](http://www.socialclubdenhaag.nl/investordays/)

> Het eerst volgende evenement is het Social Club Investor Days event op 23 november i.s.m. Fonds 1818. Hiervoor zijn we op zoek naar sociale business plannen die een financieringsbehoefte tussen de 200.000 euro en 1 miljoen euro nodig hebben. In onderstaand formulier kan je een document toevoegen waarin deze financieringsbehoefte wordt verantwoord. Fonds 1818 biedt ondernemers met kansrijke sociale businessplannen de mogelijkheid van een lening of participatie van 200.000 tot 1 miljoen. De financieringsvorm ligt niet vast, het kan een rentedragende lening, aandelenovereenkomst of elke vorm daartussen zijn. Het gaat om een op-maat-financiering, overeen te komen tussen Fonds 1818 en de ondernemer(s) zelf. Bij de beslissing over de financiering wordt rekening gehouden met criteria, zoals: - de sociale doelstellingen die worden nagestreefd - de haalbaarheid van het plan - de risico's - de kwaliteit en ervaring van de ondernemer(s). De financieringsvoorwaarden zijn vergelijkbaar maar zeker niet scherper dan bij een financiële instelling als een bank.