---
type: event
path: /event/border-sessions
title: 'Border Sessions'
date: 2019-10-01
tags: ["international","technology"]
start: 2015-11-11 00:00:00 
end: 2015-11-12 23:59:59 
event_url: bordersessions.org 
organizers: ["border_session"]
locations: ["theater_aan_het_spui"]
featuredImage: jeffrey-dachis.jpg
---

Border Sessions Festival is located in downtown The Hague in one of the most popular theatres in the city Theater aan het Spui and the local art-house movie theatre Filmhuis Den Haag. In the heart of the city, all just footsteps away from numerous hotels so you can stay overnight.