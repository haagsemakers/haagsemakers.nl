---
type: event
path: /event/trashhunting-time-at-border-sessions
title: 'Trashhunting time at Border Sessions'
date: 2019-10-01
tags: ["Border Sessions","Events","plastic soup","trashhunters"]
start: 2015-11-12 12:00:00 
end: 2015-11-12 13:00:00 
organizers: ["border_session"]
locations: ["theater_aan_het_spui"]
---

[![Trash-Hunters-Logo-lang-zwart-300-75](http://haagsemakers.nl/wp-content/uploads/2015/10/Trash-Hunters-Logo-lang-zwart-300-75-300x75.png)](http://www.trashhunters.org/) **It's Trashhunting time!** Op 12 november gaan we tussen 12:00 en 13:00 de stad in om samen met de [Plastic Soup Foundation](http://www.plasticsoupfoundation.org/) het straatafval in de Haagse binnenstad op een hoop te gooien. Daniel en Jeroen Van PSF spreken die dag op [Border Sessions](http://bordersessions.org) over hun missie. Daarna geven ze ook een workshop [trashhunting](http://www.trashhunters.org/). Kijk voor meer info op [http://www.trashhunters.org/](http://www.trashhunters.org/) Je hoeft geen Border Sessions festival ticket te hebben voor deelname aan de workshop. We verzamelen in de foyer van Theater aan het Spui en gaan dan op jacht naar trash in de Haagse Binnenstad.