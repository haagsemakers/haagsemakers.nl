---
type: event
path: /event/challenge-haagse-vernieuwers-2016-deadline
title: 'Haagse Vernieuwers Challenge 2016 Deadline'
date: 2019-10-01
tags: ["challenge","Deadlines","gemeente den haag","Haagse Vernieuwers","impact economy"]
start: 2015-11-02 09:00:00 
end: 2015-11-02 10:00:00 
event_url: http://www.denhaag.nl/home/bedrijven-en-instellingen/to/Challenge-Haagse-Vernieuwers-2016.htm 
organizers: ["impact_economy_gemeente_den_haag"]
locations: ["0"]
featuredImage: den-haag-stadswapen.png
---

Vandaag is de deadline voor de Challenge Haagse Vernieuwers [http://www.denhaag.nl/home/bedrijven-en-instellingen/to/Challenge-Haagse-Vernieuwers-2016.htm](http://www.denhaag.nl/home/bedrijven-en-instellingen/to/Challenge-Haagse-Vernieuwers-2016.htm)

Over Haagse Vernieuwers
-----------------------

> Haagse Vernieuwers is een wedstrijd waarmee de gemeente Den Haag organisaties uitdaagt om vernieuwende ideeën te presenteren rond maatschappelijke vraagstukken. Concepten die verschillende disciplines en sectoren verbinden, hebben de voorkeur. De beste ideeën maken kans op een geldprijs tussen de €5.000 en €35.000.