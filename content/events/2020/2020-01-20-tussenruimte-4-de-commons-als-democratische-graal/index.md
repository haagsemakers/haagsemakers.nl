---
type: event
path: "/event/tussenruimte-4-commons-als-democratische-graal"
title: Tussenruimte 4 - De commons als democratische graal?
date: 2020-01-12T23:00
tags:
- tussenruimte
- emma
- commons
start: 2020-01-28T16:30
end: 2020-01-28T18:30
event_url: 'https://www.emma.nl/agenda/4-de-commons-als-democratische-graal'
organizers: ["de_tussenruimte"]
locations: ["de_tussenruimte"]
featuredImage: tine-de-moor.jpg

---
**Prof. dr. Tine de Moor** in gesprek met journalist **Bas Mesters**. Over de commons: burgercollectieven tussen falende markt en terugtrekkende overheid.

Van deelauto’s tot zorgcoöperaties, van stadsboerderijen tot de aanschaf van een windmolen met de wijk of buurt – commons komen in vele vormen en maten op. Voor burgers is dit een mogelijkheid om zelf het beheer over te nemen. Om hun rol te pakken in het gat dat is ontstaan door het falen van de vrije markt en de terugtredende overheid.

Het lijkt een innovatie. Samenwerken in coöperaties aan de leefbaarheid, inclusiviteit en duurzaamheid van dorp of wijk. En dat is het ook. Maar nieuw is het niet, zo leert het onderzoek van prof. dr. Tine de Moor, de volgende spreker in de serie Nieuwe Democratie. Twee eeuwen terug was het Nederlandse landschap bezaaid met kleine lappen grond in handen van de gemeenschap. Plekken om vee te laten grazen, turf te steken of brandhout te hakken.

Wat kunnen de nieuwe coöperaties leren van de oude? Waarom komen ze weer op? Wat werkt en wat niet? In hoeverre kan het de democratie vernieuwen en versterken? En: wat is de rol van de overheid in het succes (of falen) van de commons?

**Waar en wanneer?** Dinsdag 28 januari 17.30 tot 19.00 uur – De Tussenruimte bij EMMA, Wijnhaven 88 Den Haag.   
Inloop vanaf 17:00, we sluiten het programma af met een borrel.

**Waarover?** Een lezing en debat over burgerparticipatie in de vorm van burgercollectieven. Coöperaties die de leemte tussen de terugtredende overheid en het falen van de vrije markt proberen in te vullen.

**Met Wie?**

* **Prof. dr. Tine de Moor**, hoogleraar sociale en economische geschiedenis aan de Universiteit Utrecht. De Moor doet onderzoek naar het ontstaan, functioneren en evolueren van ‘instituties voor collectieve actie’. Meer info: [www.collective-action.info](http://www.collective-action.info/)
* Diverse vertegenwoordigers van energiecoöperaties.
* **Bas Mesters**, gespreksleider, journalist en schrijver van de serie [Wie is wij?](https://www.groene.nl/series/wat-bindt-europa) in de Groene Amsterdammer. Bas is initiator/coördinator van de Tussenruimte en programmamaker van de debatserie Nieuwe Democratie.
* Live column door **Aukje van Roessel**, politiek redacteur van de Groene Amsterdammer.