---
type: event
path: "/event/opening-pop-up-atelier-100000-bomen-en-een-bos-van-draad"
title: Opening pop-up atelier 100.000 bomen en een bos van draad
date: 2020-01-15T23:00
tags:
- 100.000 bomen
- pop-up atelier 
- participatieve kunst
start: 2020-01-22T13:00
end: 2020-01-22T18:00
event_url: https://www.honderdduizendbomen.nl
organizers: ["studio_vrugt"]
locations: ["centrale_bibliotheek"]
featuredImage: iHoa6ltLzj86sZ8W.jpeg
---

**Draag bij aan een groot kunstproject en kom in beweging voor de natuur!**
Met dit participatieve project viert kunstenaar Sara Vrugt de natuur. In 2020 borduurt zij samen met vele anderen een bos van honderd vierkante meter.
Dit kunstwerk wordt gemaakt in een pop-up atelier dat vier seizoenen lang door het land reist, iedereen is er welkom om de mooiste borduursteken te leren of een persoonlijk natuurverhaal te vertellen. Loop binnen en doe mee!

Vandaag opent het pop-up atelier opent haar deuren. 

Honderd vierkante meter stof, een fijne vertelstoel en een geweldig team staan dan klaar om samen met jou van start te gaan!
Wees welkom bij de eerste steek op 22 januari om 13 uur in de Centrale Bibliotheek in Den Haag.

Ik wil je hartelijk danken voor alle aandacht (in welke vorm ook) die jij in Honderdduizend bomen steekt. Samen brengen we dit bijzondere project tot leven!
