---
type: event
path: "/event/odyssey-connect"
title: Odyssey Connect
date: 2020-01-12T23:00
tags:
  - odysey
  - hackathon
start: 2020-02-04T10:00
end: 2020-02-05T23:00
event_url: "https://connect.odyssey.org/"
organizers: ["odyssey"]
locations: ["haagse_hogeschool"]
featuredImage: ./odyssey-connect-2020.png
---
We can all sense the stirring of a breeze. Massive changes in our social-economic-environmental context are building up. How are we part of these changes? Even when you work in tech, your product is not just part of a tech revolution. Your work, your team and yourself are actually part of a deep, profound transformation of our society, economy that is joined by a shift in the personal and collective consciousness.

So what’s in here for you?

* Odyssey 2020: _21 challenges to shape the 21st century_. Each challenge is a rabbit hole of opportunities to open up new markets, land clients, and contribute to reaching the UN Sustainable Development Goals. 2000 participants, 21 challenges, 105 teams. Over € 200.000 in cash rewards. Our partners are better than ever, prepared to pilot winning solutions after the hackathon in April. Trailer: [https://www.youtube.com/watch?v=rsINLKEEK9Y](https://www.youtube.com/watch?v=rsINLKEEK9Y "https://www.youtube.com/watch?v=rsINLKEEK9Y")
* All previous deep dives are now converged into one preliminary event with a freakin amazing speaker lineup… Odyssey Connect.

**On the first day** at Odyssey Connect, you can meet with the challenge partners and their stakeholders (clients, users, competitors, partners, etc), to learn about the problem behind their challenge and why it is crucial that we solve it. The cutting edge of open innovation in Europe.

![deep dive.jpg](https://mail.google.com/mail/u/0?ui=2&ik=4026227636&attid=0.1&permmsgid=msg-f:1655537604202528737&th=16f9a6adfacaffe1&view=fimg&sz=s0-l75-ft&attbid=ANGjdJ87sEFWk6VLju5d0Knax7m-Sqnh9xCC31aKEw_AFsyFZUs7Q6-RMpzTY0uPmgYcKumJeQzgI2Msss1i3t-BwamFwDVwmoCVQI_4wwWy6A5yxMIBgJXplRepNHM&disp=emb&realattid=ii_k5b6jvdz0 =169x142)

**On the second day**, we will be treated by top-notch specialists from all over the world with the latest insights in the field of decentralized systems, ecosystem & protocol building, AI, Identity, but also team performance, business community culture building, and legal frameworks. For example:

* Christopher Allen - Founder #RebootingWebOfTrust, Co-chair W3C Credentials CG (DIDs)
* Audrey Tang - Digital Minister, Taiwan
* Trent McConaghy - Co-founder & CTO, Ocean Protocol
* Jamie Burke - Founder & CEO, Outlier Ventures
* Michael Zargham - Founder & CEO, BlockScience
* Matan Field - Founder & CEO, DAO Stack
* Gintaras Pelenis - Blockchain Lead, VM Ware

As always: Limited seating available. We don’t sell any tickets. All participants are curated.