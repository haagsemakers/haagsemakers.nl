---
type: event
path: "/event/gezocht-stadsstedenbouwer-m-v/"
title: "STAD gesprek: Gezocht: Stadsstedenbouwer m/v"
date: 2025-01-28T23:00
tags:
  - stadgesprek
  - platform stad
  - stadsstedenbouwer
start: 2020-02-11T16:00
end: 2020-02-11T17:30
organizers:
- platform_stad
locations:
- bleyenberg
featuredImage: stadgesprek-stadsstedenbouwer.jpg
event_url: "https://www.platformstad.nl/agenda/gezocht-stadsstedenbouwer-m-v/"
---

In Den Haag is de positie van stadsstedenbouwer vacant. De stadsstedenbouwer adviseert het college en ondersteunt de stedelijke diensten met het oog op maximale ruimtelijke kwaliteit.

In het verleden hebben grote namen als Berlage en Dudok hun stempel gedrukt op de stedelijke ontwikkeling van Den Haag. Afhankelijk van de tijdgeest of de stedelijke opgaven vond supervisie plaats vanuit een onafhankelijke, externe positie of vanuit een meer interne rol.

### Onafhankelijk adviseur en /of stevige interne dienst?
Den Haag staat voor een grote verdichtingsopgave en voor uitdagingen als het gaat om klimaat, bereikbaarheid, economie en samenleven in de stad. Een goed ontwerp van de stad is nu belangrijker dan ooit.

Dat vraagt om visie en regie. Om samenwerking tussen gemeente, ontwikkelaars, woningcorporaties en ontwerpers. En om afstemming tussen Den Haag en de regio.

Wat kan een stadsstedenbouwer daarin betekenen? Welk functieprofiel en welke positie past daarbij? Is Den Haag gebaat bij een onafhankelijke, externe stadsstedenbouwer of juist bij een visionair hoofd van de gemeentelijke dienst?

Platform STAD gaat in gesprek met Max van Aerschot (voormalig stadsbouwmeester Haarlem), Marianne Loof (partner LEVS architecten en voormalig voorzitter van de Commissie Ruimtelijke Kwaliteit gemeente Amsterdam), Erik Pasveer (voormalig stadsstedenbouwer van Den Haag), en Tako Postma (stadsbouwmeester in Delft).

Wat zijn hun ervaringen vanuit hun verschillende rollen en welke aanbevelingen geven zij Den Haag mee voor de toekomst?