---
type: event
path: /event/smaakmakers-lunch
title: Haagse Smaakmakers Lunch
date: 2019-10-01
tags:
  - Food
start: 2020-01-17T12:00
end: 2020-01-17T14:00
organizers:
  - haagsemakers
locations: 
  - het_koorenhuis
featuredImage: smaakmakers.jpg
---

Leve de Haagse Smaakmaker! Haagse Smaakmakers zijn de makers die zich richten op Haagse voedselproducten. Door lokaal te produceren, verkopen en samen te eten verterken we de lokale economie, verbondenheid en dragen we bij aan duurzame ontwikkeling.

We starten op dinsdag 10 december met de eerste Smaakmakerslunch. Tijdens de lunch eten we lokaal voedsel, door een Haagse chefkok, en krijgen Haagse Makers een podium om hun verhaal over voedsel te vertellen en wat het betekent voor verschillende beleidsdoelen als je lokaal gaat eten (hoe je voetafdruk verkleint ten opzichte van ‘normaal’). Centraal staat de vraag hoe we hierbij het beste kunnen samenwerken. Wat is er nodig om met elkaar het beste resultaat te bereiken?

Daarnaast wordt op beeldende wijze het verhaal verteld over de samenhang tussen klimaattransitie, voedsel, hoe je je organiseert in de stad en wat de gemeente hierin kan doen. Van abstracte doelen naar concrete stappen.

De Smaakmakers Lunch is op uitnodiging. Ben je een Haagse Smaakmaker, wil je meedoen? Neem dan [contact](/contact) op!

Locatie: [in het Koorenhuis, Huis voor Makers](http://inhetkoorenhuis.nl/).
