---
type: event
path: "/event/bijeenkomst-bewonersbedrijven-aan-zet-in-den-haag"
title: Bijeenkomst Bewonersbedrijven aan zet in Den Haag
date: 2025-01-12T23:00
tags:
  - cooperatie
  - lsa
start: 2020-02-19T19:30
end: 2020-02-19T21:30
event_url: "https://www.greenwish.nl/agenda/bewonersbedrijven-aan-zet-in-den-haag/"
organizers:
- gemeente_den_haag
locations:
- "moerwijk_cooperatie"
featuredImage: bewonersbedrijven.png
---
Bewonersbedrijven  zijn in trek, omdat bewonersbedrijven taken soms goedkoper kunnen doen dan andere professionals en omdat ze bijdragen aan sterke buurten. Op 19 februari staat dit onderwerp centraal tijdens de bijeenkomst Bewonersbedrijven aan zet in Den Haag. Locatie: Moerwijk Coöperatie, Jan Luykenlaan 92a in Den Haag van 19.15 uur tot 21.30 uur.

Bewonersbedrijven ondernemen allerlei activiteiten, sommige gericht op duurzame ontwikkeling, sommige (ook) op sociale cohesie of leefbaarheid. Kenmerk is dat ze werken op een ondernemende manier en dat het de samenhang in de wijk versterkt. In het Haagse Moerwijk zit [Moerwijk Coöperatie](https://moerwijkcooperatie.nl), een bewonersbedrijf, bewonersplatform en bewonersorganisatie in één. Moerwijk Coöperatie ondersteunt bewoners om meer zeggenschap te krijgen over hun fysieke en sociale leefomgeving. De coöperatie wil het weglekken van geldstromen tegen gaan, door de belangen van bewoners bij de gemeente en zorg- en welzijnsinstellingen goed te vertegenwoordigen. Zij wil het werk in de wijk zo veel mogelijk en vanuit zelforganisatie door bewoners van de wijk laten uitvoeren. In de praktijk betekent dat, dat bijvoorbeeld gekeken wordt welke werkzaamheden de gemeente of welzijnsorganisatie, kunnen worden overgenomen door bewoners, tegen betaling. Wat kunnen maatschappelijke initiatieven leren van de ondernemende opstelling van bewonersbedrijven?

Neo de Bono, initiatiefnemer van Moerwijk Coöperatie vertelt over zijn ervaringen. Daarnaast zal Rutger van Weeren van [LSA bewoners](https://www.lsabewoners.nl) inspiratie bieden uit andere delen van het land. Rutger deed onderzoek in Den Haag en schreef in 2017 samen met Kiemkracht ‘[Nieuwe Perspectieven op de Haagse Wijkonderneming](https://www.lsabewoners.nl/onderzoek-nieuwe-perspectieven-op-haagse-wijkonderneming/)’. Via interviews met initiatiefnemers achter vier bewonersbedrijven onderzochten zij vanuit de praktijk wat wel en niet werkt bij het ondernemen in de wijk.  Ze vertaalden de uitkomsten naar een aantal aanbevelingen voor de gemeente Den Haag. Tijdens de bijeenkomst bekijken we samen welke aanbevelingen ook nu nog actueel zijn. En we kijken aan de hand van nieuw onderzoek van LSA naar de toekomst van deze beweging.

De bijeenkomst wordt georganiseerd vanuit de subsidieregeling Duurzame Haagse Wijkactie. Iedereen met interesse in duurzaamheid en/of bewonersbedrijven is van harte welkom. Deelname is gratis, graag wel van te voren aanmelden via info@greenwish.nl

Programma:

    19.15   Inloop
    19.30  Welkom
    19.40  Bewonersbedrijven: Rutger van Weeren en Neo de Bono
    20.20  Zeepkist moment: gelegenheid om over jouw initiatief te vertellen
    20.45  Netwerkborrel
    21.30  Afsluiting