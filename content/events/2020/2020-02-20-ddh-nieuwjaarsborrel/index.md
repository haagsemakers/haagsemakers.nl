---
type: event
path: "/event/ddh-nieuwjaarsborrel"
title: Duurzaam Den Haag Nieuwjaarsborrel 2020
date: 2025-01-20T23:00
tags:
  - duurzaam den haag
  - nieuwjaarsborrel
start: 2020-01-30T17:00
end: 2020-01-30T19:00
organizers:
- duurzaam_den_haag
locations:
- het_nutshuis
featuredImage: ddh-nieuwjaarsborrel.png
---

## Duurzaam Den Haag nodigt je van harte uit voor de Nieuwjaarsbijeenkomst 2020 

In het afgelopen jaar hebben we samen met vele bewoners stappen gezet naar een duurzamer Den Haag. Graag willen we stilstaan bij de resultaten van 2019 met de mensen die dit mogelijk hebben gemaakt. En we kijken vooruit naar onze plannen voor het nieuwe jaar met de mensen waarmee we dit gaan realiseren.
 
Onze plannen voor 2020 dragen nog meer bij aan een groene, leefbare en duurzame stad. Een stad waarin iedereen doet wat zij of hij kan om Den Haag een stukje duurzamer te maken. Een stad waarin het fijn leven is voor alle Hagenaars. Daarom willen we jou graag van harte uitnodigen om gezamenlijk het nieuwe jaar af te trappen bij de Nieuwjaarsbijeenkomst op donderdag 30 januari.

Ontmoet andere duurzame doeners, verbreed je groene netwerk, geef jouw duurzame idee de boost die het nodig heeft of maak kennis met ons en ons aanbod. 

Programma

17.00 uur: Inloop
17.45 uur: Welkom en toespraak Heleen Weening, directeur stichting Duurzaam Den Haag
19.00 uur: Einde
 
Mocht je dat nog niet gedaan hebben zouden we het fijn vinden als je jezelf aanmeldt via ikbenerbij@duurzaamdenhaag.nl