---
type: event
path: "/event/oc070-inspiratie-netwerkevent-februari-editie"
title: OC070 Inspiratie- en netwerkevent - februari editie
date: 2020-01-28T23:00
tags:
  - oc070
  - netwerkevent
start: 2020-02-05T09:00
end: 2020-02-05T11:00
event_url: "https://www.meetup.com/oc070netwerkevent/events/267875110/"
organizers:
- oc070
featuredImage: highres_488123422.jpg
---

## Details
Op deze 1e woensdag van deze maand weer een OC070 netwerkevenement zoals je gewend bent, inspirerend / verbindend en gezellig. Echter vanaf 5 februari 2020 op een nieuwe locatie: ** De Compagnie in Rijswijk (randje Den Haag) **. Helaas kunnen wij onze bijeenkomsten niet meer organiseren in de BINK Rooftop (is inmiddels The Harbour Club geworden). Maar met De Compagnie hebben wij een fantastische nieuwe locatie waar wij onze meetings kunnen voortzetten! Met daarbij nog steeds gratis parkeren, gratis toegang en voldoende ruimte om onze gasten te ontvangen.

Kom naar ons evenement OC070 en ontmoet ondernemers die jou mogelijk kunnen helpen om jouw doelen te bereiken. Of kom in contact met iemand die jij kunt helpen? Wil jij jouw netwerk vergroten? Met andere ondernemers sparren? Geïnspireerd worden of anderen inspireren? Kom dan 5 februari naar De Compagnie.

OC070 is het grootste gratis* netwerkevenement van Den Haag / Rijswijk waar jij in contact komt met zeer diverse ondernemers / ZZP'ers / MKB'ers / solopreneurs / Entrepreneurs uit Den Haag en omgeving.

PITCHEN
Ook op de nieuwe locatie kun jij een dienst / product / evenement onder de aandacht brengen via de pitch. Wij bieden een aantal ondernemers de mogelijkheid om op een klein podium een presentatie / pitch te geven.

WORKSHOP / INSPIRATIE
Op 5 februari komt Erwin Wils een bijzondere workshop geven met de titel: "Haal meer uit jezelf dan je denkt dat erin zit". Deze workshop gaf hij ook tijdens ons XXL evenement en de deelnemers waren zeer enthousiast. Zonder dat we het door hebben, beperken we ons in ons presteren. Dat is er met de paplepel ingegoten: “Doe maar gewoon, dan doe je gek genoeg, je moet je hoofd niet boven het maaiveld uitsteken”, en nog meer van dat soort mooie uitspraken. Aangezien dat aangeleerd gedrag is, kunnen we dat ook veranderen. De eerste stap is bewustwording. Wil je weten èn ervaren hoe? Kom dan naar deze workshop.

PROGRAMMA
• 9:00 deur open
• 9:30 pitches en inspiratie sessie
• 10:00 vervolg ontmoetingen met collega ondernemers
• 11:00 aan het werk of nog even blijven hangen

Met oog op de organisatie en de inkoop van drankjes / koffie het verzoek je snel aan te melden.

Neem ook even een kijkje op onze website: https://www.OC070.nl/ - en volg ons via https://www.facebook.com/OC070

We kijken er naar uit om elkaar weer te zien en te spreken,

Onno, Jennifer, Karen, Meggie en Arne

* de toegang en het parkeren zijn gratis, lekkere koffie/thee voor eigen rekening