---
type: event
path: /event/maker-cafe-8
title: 'Maker Cafe #8'
date: 2019-10-01
tags: ["Events","Haagse Makers","maker cafe"]
start: 2016-02-04 19:00:00 
end: 2016-02-04 21:00:00 
event_url: http://www.meetup.com/haagsemakers/events/227923336/ 
organizers: ["haagse_makers"]
locations: ["bazaar_of_ideas"]
featuredImage: makerscafe.png
---

The Maker Café is the monthly show and tell by and for makers. Do you create things? Or would you like to? Do you like to exchange knowledge, get to know awesome projects & show what you are working? **See you on the first Thursday of the month!** It's an open agenda, when you want to present something there are 10 minute slots to present and discuss. http://www.meetup.com/haagsemakers/events/227923336/