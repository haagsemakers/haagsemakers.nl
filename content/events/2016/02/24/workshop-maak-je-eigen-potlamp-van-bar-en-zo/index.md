---
type: event
path: /event/workshop-maak-je-eigen-potlamp-van-bar-en-zo
title: 'Workshop Maak je eigen Potlamp van Bar en Zo'
date: 2019-10-01
tags: ["diy","handmade","jarlight","lighting","potlamp","reused","workshop","Workshops","zeeheldenkwartier"]
start: 2016-02-24 19:00:00 
end: 2016-02-24 22:00:00 
event_url: https://www.facebook.com/bar.enzo.denhaag/?fref=nf 
organizers: ["bar_en_zo"]
locations: ["bar_en_zo"]
featuredImage: 12694487-1001235933283522-2441664672951268684-o.jpg
---

Woendag 24 feb vanaf 19.00 geven wij een workshop Potlamp maken van oude kappertjes potten en strijkijzersnoer ! Wil jij mee doen stuur dan een email naar bar.enzo.denhaag@gmail.com dan sturen wij de info door! (Kosten €45 incl cake en koffie ! [‪#‎workshop‬](https://www.facebook.com/hashtag/workshop?source=feed_text&story_id=1001235933283522) [‪#‎denhaag‬](https://www.facebook.com/hashtag/denhaag?source=feed_text&story_id=1001235933283522) [‪#‎handmade‬](https://www.facebook.com/hashtag/handmade?source=feed_text&story_id=1001235933283522)[‪#‎reused‬](https://www.facebook.com/hashtag/reused?source=feed_text&story_id=1001235933283522) [‪#‎zeeheldenkwartier‬](https://www.facebook.com/hashtag/zeeheldenkwartier?source=feed_text&story_id=1001235933283522) [‪#‎diy‬](https://www.facebook.com/hashtag/diy?source=feed_text&story_id=1001235933283522) [‪#‎lighting‬](https://www.facebook.com/hashtag/lighting?source=feed_text&story_id=1001235933283522) [‪#‎jarlights‬](https://www.facebook.com/hashtag/jarlights?source=feed_text&story_id=1001235933283522) [‪#‎potlamp‬](https://www.facebook.com/hashtag/potlamp?source=feed_text&story_id=1001235933283522) https://www.facebook.com/bar.enzo.denhaag