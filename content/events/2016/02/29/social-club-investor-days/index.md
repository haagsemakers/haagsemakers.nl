---
type: event
path: /event/social-club-investor-days
title: 'Social Club Investor Days'
date: 2019-10-01
tags: ["Events","social business"]
start: 2016-02-29 17:00:00 
end: 2016-02-29 20:00:00 
event_url: http://www.socialclubdenhaag.nl/investordays/ 
organizers: ["social_business_club"]
locations: ["nutshuis"]
featuredImage: unnamed.jpg
---

Social Club Den Haag en Fonds 1818 organiseren in samenwerking de Social Club Investor Days. Twee unieke events in serie en een versnellingstraject voor sociaal ondernemers die op zoek zijn naar financiering tussen 200.000 en 1.000.000 euro. Een uitgelezen kans voor sociale ondernemers die op zoek zijn naar FUNDING voor hun (toekomstige) sociale onderneming. Social Club Den Haag en Fonds 1818 bundelen hiervoor hun kennis, netwerk en ervaring om Sociaal Ondernemen in de regio Den Haag te stimuleren. 

Op 29 februari 2016 is de tweede Social Club Investor Day. 16:30 inloop – 20:30 uur einde (inclusief borrel en voldoende hapjes) in het Nutshuis. Vier overgebleven ondernemers pitchen op deze middag hun verbeterde business plannen aan de jury en krijgen te horen of zij in aanmerking komen voor de financiering!