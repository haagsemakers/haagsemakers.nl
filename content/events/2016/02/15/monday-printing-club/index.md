---
type: event
path: /event/monday-printing-club
title: 'Monday Printing Club'
date: 2019-10-01
tags: ["grafische werkplaats","Workshops"]
start: 2016-02-15 18:00:00 
end: 2016-02-15 20:30:00 
event_url: http://grafischewerkplaats.nl/event-registration/?ee=63 
organizers: ["grafische_werkplaats"]
locations: ["de_grafische_werkplaats"]
featuredImage: 8448351c-528b-483e-908d-a8ca0892b309.jpg
---

In 1,5 uur een zeefdrukje maken? Een uitdaging!

Monday Printing Club is kort en krachtig en het is proeven aan een techniek. 18.00-19.30 en 8,- per keer