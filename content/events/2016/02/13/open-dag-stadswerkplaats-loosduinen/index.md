---
type: event
path: /event/open-dag-stadswerkplaats-loosduinen
title: 'Open dag stadswerkplaats Loosduinen'
date: 2019-10-01
tags: ["Events","Loosduinen","Open dag","Stadswerkplaats"]
start: 2016-02-13 11:00:00 
end: 2016-02-13 15:00:00 
event_url: https://www.facebook.com/StadswerkplaatsDenHaag/ 
organizers: ["stadswerkplaats_loosduinen"]
locations: ["stadswerkplaats_loosduinen"]
featuredImage: 12646848-523532894484262-2814665122444283352-o.jpg
---

Open dag stadswerkplaats Loosduinen:

> Er is druk gesopt, gezeemd, getuinierd en gesjouwd om het er morgen mooi uit te laten zien. We hopen op veel belangstelling. Er is veel te zien: er wordt gezeefdrukt, een bakfiets wordt gerestaureerd en de meubelmaker heeft een meubel in de verkoop. Er kan op geboden worden. De inmiddels befaamde vogelhuisjes zijn te koop, zakdoekjes met logo en kleine doosjes met inhoud.