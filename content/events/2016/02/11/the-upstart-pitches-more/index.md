---
type: event
path: /event/the-upstart-pitches-more
title: 'UpStart: Pitches & More..'
date: 2019-10-01
tags: ["bazaarofideas","Events","startup","upstart"]
start: 2016-02-11 17:00:00 
end: 2016-02-11 20:00:00 
event_url: https://www.eventbrite.co.uk/e/the-upstart-pitches-more-tickets-21082170339 
organizers: ["startup_den_haag"]
locations: ["bazaar_of_ideas"]
featuredImage: the-up-start-february.jpg
---

The Upstart: Startups, pitches, stories, people.
------------------------------------------------

The upstart is the monthly startup event in The Hague. Enjoy the pitches Meet fellow startups Listen to interesting stories Get in touch with mentors, informals, etc Organized by [Tygers](http://tygers.nl) and [Startup Den Haag](http://startupdenhaag.nl)