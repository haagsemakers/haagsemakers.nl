---
type: event
path: /event/iotuesdays-2016-04-12
title: 'IoTuesdays'
date: 2019-10-01
tags: ["Events","Haagse Makers","IoT","IoTuesdays"]
start: 2016-04-12 17:00:00 
end: 2016-04-12 18:30:00 
organizers: ["haagse_makers"]
locations: ["icx"]
featuredImage: highres-448842293.jpg
---

Iedere twee weken is het IoTuesday. We werken aan verschillende projecten en projectjes, net waar we zin in hebben. Werk met anderen aan je eigen idee of IoT-project of doe mee met een bestaand project. Je bent welkom! Projecten waar nu aan gewerkt wordt: • IoT slim slot • [DIY lora gateway](https://community.haagsemakers.nl/t/iotuesday-project-diy-lora-gateway/82) • Experimenteren met lora sensoren • Leren over nieuwe technologie die wordt gedeeld door het Permanent Future Lab Wat ga jij maken?