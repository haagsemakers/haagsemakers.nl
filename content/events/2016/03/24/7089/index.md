---
type: event
path: /event/7089
title: 'Wij Maken de Stad (Leidschenveen/Ypen­­burg 2040) - Maaksessie 2'
date: 2019-10-01
tags: ["Events","Haagse Makers","Leidschenveen-Ypenburg","Ruimte voor de stad","stadmakers"]
start: 2016-03-24 19:00:00 
end: 2016-03-24 22:00:00 
event_url: http://www.meetup.com/haagsemakers/events/229320515/ 
organizers: ["haagse_makers"]
locations: ["0"]
featuredImage: rvds-header.png
---

Hoe ziet Leidschenveen/Ypenburg eruit in 2040? Maak het mee op de eerste maak-sessie op woensdag 16 maart 2015 om 19:30. We zoeken enthousiaste mensen uit Leidschenveen/Ypenburg die een bijdrage willen leveren aan het project ‘[Agenda Ruimte voor de Stad](http://ruimtevoordestad.nl/)’: de nieuwe ruimtelijke agenda van Den Haag 2040. Samen met de gemeente Den Haag werken we aan de agenda 2040 voor Leidschenveen-Ypenburg. Samen met [Studio Kustlijn](http://studiokustlijn.nl/)en [Part-up](http://part-up.com/) werken we in opdracht van de [gemeente Den Haag](http://denhaag.nl/) aan Ruimte voor de Stad.