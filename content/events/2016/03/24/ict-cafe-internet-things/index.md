---
type: event
path: /event/ict-cafe-internet-things
title: 'ICT-cafe: Internet of Things'
date: 2019-10-01
tags: ["Events","gemeente den haag","ict","ict-cafe","IoT"]
start: 2016-03-24 14:00:00 
end: 2016-03-24 18:00:00 
organizers: ["gemeente_den_haag"]
locations: ["museum_voor_communicatie"]
featuredImage: ict-cafe-den-haag-24032016-003-760x380.jpg
---

Van stoplichten tot medische apparatuur: naar verwachting zijn in 2020 zo’n 28 miljard 'dingen' verbonden aan het web. Welke impact heeft het Internet of Things op Den Haag? Meld u aan voor het ICT-café met dit thema op donderdag 24 maart.

Internet of Things
------------------

Tijdens de eerste internetgolf, in de jaren 90, werden één miljard computers met internet verbonden. In het decennia daarop volgde de mobiele golf en werden nog eens twee miljard apparaten met het internet verbonden. Het Internet of Things (IoT) wordt gezien als de derde golf. Naar verwachting zijn in 2020 maar liefst 28 miljard 'dingen' aan het web verbonden. IoT maakt bijvoorbeeld mogelijk dat:

*   Auto’s automatisch remmen als verderop een ongeluk is gebeurd.
*   Een fietsbel een signaal geeft wanneer er sprake is van ernstige luchtvervuiling.
*   Apparaten, geïmplanteerd in het menselijk lichaam, medicatie toedienen als dat nodig is.

Met elkaar in gesprek
---------------------

Wat precies de impact van het Internet of Things zal zijn, is heel moeilijk te voorspellen. De verwachting is dat IoT nog meer invloed op ons dagelijkse leven zal hebben dan de introductie van het internet. Daarom is het goed om met elkaar in gesprek te gaan over de kansen en de uitdagingen van het IoT in relatie tot de stad Den Haag.

Programma ICT-café
------------------

Het mini-symposium begint met een mini-symposium. Daarna volgt verder verdieping tijdens de Smart Tables, pitches en de bedrijvenmarkt. Het doel van de dag is niet zozeer het vinden van de waarheid, maar wel inzicht vergaren en gezamenlijke issues adresseren.

### 14.00-16.00 uur: Mini-symposium ‘Internet of Things in de stad Den Haag’

De ingrediënten van het mini-symposium: experts van onder meer KPN, de Haagse Hogeschool, De Data Coöperatie en de gemeente Den Haag, een moderator, een paneldiscussie en een drietal vragen:

*   **Wat is het IoT?** Wat is het IoT is in de context van een stad, specifiek Den Haag?
*   **Wat kunt u ermee?** Het fenomeen IoT gebeurt nu en groeit iedere dag, gewoon in uw huis of wijk. Technisch gezien is een IoT-toepassing ook relatief simpel te realiseren. Sensoren, computeronderdelen en dataverbindingen zijn volop aanwezig, tegen een veel lagere kostprijs dan een paar jaar geleden. Wat zijn de uitdagingen en vraagstukken in Den Haag die profijt kunnen hebben van een IoT-toepassing?
*   **Wat is ervoor nodig?** Een IoT-toepassing is meer dan alleen de technische onderdelen in elkaar schroeven. Een goede commerciële of maatschappelijke business-case en een ontwerp dat veilig omgaat met data en respect voor persoonsgegevens zijn slechts enkele 'bouwstenen' waar aandacht voor nodig is. U zult merken dat er behoorlijk wat bij komt kijken.

### 16.00-18.00 uur: Smart Tables, pitches en standhoudersmarkt

Na het symposium is het tijd om de diepte in te duiken en te netwerken. Schuif aan bij de verschillende Smart Tables en ga in gesprek over de mogelijkheden en gevolgen van IoT in Den Haag, op het gebied van bijvoorbeeld duurzaamheid, zorg, veiligheid en mobiliteit. Luister naar de pitches, bezoek de standhoudersmarkt  en ontdek: wat is er ‘hot’ op het gebied van IoT in de regio? Zodra er meer details bekend zijn over de Smart Tables, de pitches en standhoudersmarkt, vullen we de informatie op deze pagina aan.