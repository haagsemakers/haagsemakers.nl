---
type: event
path: /event/iotuesday-1-maart-2016
title: 'IoTuesday (1 maart 2016)'
date: 2019-10-01
tags: ["Events","Haagse Makers","IoT","IoTuesday"]
start: 2016-03-01 17:00:00 
end: 2016-03-01 19:00:00 
event_url: http://www.meetup.com/haagsemakers/events/229037722/ 
organizers: ["haagse_makers"]
locations: ["icx"]
---

We will continue the IoTuesday where we left off last time: a lot of interesting ideas to work on were presented by people in the group. We will streamline these under a generic theme which we called "Open Source Living", relating to IoT in and around the house, mobility, air quality, The Things Network and the like. Then we will decide together what is interesting to work on in 2016, or at least for the coming months.