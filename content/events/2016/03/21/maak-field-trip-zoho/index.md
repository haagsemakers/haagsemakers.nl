---
type: event
path: /event/maak-field-trip-zoho
title: '[Concept] Maak Field Trip ZoHo'
date: 2019-10-01
tags: ["Events","Haagse Makers","makerskwartier","rotterdam","zoho"]
start: 2016-03-21 16:00:00 
end: 2016-03-21 20:00:00 
organizers: ["haagse_makers"]
locations: ["zoho"]
featuredImage: route-du-nord201401.jpg
---

Field Trip naar ZoHo Rotterdam! Meer info volgt zsm