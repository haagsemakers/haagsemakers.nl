---
type: event
path: /event/informatie-avond-stadswerkplaats-loosduinen
title: 'Informatie avond Stadswerkplaats Loosduinen'
date: 2019-10-01
tags: ["Events","Loosduinen","Stadswerkplaats"]
start: 2016-03-23 19:00:00 
end: 2016-03-23 21:00:00 
event_url: https://www.facebook.com/StadswerkplaatsDenHaag 
organizers: ["stadswerkplaats_loosduinen"]
locations: ["stadswerkplaats_loosduinen"]
featuredImage: 12829017-536315109872707-6672220195201612626-o.jpg
---

Woont u in Loosduinen/ Nieuw Waldeck/ Houtwijk, zit u al langere tijd thuis en zou u graag (weer) aan de slag gaan? Zou u graag willen ondernemen maar kunt u daarbij wel wat hulp gebruiken? Stadswerkplaats Den Haag/Loosduinen organiseert samen met Ontwikkelingsorganisatie Cordaid op woensdag 23 maart een informatie avond over samen ondernemen in een sociale coöperatie. In een sociale coöperatie kunnen ondernemende wijkbewoners een eigen bedrijf opzetten. Cordaid adviseurs leggen uit hoe ondernemen in een coöperatie in zijn werk gaat. Ook presenteren zij voorbeelden uit andere steden. Een ondernemer van de Stadswerkplaats legt uit welke bedrijven al bestaan op de Narcislaan 4 in Loosduinen. Op de Narcislaan zijn bewoners al sinds november 2015 met de Stadswerkplaats Loosduinen bezig. VÓÓR Welzijn, Stek voor Stad en Kerk, gemeente Den Haag stadsdeel Loosduinen en ook Cordaid ondersteunen Stadswerkplaats Den Haag / Loosduinen. Mogelijke nieuwe ondernemers kunnen aansluiten bij de bestaande timmer werkplaats, en/of de fietsenmaker. Ook kunnen er bedrijfjes starten op het gebied van persoonlijke verzorging, catering, upcycling, 2e hands zorgdiensten/materialen en andere diensten. Cordaid in Nederland creëert kansen voor en met werkzoekenden met en zonder uitkering. De organisatie ondersteunt hen met de oprichting van sociale coöperaties, waarbinnen mensen met behoud van uitkering kunnen ondernemen. Ben jij een (nieuwe) ondernemer en wil je weten of dit iets voor je is? Geef je snel op en kom dan op woensdag 23 maart van 19 - 21 uur naar Narcislaan 4 In Loosduinen. Geef je op bij: Kees Buist kbuist@stekdenhaag.nl