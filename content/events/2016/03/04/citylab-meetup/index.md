---
type: event
path: /event/citylab-meetup
title: 'Citylab Meetup at iForesee'
date: 2019-10-01
tags: ["citylab","Events","iforesee","meetup"]
start: 2016-03-04 18:00:00 
end: 2016-03-04 21:00:00 
event_url: https://www.facebook.com/events/508544309332737/ 
organizers: ["iforsee"]
locations: ["iforesee"]
featuredImage: 12778690-758725450929115-2771338810115475748-o.png
---

Citylab Meetup
--------------

We kindly invite you to join us at the Citylab Meetup at iForesee on [4 March from 18:00 till 21:00](https://www.facebook.com/events/508544309332737/). You will get the chance to meet those involved in Citylab and iForesee, experience the entrepreneurial spirit in action and meet the students of The Hague’s community of young creatives and entrepreneurs.

Programme
---------

18:00 - Doors Open 18:30 - Opening 19:00 - Elevator Pitches 19:30 - Fun Fair 21:00 - Closing We look forward to welcoming you into our world.