---
type: event
path: /event/fiets-stad-debat
title: 'Fiets in de stad debat'
date: 2019-10-01
tags: ["debat","Events","fietsen","platform stad"]
start: 2016-03-29 17:00:00 
end: 2016-03-29 19:00:00 
event_url: http://www.platformstad.nl/events/fietsindestad/ 
organizers: ["platform_stad"]
locations: ["centrale_bibliotheek"]
featuredImage: f24ff0e2-dfd0-4b9f-98e3-4467ebcad990.png
---

De fiets wordt steeds populairder. Mensen in Den Haag pakken vaker de fiets en die trend zet door. Fietsen is schoon, gezond en gemakkelijk, maar heeft wel impact op het gebruik van de ruimte in de stad. Het fietsgebruik zorgt soms voor overlast en onveilige situaties. "Fietsen in Den Haag kan nog veel beter gefaciliteerd worden" Fietsers laten zich moeilijk combineren met voetgangers in de shared spaces in de Grote Marktstraat. In veel woonwijken zijn de fietsstroken smal en fietsparkeerplekken nauwelijks aanwezig. Fietsen in Den Haag kan nog veel beter gefaciliteerd worden. Moeten er in de binnenstad fietsvrije zones worden aangewezen, met ruime fiets(parkeer)plekken aan de randen ervan? Richten we wegen zo in dat auto’s en fietsen de beschikbare ruimte echt delen? Dat vraagt om scherpe keuzes. In maatschappelijke kosten-batenanalyses (MKBA) komt de fiets steevast als winnaar uit de bus. De voordelen voor luchtkwaliteit, gezondheid en de kwaliteit van de openbare ruimte zijn groot. Om die beloftes waar te maken moet de toename van het fietsgebruik wel in goede banen geleid worden. Dat vraagt om investeringen en om het optimaal benutten van de (soms schaarse) ruimte. Durft Den Haag te kiezen voor de fiets? Debatteer mee met o.a.: Lucas Harms (Kennisinstituut voor Mobiliteitsbeleid), Johan Diepens (ANWB/ Mobycon), Jac Wolters (bestuurslid Fietsersbond), Herman de Graaff (directeur Biesieklette), Arnoud Kapaan (voorzitter BIZ City Center) Praktische Informatie 16.30 uur inloop met broodje Centrale Bibliotheek Eerste verdieping, Studio B Spui 68 Den Haag