---
type: event
path: /event/devhaag-meetup-34
title: '/dev/haag meetup #34'
date: 2019-10-01
tags: ["Events","meetup","nomads"]
start: 2016-03-18 16:30:00 
end: 2016-03-18 19:30:00 
event_url: http://www.meetup.com/devhaag/events/228921908/ 
organizers: ["devhaag"]
locations: ["nomadz"]
featuredImage: global-21873397.jpeg
---

Because of Good Friday, we are moving the March meetup one week ahead. On Friday March 18th, we will be back in [Nomadz](http://www.nomadz.nl/)with the following speakers: • **Matthijs Kamstra** will introduce us to [Haxe](http://haxe.org/), a programming language with which you can target a whole range of platforms. This will be a gentle introduction to the Haxe toolkit: what is it? What can you use it for? What problems does it solve? • **Alex Bertram** will tell us about [ActivityInfo](http://about.activityinfo.org/) and what it takes to run a Software-as-a-Service product with thousands of users around the world. It turns out that it takes a little more than setting up a box in the corner with a server on it... Get in touch if you want to sponsor the drinks and snacks. Hope to see you on the 18th! Maarten-Jan and the /dev/haag crew. P.S. this meetup has limited space so sign up quickly or add yourself to the waitlist.