---
type: article
path: '/article/duurzame-wijkactie-den-haag'
tags:
  - subsidie
  - duurzaamheid
  - wijkinitiatief
sections:
  - Community
date: 2028-01-15
title: 'Nieuwe subsidieregeling voor duurzame wijk-initiatieven gaat weer van start'
excerpt: Vanaf 1 februari 2020 gaat de regeling Duurzame Haagse Wijkactie weer open. Voor duurzame projecten in de wijk kun je maximaal 5.000 euro aanvragen.
author: arn
featuredImage: header.jpg
---

Goed nieuws! Vanaf 1 februari 2020 gaat de regeling Duurzame Haagse Wijkactie weer open. Voor duurzame projecten in de wijk kun je maximaal 5.000 euro aanvragen. [Hier](https://www.denhaag.nl/nl/subsidies/subsidies-wonen-en-bouwen/subsidie-duurzaamheid-aanvragen.htm) kun je lezen hoe het aanvragen in zijn werkt gaat. Mocht je een aanvraag willen doen, lees dit dan goed! Heb je vragen of wil je sparren over je projectidee, neem dan contact op met medewerkers van [GreenWish](mailto:info@greenwish.nl).

Vind je het leuk om te lezen welke impact de projecten in 2019 hebben gehad op de duurzaamheid in Haagse wijken? Lees dan het korte evaluatie-rapport [hier](https://www.greenwish.nl/wp-content/uploads/Subsidieregeling-Duurzame-Wijkactie-Den-Haag-Impact-projecten-2019.pdf).

![](impact.png)
