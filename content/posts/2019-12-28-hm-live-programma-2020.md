---
type: article
draft: true
path: /hm-live-programma-2020
date: 2019-12-25
title: 'Haagse Makers Live! Programma 2020'
excerpt: Na een rustige periode wordt Haagse Makers verder ontwikkeld tot het platform voor de Haagse Do-It-Together beweging.
categories:
  - hmlive
tags:
  - Haagse Makers Live!
  - Talkshow
sections:
  - Live!
author: arn
intro: 'Haagse Makers is in 2014 ontstaan vanuit de eerste Haagse Makersbeurs. Vanuit het idee om de Haagse Willie Wortels bij elkaar te brengen en op een publiek podium te laten zien. In de jaren daarna zijn een aantal richtingen en projecten geprobeerd, met de ambitie om een platform voor de Haagse Maker Movement te zijn. Afgelopen jaar lag het even stil,maar er wordt nu met hernieuwde energie gewerkt om het platform verder te versterken. '
featuredImage: null
---
