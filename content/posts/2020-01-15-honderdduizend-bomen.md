---
type: article
path: '/article/crowdfunding-alert-honderdduizend-bomen-en-een-bos-van-draad'
tags:
  - voordekunst
  - crowdfunding
  - 1000.000 bomen
sections:
  - Community
date: 2020-01-15
title: 'Crowdfunding alert: 100.00 bomen en een bos van draad'
excerpt: Nog een aantal dagen is het project van Sara Vrugt via voordekunst te ondersteunen!
author: arn
featuredImage: iHoa6ltLzj86sZ8W.jpeg
---

**Draag meteen bij via [Voordekunst](https://www.voordekunst.nl/projecten/9733)**

### Draag bij aan een groot kunstproject en kom in beweging voor de natuur!

Met dit participatieve project viert kunstenaar Sara Vrugt de natuur. In 2020 borduurt zij samen met vele anderen een bos van honderd vierkante meter.
Dit kunstwerk wordt gemaakt in een pop-up atelier dat vier seizoenen lang door het land reist, iedereen is er welkom om de mooiste borduursteken te leren of een persoonlijk natuurverhaal te vertellen. Loop binnen en doe mee!

### De geborduurde bomen vormen een kunstwerk waarin je in gedachten kunt verdwalen.

Als het bos geborduurd is wordt de semi-transparante stof in spiraalvorm opgehangen, zodat een gelaagde ruimte ontstaat. Het binnengaan van deze installatie, waarin de bomen vier meter boven je uittorenen, zal de ervaring van een bos benaderen én ruimte laten aan de verbeelding.
![100.000 bomen](./Mu9qpqDmAfigOHFy.jpeg)

### Iedereen is welkom, samen bereiken we meer!

Onder professionele begeleiding komt elke geborduurde boom tot stand naar aanleiding van een persoonlijk natuurverhaal. De verhalen van bezoekers aan het pop-up atelier worden door een schrijver opgetekend en vervolgens met naald en draad in het doek verwerkt.
Een audiotour zorgt er voor dat beeld en verhaal weer samenkomen in het eindresultaat en dat het kunstwerk een zinnenprikkelende beleving wordt.
Na een aantal tentoonstellingen krijgt het werk een permanente plek buiten, waar het met de natuur kan vergroeien doordat we zaden in de zoom verwerken.

### Zorg jij voor de kers aan de boom?

![100.000 bomen](./ZzZ7AaGC6KSD0FXi.jpeg)
Het project gaat sowieso van start door bijdragen van onder andere het BankGiro Loterij Fonds, het VSB Fonds, de Gemeente Den Haag en andere fondsen.
Op 22 januari 2020 wordt in Den Haag de eerste steek gezet in het winteratelier en ook voor de seizoenen die volgen zijn de voorbereidingen in volle gang.
Ondanks het bij elkaar gebrachte geld, missen we echter nog een bedrag van ongeveer €15.000 om het project in al z´n facetten te kunnen uitvoeren. Met deze crowdfunding willen we zorgen voor de kers op de taart, of in dit geval de geborduurde kers aan de boom!
En... als het kunstwerk gerealiseerd is gaan we met onze partners IVN en TreeSisters levende bomen planten: een echt bos van honderdduizend bomen!

![100.000 bomen](./Mu9qpqDmAfigOHFy.jpeg)
Samen zetten we kunst in als vorm van verzet én gaan we een levend bos planten: activisme was nog nooit zo aangenaam.
Wil je op andere wijze een bijdrage leveren? Heb je zin om te komen borduren of je verhaal met ons delen? Volg ons op [Facebook](https://www.facebook.com/Honderdduizendbomen) en/of [Instagram](https://www.instagram.com/saravrugt/).
