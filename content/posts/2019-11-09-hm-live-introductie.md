---
type: article
draft: true
path: /hm-live-introductie
date: 2019-11-09
title: 'Nieuw: Haagse Makers Live!'
excerpt: 'Een nieuw project voor Haagse Makers: Laten we vindingrijkheid in beeld brengen via live interviews'
author: arn
tags:
  - Haagse Makers Live!
  - Talkshow
sections:
  - Live!
featuredImage: img.jpg
---

Platform Haagse Makers wil graag vindingrijkheid in de stad in beeld brengen en versterken. Dat doen we onder andere door het communiceren over interessante ontwikkelingen in de stad en daarbuiten. We hebben dat in het verleden op verschillende manieren geprobeerd, via platforms als facebook, meetup, discord en anderen. De laatste periode was vooral de agenda het eerste aandachtspunt. Een overzicht van interessante bijeenkomsten voor alle makers in Den Haag.

kuhbiuhuiho oiuhouh

Maar het liefst vormen we een redactie waarbij we zelf ook content gaan maken. Dit kan gaan om tekst (artikelen) en beelf (foto, video en audio.
Naast redactionele inhoud willen we met Platform Haagse Makers ook graag mensen bij elkaar brengen.

Een interview-serie over creatie, vindingrijkheid en de kunst van het maken, waarbij we ons vooral richten op het ontwikkelproces

<h2>Format</h2>
<p>Haagse Makers Live! is een serie van events waarbij de kunst van het maken centraal staat. We gaan op zoek hoe iemand iets succesvols tot stand brengt. Het hebben van een idee is 1, maar hoe zorg je ervoor dat het ook gerealiseerd wordt? We nodigen makers van allerlei achtergronden en leeftijden uit om te praten over hun projecten. We vinden uit hun concepten en processen nieuwe ontwikkelingen kunnen inspireren.</p>
<p>Haagse Makers Live! vindt maandelijks plaats, met iedere editie 4 gasten. Iedere gast heeft circa 12 minutes om zijn/haar werk toe te lichten en om vragen van de host of uit het publiek te beantwoorden.</p>
<p>De interviews zijn in het Nederlands of Engels (afhankelijk van de voorkeur van de gast) en gaan vooral over maakproces, niet zozeer over het eindproduct. De interviews worden gefilmd en zijn ook te volgen via het <a href="https://www.youtube.com/playlist?list=PLYrBsYVxZNLW_3MYmZsf1ONhq0-bKGbLu">YouTube kanaal</a>. Voor, tussen en na de interviews is er ruimte voor een drankje en te netwerken.</p>

<h2>Komende editie</h2>
<p>De eerste editie staat gepland voor 2019, in het Koorenhuis. Op het podium in de foyer is er ruimte voor 4 gasten! Op dit moment wordt het programma voorbereid.</p>

<h2>Locatie</h2>
<p>Haagse Makers Live! vindt plaats <a href="http://inhetkoorenhuis.nl/">in het Koorenhuis, Huis voor Makers.</a></p>
