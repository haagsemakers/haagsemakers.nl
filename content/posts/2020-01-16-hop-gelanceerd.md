---
type: article
path: '/article/hop-gelanceerd'
tags:
  - platform stad
  - hop
  - prijs
sections:
  - Community
date: 2020-01-18
title: 'Haagse Openbare ruimte Prijs (HOP) gelanceerd door platform stad'
excerpt: Platform STAD lanceert de HOP - een nieuwe prijs voor de meest gewaardeerde vernieuwde openbare ruimte van Den Haag van de afgelopen vier jaar.
author: arn
featuredImage: hop.jpg
---

## Zend je favoriete openbare ruimte in

Platform STAD lanceert de HOP: een nieuwe prijs voor de meest gewaardeerde vernieuwde openbare ruimte van Den Haag van de afgelopen vier jaar.
Ontwerpers, opdrachtgevers en bewoners kunnen tot en met 9 februari hun project inzenden.
https://www.platformstad.nl/dien-je-project-in/

### De HOP

Platform STAD onderstreept met de Haagse Openbare ruimte Prijs (HOP) het belang van een goede openbare ruimte. De prijs stimuleert het bewust omgaan met en inrichten van de openbare ruimte.
Maar vooral wil Platform STAD samen met ontwerpers, opdrachtgevers en bewoners het publieke leven in de openbare ruimte van Den Haag vieren.

### De jury

De vakjury nomineert 5 inzendingen waarop het publiek kan stemmen. Tijdens een feestelijke bijeenkomst in mei worden de HOP juryprijs en de HOP publieksprijs uitgereikt.
https://www.platformstad.nl/de-vakjury/
