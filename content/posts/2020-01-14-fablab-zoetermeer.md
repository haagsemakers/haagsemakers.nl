---
draft: false
type: article
path: '/article/fablab-zoetermeer'
tags:
  - Fablab
  - zoetermeer
  - interview
sections:
  - Makerspaces
date: 2020-01-13
title: Fablab zoetermeer
excerpt: Fablab Zoetermeer in de bibliotheek
author: arn
featuredImage: IMG_20200114_095244.jpg
---

## Over de sectie Makerspaces op haagsemakers.nl

In deze serie over makerspaces ga ik op zoek naar de status en ontwikkeling van makerspaces in de stad. Met Haagse makers zeggen we dat iedereen het moet kunnen maken in de stad, en plekken waar dat kan zijn dan nodig. In deze serie breng ik deze plekken in beeld: plekken waar je zelf aan de slag kunt gaan om te maken wat je hebt bedacht. Er zijn verschillende vormen, er worden verschillende namen aan gegeven, fablab, makerspace, innovation hub etc. De naam maakt me niet zoveel uit, zolang de plek maar de mogelijkheid biedt om zelf aan de slag te gaan, en waar het delen van kennis en kunde centraal staat.

## Over Fablab Zoetermeer

![](IMG_20200114_095329.jpg)
Vandaag ga ik op bezoek bij [Fablab](http://digitalli.nl/fablab-zoetermeer/) Zoetermeer. Als Haagse maker heb ik Zoetermeer niet echt op mijn vizier, maar tot mijn verbazing is het een kleine 40 minuten met de Randstadrail van deur tot deur. Dat is niet veel langer dan met de fiets naar Kijkduin of Mariahoeve. Het Fablab Zoetermeer zit (voorlopig) in het [Forum Zoetermeer](https://www.forumzoetermeer.nl), in het centrum van de stad waarin ook het gemeentehuis en de openbare bibliotheek zit.

Oprichter van Fablab Zoetermeer is Sealeung Li, hij vertelt me vandaag meer over zijn Fablab.

## Kun je iets vertellen over fablab in het algemeen?

“In een [FabLab](https://fablab.nl) kan iedereen zelf aan de slag met de machines die beschikbaar zijn. En als je hier nog geen ervaring mee hebt, dan geven we instructie om dit te leren. Een FabLab heeft als doelstelling iedereen een mogelijkheid te geven hun ideeën om te zetten in een fysiek product en hun ontwerpen te delen.
Bezoekers gebruiken het FabLab voor het maken van innovatieve prototypes, modellen, maquettes, kleine series, op maat gemaakte producten en voor allerlei interessante experimenten. Daarnaast is het ook mogelijk om bestaande producten te personaliseren.
![](IMG_20200114_095650.jpg)
Het FabLab biedt scholieren, studenten, (beginnend) ondernemers, kunstenaars en particulieren een werkplek waar zij met behulp van computergestuurde machines en onze kennis ideeën kunnen (laten) uitwerken tot concrete producten. “

## “Kun je iets vertellen over je eigen achtergrond?”

In 2008 ben ik mijn eigen grafische bedrijf gestart: [Digitalli](http://digitalli.nl). Ik wilde graag zelf onderdelen frezen en lassen, en wilde dit graag zelf doen. Ik kwam toen bij Fablab Den Haag uit, waar je dit zelf kon doen. Ik kwam erachter dat dat allemaal machines waren die ik zelf ook al had. Daarop heb ik besloten om zelf een fablab in Zoetermeer te starten.
In 2014 ben ik begonnen in een oude school op anti-kraak-basis. Mijn plan was om daar in elk lokaal een werkplaats op te zetten met verschillende functies. Helaas bleek dit niet mogelijk, bijvoorbeeld omdat het niet te verzekeren was.
Op dat moment, in 2015 was ook de trend in bibliotheekland om fablabs in bibliotheken op te zetten. Op die manier kwam ik in contact met de bibliotheek en hebben we samen het fablab zoetermeer in de bibliotheek opgezet.
![](IMG_20200114_095349.jpg)

## Hoe is de ontwikkeling van fablab Zoetermeer verlopen?

In 2018 was een grote verbouwing van de bibliotheek en het Forum complex. De locatie is verbouwd en het FabLab heeft daar een vaste plek in gekregen. Sindsdien is de opzet wat veranderd. Het FabLab is veranderd naar het [e-lab](https://www.bibliotheek-zoetermeer.nl/educatie/fablab-zoetermeer.html) en is dit een lab van de bibliotheek. Alle apparatuur is van mij en ik zorg voor de uitvoering, en ik ben huurder in de bibliotheek. Samen zorgen we voor de communicatie en het aanbieden van workshops. We geven bijvoorbeeld ook workshops op scholen, dat doen we (onder andere) in samenwerking met de bibliotheek en scholen.
Het fablab is het een combinatie van verschillende onderdelen: van het uitvoeren van commerciële opdrachten tot het hosten van kinderfeestjes, geven van workshops voor scholen en het organiseren en ondersteunen van vrijwilligers.

Helaas is recent de beslissing genomen door de bibliotheek om te stoppen met het e-lab in de huidige vorm. De ruimte die ik nu heb wordt waarschijnlijk omgevormd tot een café, in de hoop om het Forum nieuw leven in te blazen. In april 2020 stopt daarom (waarschijnlijk) de samenwerking en ben ik op zoek naar nieuwe mogelijkheden.

## Wat zijn de belangrijkste leermomenten?

De communicatie over het fablab, wat het is en wat de mogelijkheden zijn hadden we beter kunnen doen. Ook de communicatie van de locatie van buitenaf kon beter, het is eigenlijk niet goed zichtbaar, ondanks dat we in het centrum en direct achter een glazen ruit zitten aan de straatkant.

De ervaring leert ook dat het zeker 2 jaar kost voordat het echt gaat lopen. Deze aanloop heb je nodig om bekendheid te genereren en ervoor te zorgen dat mensen je gaan vinden.
De organisatie bleek met name voor de communicatie lastig. Forum, Bibliotheek en Fablab werken samen in de uitvoering, maar ieder heeft haar eigen communicatie.

Tegenwoordig willen jongeren meteen alles goed hebben. Ze zijn perfectionistisch, willen direct resultaat en het moet meteen goed zijn. Dingen worden snel gekocht en weer vervangen. Het kost weinig moeite en geld om dingen te kopen. Waarom zou je het dan zelf doen, waarbij het resultaat onzeker is. Je merkt dat je daar tegen aanloopt bij het werken met jongeren. Dit is ook een teken van deze tijd, waarbij alles snel, makkelijk en direct moet gaan.

Wat wel erg goed is gelukt is dat je vaak wel mensen aan het werk krijgt die je normaal gesproken moeilijk aan het werk krijgt. Bijvoorbeeld autistische kinderen die helemaal op kunnen gaan in het werken met deze tools. Dat geeft me weer kippenvel als ik eraan denk, en dat had ik van tevoren nooit bedacht.

## Wat zijn de mogelijkheden in fablab Zoetermeer

![](IMG_20200114_095814.jpg)
We geven bijvoorbeeld workshops 3d-ontwerpen, 3d-printen en kleding bedrukken. Het is mogelijk om zelf aan de slag te gaan met lasersnijden, 3d-printen en andere vormen van bedrukken.
Je kan zelf aan de slag gaan, maar het kan ook in opdracht. Ik zit hier naast het FabLab ook met mijn eigen onderneming Digitalli. Dus het is ook mogelijk om in opdracht iets te laten produceren.  
Op de website staan alle mogelijkheden in detail beschreven: [http://digitalli.nl/fablab-zoetermeer/](http://digitalli.nl/fablab-zoetermeer/)

## Wat zijn je plannen voor de toekomst?

Ik zoek een nieuwe plek voor mijn apparatuur en mijn bedrijf. Ik ben met een aantal partijen in gesprek om te bekijken welke vorm dat krijgt. Op dit moment zijn alle opties open. Het liefst zet ik de huidige opzet door: Een locatie waar ik met mijn eigen bedrijf, gecombineerd met een fablab werkplaats aan de slag kan gaan. Ik word nog steeds enthousiast van het idee van een grote gecombineerde werkplaats, zoals ik dat bijvoorbeeld voor ogen had in de beginfase.
De investering die ik heb gedaan in apparatuur is behoorlijk hoog, dus het is wel noodzakelijk om op een toegankelijke en zichtbare plek te zitten. Een optie is ook om (gezamenlijk) een makerspace met lidmaatschap op te zetten. Wat dat betreft heeft een Fablab geen harde restricties, zolang het (gedeeltelijk) publiek toegankelijk is, kennis delen centraal staat kun je jezelf aanmelden als een Fablab, in Nederland bij Fablab Benelux in Utrecht.

---

Het was een mooie introductie van Fablab Zoetermeer en inspirerend om te zien hoe iemand op een succesvolle manier een fablab heeft opgezet en weet uit te baten. In dit geval door het combineren van zijn eigen onderneming met het Fablab, een interessante combinatie van een commercieel en een maatschappelijk doel.

![](IMG_20200114_095256.jpg)
![](IMG_20200114_095415.jpg)
![](IMG_20200114_095555.jpg)
