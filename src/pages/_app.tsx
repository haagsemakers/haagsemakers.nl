import 'fontsource-montserrat';
import 'fontsource-montserrat/600.css';
import 'fontsource-montserrat/800.css';
import 'fontsource-raleway';
import 'fontsource-raleway/600.css';
import { useRouter } from 'next/router';
import React, { useEffect } from 'react';
import * as gtag from '../lib/gtag';
import '../styles/index.css';

const App = ({ Component, pageProps }: { Component: any; pageProps: any }): JSX.Element => {
  const router = useRouter();

  useEffect(() => {
    const handleRouteChange = (url: string) => {
      gtag.pageview(url);
    };
    router.events.on('routeChangeComplete', handleRouteChange);
    return () => {
      router.events.off('routeChangeComplete', handleRouteChange);
    };
  }, [router.events]);

  return <Component {...pageProps} />;
};

export default App;
