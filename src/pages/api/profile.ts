import auth0 from 'lib/auth0';
import { NextApiRequest, NextApiResponse } from 'next';

export default async function me(req: NextApiRequest, res: NextApiResponse): Promise<any> {
  try {
    const a = auth0.tokenCache; // auth0.handleProfile(req, res, {});
    console.log('a', a);
  } catch (error) {
    console.error(error);
    res.status(error.status || 500).end(error.message);
  }
}
