import auth0 from 'lib/auth0';
import { NextApiRequest, NextApiResponse } from 'next';

export default async function me(req: NextApiRequest, res: NextApiResponse): Promise<any> {
  console.log('me');
  try {
    const a = await auth0.getSession(req); // auth0.handleProfile(req, res, {});

    console.log('a', a);

    await auth0.handleProfile(req, res, {});
  } catch (error) {
    console.error(error);
    res.status(error.status || 500).end(error.message);
  }
}
