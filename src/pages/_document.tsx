import Document, { Head, Html, Main, NextScript } from 'next/document';
import React from 'react';
// import FavIcon from '../../public/static/img/favicon.png';
import { GA_TRACKING_ID } from '../lib/gtag';

export default class MyDocument extends Document {
  isProduction = process.env.NODE_ENV === 'production';

  render() {
    return (
      <Html lang="nl">
        <Head>
          {/* <link rel="shortcut icon" type="image/x-icon" href={FavIcon} /> */}
          {this.isProduction && (
            <React.Fragment>
              {/* Global Site Tag (gtag.js) - Google Analytics */}
              <script async src={`https://www.googletagmanager.com/gtag/js?id=${GA_TRACKING_ID}`} />
              <script
                dangerouslySetInnerHTML={{
                  __html: `
              window.dataLayer = window.dataLayer || [];
              function gtag(){dataLayer.push(arguments);}
              gtag('js', new Date());
              gtag('config', '${GA_TRACKING_ID}', {
                page_path: window.location.pathname,
              });
            `,
                }}
              />
            </React.Fragment>
          )}
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}
