import React from 'react';
import { Link } from 'gatsby';
import Layout from '../components/shared/Layout';
import SEO from '../components/shared/seo';
import tw from 'tailwind.macro';
import styled from 'styled-components';
import Container from '../components/shared/Container';

export default () => {
  return (
    <Layout>
      <SEO title="Haagse Smaakmakers" />
      <Container>
        <PageHeader>
          <PageTitle>Haagse Smaakmakers</PageTitle>
          <PageSubtitle>
            Leve de Haagse Smaakmaker! Haagse Smaakmakers zijn de makers die
            zich richten op Haagse voedselproducten. Door lokaal te produceren,
            verkopen en samen te eten verterken we de lokale economie,
            verbondenheid en dragen we bij aan duurzame ontwikkeling.{' '}
          </PageSubtitle>
        </PageHeader>

        <Notice>
          <h2>Smaakmakerslunch - 10 december 2019</h2>
          <p>
            We starten op dinsdag 10 december met de eerste Smaakmakerslunch.
            Tijdens de lunch eten we lokaal voedsel, door een Haagse chefkok, en
            krijgen Haagse Makers een podium om hun verhaal over voedsel te
            vertellen en wat het betekent voor verschillende beleidsdoelen als
            je lokaal gaat eten (hoe je voetafdruk verkleint ten opzichte van
            ‘normaal’). Centraal staat de vraag hoe we hierbij het beste kunnen
            samenwerken. Wat is er nodig om met elkaar het beste resultaat te
            bereiken?
          </p>
          <p>
            Daarnaast wordt op beeldende wijze het verhaal verteld over de
            samenhang tussen klimaattransitie, voedsel, hoe je je organiseert in
            de stad en wat de gemeente hierin kan doen. Van abstracte doelen
            naar concrete stappen.
          </p>
          <p>
            De smaakmakerslunch is op uitnodiging. Ben je een Haagse Smaakmaker,
            wil je meedoen? Neem dan <Link href="/contact">contact</Link> op!{' '}
          </p>
          <p>
            Locatie:{' '}
            <a href="http://inhetkoorenhuis.nl/">
              in het Koorenhuis, Huis voor Makers.
            </a>
          </p>
        </Notice>
      </Container>
    </Layout>
  );
};

const PageTitle = styled.h1`
  ${tw`font-bold text-2xl`}
`;
const PageSubtitle = styled.h1`
  ${tw`text-lg font-normal leading-loose `}
`;
const Notice = styled.div`
  ${tw`p-8 text-sm bg-blue-100`}
`;
const PageHeader = styled.div`
  ${tw`text-center pt-10 pb-20`}
`;
