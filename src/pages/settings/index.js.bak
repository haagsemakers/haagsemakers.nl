// pages/settings.index.js

import React from 'react';
import styled from 'styled-components';
import tw from 'tailwind.macro';
import { navigate } from 'gatsby';
import * as moment from 'moment';
import { isBrowser, logout, isAuthenticated, getAuthInfo } from './../../utils/auth.service';
import { getUserInfo } from '../../utils/user.service';
import Layout from './../../components/shared/Layout';
import SEO from './../../components/shared/seo';
import Container from './../../components/shared/Container';
import AccountHeader from './../../components/Account/Settings/SettingsHeader';

const AccountPage = ({ data }) => {
  if (isBrowser && !isAuthenticated()) {
    navigate('/aanmelden');
    return null;
  }

  const userInfo = getUserInfo();
  const authInfo = getAuthInfo();

  const doLogout = (evt) => {
    evt.preventDefault();
    logout();
  };

  let userId = authInfo.sub;
  if (userId) {
    userId = userId.split('|')[1];
  }

  const signedUp = () => {
    if (!userInfo.signed_up) return '-';
    return moment(userInfo.signed_up).format('D MMMM YYYY');
  };

  return (
    <Layout>
      <SEO title="Account" />

      <Container>
        <AccountHeader title="Jouw Haagse Makers account" />
        <Wrapper>
          <MainContent>
            <Title>Account gegevens</Title>
            <table>
              <tbody>
                <tr>
                  <TD style={{ fontWeight: 600 }}>Email:</TD>
                  <TD>
                    {authInfo.email} {!authInfo.email_verified && ' (unverified)'}
                  </TD>
                </tr>
                <tr>
                  <TD style={{ fontWeight: 600 }}>Account type:</TD>
                  <TD>{userInfo.account_plan}</TD>
                </tr>
                <tr>
                  <TD style={{ fontWeight: 600 }}>Account sinds:</TD>
                  <TD>{signedUp()}</TD>
                </tr>
                <tr>
                  <TD style={{ fontWeight: 600 }}>User id:</TD>
                  <TD>{userId}</TD>
                </tr>
              </tbody>
            </table>
            <Button onClick={(evt) => doLogout(evt)}>Uitloggen</Button>
            <DeleteNotice>
              Account opzeggen? Dat kan (nog) niet automatisch. Stuur een bericht naar{' '}
              <a href="mailto:redactie@haagsemakers.nl">de redactie</a>.
            </DeleteNotice>
          </MainContent>
        </Wrapper>
      </Container>
    </Layout>
  );
};

export default AccountPage;

const Wrapper = styled.div`
  ${tw``}
`;
const Title = styled.h3`
  ${tw`pl-0`}
`;
const MainContent = styled.div`
  ${tw`block bg-white shadow-md rounded px-8 pt-2 mb-16 border border-solid border-gray-200`}
`;
const TD = styled.td`
  ${tw`p-2`}
`;
const DeleteNotice = styled.div`
  ${tw`text-sm my-4`}
`;
const Button = styled.button`
  ${tw`mt-8 mb-4 py-4 px-8 bg-white cursor-pointer font-bold uppercase rounded text-orange-400 border-2 border-solid border-orange-400 hover:bg-orange-400 hover:text-white`}
`;
