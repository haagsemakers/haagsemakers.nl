import { NextPage } from 'next';
import React from 'react';
import { Layout } from '../components/shared/Layout';

export const AgendaPage: NextPage = () => {
  return (
    <Layout>
      <div className="container">
        <div className="pt-16 mb-8">
          <h1 className="text-3xl">Haagse Makers Community Agenda</h1>
          <h2 className="text-xl mb-2">Wat is er allemaal te doen op makersgebied voor Haagse makers?</h2>
        </div>

        <div className="grid grid-cols-12 gap-8">
          <div className="col-span-12 sm:col-span-9">
            <iframe
              src="https://calendar.google.com/calendar/embed?height=600&amp;wkst=2&amp;bgcolor=%23ffffff&amp;ctz=Europe%2FAmsterdam&amp;src=dXJiYW5saW5rLm5sXzF2cHIzaTJ1amd2dWJqcGlnbDVvMDNhanRjQGdyb3VwLmNhbGVuZGFyLmdvb2dsZS5jb20&amp;color=%233F51B5&amp;showTitle=0&amp;showNav=1&amp;showDate=1&amp;showPrint=0&amp;showTabs=0&amp;showCalendars=0&amp;showTz=0&amp;mode=MONTH&amp;hl=nl&amp;title=Haagse%20Makers%20events"
              style={{ borderWidth: 0 }}
              width="100%"
              height="600"
              frameBorder="0"
              scrolling="no"
            ></iframe>
          </div>

          <div className="col-span-12 sm:col-span-3">
            <div className="rounded border-2 border-color-blue p-4 bg-gray-100 mb-4">
              <h3>Zet jouw event in de Community agenda!</h3>
              <p>Plaats een bericht in het Haagse Makers Community forum en we zetten je event in de agenda</p>
              <a
                className="bg-primary text-white p-2 rounded block text-center mt-2 hover:underline hover:bg-blue-600"
                target="_blank"
                rel="noreferrer"
                href="https://community.haagsemakers.nl/c/agenda/7"
              >
                Naar het community forum
              </a>
            </div>
            <div className="rounded border-2 border-color-blue p-4 bg-gray-100">
              <h3>Tip: Voeg deze agenda toe aan je eigen agenda en blijf altijd op de hoogte!</h3>
              <a
                className="p-2 block text-center mt-2 hover:underline"
                target="_blank"
                rel="noreferrer"
                href="https://calendar.google.com/calendar/u/1?cid=dXJiYW5saW5rLm5sXzF2cHIzaTJ1amd2dWJqcGlnbDVvMDNhanRjQGdyb3VwLmNhbGVuZGFyLmdvb2dsZS5jb20"
              >
                + toevoegen
              </a>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default AgendaPage;
