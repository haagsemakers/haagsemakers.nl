import Layout from '@components/shared/Layout';
import tools from './tools.json';

export default function Home(): React.ReactElement {
  return (
    <Layout>
      <div className="container">
        <div className="max-w-xl m-auto mb-16 leading-loose text-center">
          <h1 className="mb-8 text-3xl">Tools and Gear We Love</h1>
          <p className="text-xl text-center">
            We find and select the best tools and gear that makers, hobbyists and DIYers like. We share deals, write
            unbiased reviews and compare tools. Click the images and links below on something that you need to compare
            prices in more than 8 different stores, so you can always find the best price. Pick a category below: Kits,
            Development Boards, Electronic Assortments, Tools and Test Equipment, Sensors and Modules or Arduino
            shields. Most links are active, but this page is being updated weekly with new tools and components. Please
            bookmark this page and use it as a reference. Thanks!
          </p>
        </div>

        <h2 className="mb-4 text-2xl font-bold">Electronica</h2>
        <h3 className="mb-2 text-xl">Development boards</h3>
        <div className="grid grid-cols-4 gap-4">
          {tools.electronics.development_boards.map((tool: any) => (
            <div key={tool.name} className="overflow-hidden bg-white rounded shadow-lg">
              <a href="/#" className="block hover:opacity-25">
                <img src={`/tools/${tool.img}`} alt="" className="w-full border-white border-1" />

                <div className="px-6 py-4">
                  <h4 className="mb-2 text-xl font-bold">{tool.name}</h4>
                  <p className="text-base text-gray-700">{tool.excerpt}</p>
                </div>
              </a>
            </div>
          ))}
        </div>

        <h2>Electronica</h2>
        <h3>Arduino Starter kits</h3>
        <h3>Arduino sensors</h3>
        <h3>Raspberry Pi Starter kits</h3>
        <h3>ESP development boards</h3>
      </div>
    </Layout>
  );
}
