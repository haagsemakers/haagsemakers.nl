import { InstantSearchFront } from '@components/algolia/InstantSearchFront';
import { LatestPosts } from '@components/discourse/LatestPosts';
import Head from '@components/shared/Head';
import Layout from '@components/shared/Layout';
import { useFetchUser } from 'lib/user';
import Link from 'next/link';
import React from 'react';
import { FaCalendarDay } from 'react-icons/fa';

export default function Home(): React.ReactElement {
  const { user, loading } = useFetchUser();

  return (
    <Layout user={user} loading={loading}>
      <Head title="Home" />
      {/* Hero */}
      <section className="pt-16 pb-8 bg-primary">
        <div className="container text-white">
          <h1 className="text-6xl" style={{ textShadow: '2px 2px 2px rgba(0,0,0,0.4)' }}>
            Hoe Maakt U Het?
          </h1>
          <h2
            className="text-sm font-light max-w-sm leading-loose"
            style={{ textShadow: '1px 1px 1px rgba(0,0,0,0.6)' }}
          >
            Haagse Makers is een community voor mensen die het leuk vinden om dingen te maken. Doe mee, deel het en maak
            het mee!
          </h2>
        </div>
        <div className="container">
          <div className="py-8 md:p-16 text-center">
            <InstantSearchFront />
          </div>
        </div>
      </section>

      {/* Primary content */}
      <section className="py-8">
        <div className="container">
          <div className="grid grid-cols-12 gap-4">
            {/* <div className="border-bottom border-black">
              <h3 className="text-xl pb-2 border-b-2 border-b-primary mb-2">
                <FaNewspaper size="16" color="#0C5D9A" className="inline-block" /> Laatste Updates
              </h3>
            </div> */}
            <div className="col-span-12 sm:col-span-9">
              <LatestPosts />
            </div>
            <div className="col-span-12 sm:col-span-3">
              <div className="pb-2 border-b-2 mb-4 border-b-primary ">
                <h3 className="text-xl pb-2 mb-2">
                  <FaCalendarDay size="16" color="#0C5D9A" className="inline-block" /> Makers Agenda
                </h3>
                <p className="text-gray-600">
                  Een overzicht van wat er te doen is voor makers in Den Haag:
                  <Link href="/agenda">
                    <a className="p-2 block my-2 text-primary hover:underline">Bekijk de volledige agenda &rarr;</a>
                  </Link>
                </p>
              </div>
              <iframe
                src="https://calendar.google.com/calendar/embed?height=600&amp;wkst=1&amp;bgcolor=%23ffffff&amp;ctz=Europe%2FAmsterdam&amp;src=dXJiYW5saW5rLm5sXzF2cHIzaTJ1amd2dWJqcGlnbDVvMDNhanRjQGdyb3VwLmNhbGVuZGFyLmdvb2dsZS5jb20&amp;color=%233F51B5&amp;showTitle=0&amp;showNav=0&amp;showDate=0&amp;showPrint=0&amp;showTabs=0&amp;showCalendars=0&amp;showTz=0&amp;mode=AGENDA&amp;hl=nl"
                style={{ borderWidth: 0 }}
                width="100%"
                height="800"
                frameBorder="0"
                scrolling="no"
              ></iframe>
              <a
                className="bg-primary text-white p-2 rounded block text-center my-2 hover:underline hover:bg-blue-600"
                href="https://calendar.google.com/calendar/u/1?cid=dXJiYW5saW5rLm5sXzF2cHIzaTJ1amd2dWJqcGlnbDVvMDNhanRjQGdyb3VwLmNhbGVuZGFyLmdvb2dsZS5jb20"
              >
                Voeg toe aan je eigen agenda
              </a>
              <Link href="/agenda">
                <a className="py-2 block my-2 text-primary hover:underline">Bekijk de volledige agenda &rarr;</a>
              </Link>
            </div>
          </div>
        </div>
      </section>
    </Layout>
  );
}
