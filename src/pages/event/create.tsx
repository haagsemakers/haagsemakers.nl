import { CreateEventForm } from '@components/events/CreateEventForm/createEventForm';
import Layout from '@components/shared/Layout';
import { useFetchUser } from 'lib/user';
import { NextPage } from 'next';
import React from 'react';

export const CreateEventPage: NextPage = () => {
  const { user, loading } = useFetchUser();
  console.log(user);
  return (
    <Layout>
      <div className="container mt-16">
        {loading && <p>Loading profile...</p>}

        {loading && !user && <div className="py-32 text-center">Loading user...</div>}
        {!loading && !user && (
          <div className="py-32 text-center">
            <h2>Om een activiteit toe te voegen aan de community agenda mnoet je zijn ingelogd. </h2>
            <a
              className="bg-primary text-white rounded px-4 py-2 inline-block mt-8 text-xl hover:underline hover:bg-blue-700"
              href="/api/login"
            >
              Login
            </a>
          </div>
        )}
        {!loading && user && <CreateEventForm />}
      </div>
    </Layout>
  );
};

export default CreateEventPage;
