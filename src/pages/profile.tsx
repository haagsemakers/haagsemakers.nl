import React from 'react';
import Layout from '../components/shared/Layout';
import { useFetchUser } from '../lib/user';

export default function Profile(): React.ReactElement {
  const { user, loading } = useFetchUser();

  return (
    <Layout user={user} loading={loading}>
      <div className="container pt-16">
        <h1>Jouw Haagse Makers profiel</h1>

        {loading && <p>Loading...</p>}

        {!loading && user && (
          <div className="p-8 bg-gray-200">
            <ul>
              <li>{user.picture && <img className="w-16 h-16 rounded-full overflow-hidden" src={user.picture} />}</li>
              <li>Username: {user.nickname}</li>
              <li>Email: {user.name}</li>
              <li>Laatste update: {user.updated_at}</li>
              <li>id: {user.sub}</li>
            </ul>
            <a
              href="/api/logout"
              className="mt-4 inline-block px-4 py-2 border-2 border-black hover:bg-black hover:text-white"
            >
              Uitloggen
            </a>
          </div>
        )}
      </div>
    </Layout>
  );
}
