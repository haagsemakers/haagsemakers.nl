import React from 'react'
import { graphql, Link } from 'gatsby'
import styled from 'styled-components'
import tw from 'tailwind.macro'
import { CalendarAlt } from '@styled-icons/fa-solid/CalendarAlt'
import Container from '../components/shared/Container'
import Layout from '../components/shared/Layout'
import SEO from '../components/shared/seo'
import Pagination from '../components/shared/Pagination'
import * as moment from 'moment'

class ArticlesPage extends React.Component {
  render() {
    const { data } = this.props
    const { currentPage, numPages } = this.props.pageContext

    return (
      <Layout>
        <SEO title="Artikelen" />

        <Container>
          <PageTitle>Artikelen</PageTitle>
          <PageSubtitle>Updates over de Haagse Makers Community.</PageSubtitle>

          <ItemsWrapper>
            <Items>
              {data.allMarkdownRemark.edges.map(({ node }, ...index) => (
                <Item key={index}>
                  <ItemInner to={`${node.frontmatter.path}`}>
                    {node.frontmatter.featuredImage && (
                      <ItemImgWrapper>
                        <ItemImg
                          src={
                            node.frontmatter.featuredImage.childImageSharp
                              .resize.src
                          }
                        />
                      </ItemImgWrapper>
                    )}

                    <ItemPreview>
                      <ItemMeta>
                        <DateWrapper>
                          <CalendarAltIcon />{' '}
                          {moment(node.frontmatter.date).format('DD-MM-YYYY')}
                        </DateWrapper>
                        {node.frontmatter.sections && (
                          <SectionsWrapper>
                            {node.frontmatter.sections.map((section) => (
                              <SectionItem>{section}</SectionItem>
                            ))}
                          </SectionsWrapper>
                        )}
                      </ItemMeta>
                      <ItemTitle>{node.frontmatter.title}</ItemTitle>
                      <ItemExcerpt>{node.frontmatter.excerpt}</ItemExcerpt>
                    </ItemPreview>
                  </ItemInner>
                </Item>
              ))}
            </Items>
          </ItemsWrapper>

          <Pagination
            basePath={'/artikelen/'}
            currentPage={currentPage}
            numPages={numPages}
          />
        </Container>
      </Layout>
    )
  }
}

const PageTitle = styled.h1`
  ${tw`text-left`}
`
const PageSubtitle = styled.h2`
  ${tw`text-left`}
`
const ItemsWrapper = styled.div`
  ${tw``}
`
const Items = styled.ul`
  ${tw`flex flex-wrap list-none pl-0`}
`
const Item = styled.li`
  ${tw`w-full md:w-1/3 mb-4`}
`
const ItemInner = styled(Link)`
  ${tw`text-black hover:no-underline block p-2 overflow-hidden`}
`
const ItemPreview = styled.div`
  ${tw``}
`
const ItemTitle = styled.h4`
  ${tw`mt-2`}
`
const ItemMeta = styled.div`
  ${tw``}
`
const DateWrapper = styled.div`
  ${tw`text-gray-500 text-sm my-2`}
`
const CalendarAltIcon = styled(CalendarAlt)`
  ${tw`text-gray-500 text-xs w-3 mr-1`}
`
const SectionsWrapper = styled.div`
  ${tw`text-sm mr-1 mt-1 `}
`
const SectionItem = styled.span`
  ${tw`text-gray-500 px-2 py-1 bg-gray-300 rounded inline-block text-gray-600`}
`
const ItemExcerpt = styled.p`
  ${tw`mt-1`}
`
const ItemImgWrapper = styled.div`
  ${tw`overflow-hidden inline-block mb-2`}
`
const ItemImg = styled.img`
  ${tw`w-full`};
  -webkit-transition: all 0.2s ease;
  -moz-transition: all 0.2s ease;
  -ms-transition: all 0.2s ease;
  -o-transition: all 0.2s ease;
  transition: all 0.3s ease;
  vertical-align: middle;

  &:hover {
    -webkit-transform: scale(1.2); /* Safari and Chrome */
    -moz-transform: scale(1.2); /* Firefox */
    -ms-transform: scale(1.2); /* IE 9 */
    -o-transform: scale(1.2); /* Opera */
    transform: scale(1.2);
  }
`

export default ArticlesPage

export const pageQuery = graphql`
  query articleListQuery($skip: Int!, $limit: Int!) {
    allMarkdownRemark(
      sort: { fields: frontmatter___date, order: DESC }
      filter: { frontmatter: { type: { eq: "article" }, draft: { ne: true } } }
      limit: $limit
      skip: $skip
    ) {
      edges {
        node {
          id
          timeToRead
          excerpt(pruneLength: 80)
          html
          frontmatter {
            title
            path
            date
            excerpt
            featuredImage {
              childImageSharp {
                resize(width: 600, height: 350) {
                  src
                }
                fluid(maxWidth: 800) {
                  ...GatsbyImageSharpFluid
                }
              }
            }
            tags
            sections
          }
        }
      }
    }
  }
`
