import React from 'react';
import { graphql, Link } from 'gatsby';
import styled from 'styled-components';
import tw from 'tailwind.macro';
import moment from 'moment';
// import { Tags } from '@styled-icons/fa-solid/Tags'
import Container from '../components/shared/Container';
import { CalendarAlt } from '@styled-icons/fa-solid/CalendarAlt';
import Layout from '../components/shared/Layout';
import SEO from '../components/shared/seo';
// import Img from 'gatsby-image'
import Pagination from '../components/shared/Pagination';

class EventsArchivePage extends React.Component {
  formatDate = (date) => {
    return moment(date).format('D MMMM YYYY');
  };

  render() {
    const { events } = this.props.data;
    const { currentPage, numPages } = this.props.pageContext;

    return (
      <Layout>
        <SEO title="Agenda archief" />

        <Container>
          <Backlink href="/agenda">&larr; Agenda</Backlink>
          <PageTitle>Agenda archief</PageTitle>

          <EventsContainer>
            {events.edges.length < 1 && <p>Nog geen items gevonden</p>}
            {events.edges.map(({ node }) => (
              <EventItem key={node.id}>
                <EventItemInner>
                  <EventLink to={node.frontmatter.path}>
                    {node.frontmatter.featuredImage && (
                      <ImgWrapper
                        src={
                          node.frontmatter.featuredImage.childImageSharp.resize
                            .src
                        }
                        alt={node.frontmatter.title}
                      />
                    )}
                    <DetailsWrapper>
                      <EventDate>
                        <CalendarAltIcon />
                        {this.formatDate(node.frontmatter.start)}
                      </EventDate>
                      <EventItemTitle>{node.frontmatter.title}</EventItemTitle>
                    </DetailsWrapper>
                  </EventLink>
                </EventItemInner>
              </EventItem>
            ))}
          </EventsContainer>

          <Pagination
            basePath={'/agenda/archief/'}
            currentPage={currentPage}
            numPages={numPages}
          />
        </Container>
      </Layout>
    );
  }
}

const Backlink = styled(Link)`
  ${tw`text-xs font-bold uppercase hover:underline`}
`;
const PageTitle = styled.h1`
  ${tw`text-left`}
`;
// const PageSubtitle = styled.h2`${tw`text-left`}`
// const EventsWrapper   = styled.section`${tw`py-16`}`
const EventsContainer = styled.div`
  ${tw`flex flex-wrap`}
`;
const EventItem = styled.div`
  ${tw`sm:w-1/2 md:w-1/3 lg:w-1/4 xl:w-1/4 mb-4`}
`;
const EventItemInner = styled.div`
  ${tw`w-full h-full `}
`;
const EventLink = styled(Link)`
  ${tw`block hover:bg-gray-200 h-full m-2 border border-solid border-gray-300`}
`;
const ImgWrapper = styled.img`
  ${tw`w-full `}
`;
const DetailsWrapper = styled.div`
  ${tw`text-black overflow-hidden px-4 pt-4 text-md`}
`;
const EventDate = styled.div`
  ${tw`text-sm`}
`;
const CalendarAltIcon = styled(CalendarAlt)`
  ${tw`text-gray-600 text-xs w-3 mr-1`}
`;
const EventItemTitle = styled.h4`
  ${tw`font-bold mb-2`}
`;

export default EventsArchivePage;

export const pageQuery = graphql`
  query eventListQuery($skip: Int!, $limit: Int!) {
    events: allMarkdownRemark(
      sort: { fields: frontmatter___start, order: ASC }
      filter: {
        frontmatter: { type: { eq: "event" } }
        fields: { isFuture: { eq: false } }
      }
      limit: $limit
      skip: $skip
    ) {
      edges {
        node {
          id
          timeToRead
          excerpt(pruneLength: 80)
          html
          frontmatter {
            title
            path
            date
            start
            end
            excerpt
            featuredImage {
              childImageSharp {
                fluid(maxWidth: 800) {
                  ...GatsbyImageSharpFluid
                }
                resize(width: 600, height: 400) {
                  src
                }
              }
            }
            tags
            categories
          }
        }
      }
    }
  }
`;

// class TagsList extends React.Component {
//   render() {
//     const {tags} = this.props;
//     if (!tags) { return null; }

//     return (
//       <TagsWrapper>
//         <TagsIcon />
//         { tags.map((tag, index) => (
//           <Tag key={index}>{tag}</Tag>
//         ))}
//       </TagsWrapper>
//     )
//   }
// }
// const TagsWrapper = styled.div`${tw``}`
// const TagsIcon = styled(Tags)`${tw`w-4 mr-1 text-gray-600`}`
// const Tag = styled.span`${tw`text-sm mr-2 text-gray-600`}`
