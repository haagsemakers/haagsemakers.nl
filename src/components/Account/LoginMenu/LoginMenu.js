import React, { Component } from 'react';
import { Link } from 'gatsby';
import styled from 'styled-components';
import tw from 'tailwind.macro';
import { getUserInfo } from '../../../utils/user.service';
import { isBrowser, isAuthenticated } from '../../../utils/auth.service';

class LoginMenu extends Component {

  constructor(props) {
    super(props);
    this.state = {
      userInfo: null,
      authenticated: null
    }
  }

  componentDidMount() {
    this.setState({
      userInfo: getUserInfo(),
      authenticated: isAuthenticated()
    })
  }

  render() {

    // Conditionalky return component
    if (!this.state.userInfo || !this.state.authenticated) {
      return <NavLink to='/login'>Login</NavLink>
    }
    else if (isBrowser) {
      return (
        <NavLink to='/settings'>
          <Avatar alt='avatar' src={ this.state.userInfo.picture } />
        </NavLink>
      )
    }

    return null;
  }
}

export default LoginMenu;

// Component styles
const NavLink = styled(Link)`${tw`block text-sm text-gray-800 p-4 lg:p-2 hover:underline`}`
const Avatar = styled.img`${tw`rounded-full h-8 w-8 p-0 m-0 mt-1 ml-3`};`
