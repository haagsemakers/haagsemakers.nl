import React from 'react';
import { Link } from 'gatsby';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import styled from 'styled-components';
import tw from 'tailwind.macro';
import { updateUser, updateAvatar } from '../../../utils/user.service'
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css';
import Thumb from "./Thumb";


const ProfileSchema = Yup.object().shape({
  nickname: Yup.string()
    .min(3, 'Minimaal 3 tekens. ')
    .max(15, 'Maximaal 15 tekens. '),
  firstname: Yup.string()
    .min(3, 'Minimaal 3 tekens. ')
    .max(15, 'Maximaal 15 tekens. '),
  lastname: Yup.string()
    .min(2, 'Minimaal 2 tekens. ')
    .max(50, 'Maximaal 50 tekens. '),
  bio: Yup.string()
    .min(5, 'Minimaal 5 tekens. ')
    .max(500, 'Maximaal 500 tekens. ')
});

const ProfileForm = ({ setFieldTouched, setFieldValue, profile }) => {
  const user = profile;
  if (!user) { return 'loading profile'; }

  return (

    <React.Fragment>
      <Formik
        initialValues = {{
          nickname: user.nickname || '',
          firstname: user.firstname || '',
          lastname: user.lastname || '',
          bio: user.bio || '' ,
          visibility: user.visibility || 'authenticated'
        }}
        validationSchema = { ProfileSchema }
        onSubmit = { async ( values, { setSubmitting }) => {
          let profile = {
            nickname: values.nickname,
            user_metadata: {
              firstname: values.firstname,
              lastname: values.lastname,
              bio: values.bio,
              visibility: values.visibility
            }
          }
          console.log('Sending profile values: ', profile);

          try {
            const result = await updateUser(profile);
            console.log('Update user result', result)
            toast.success('Je profiel is bijgewerkt', { autoClose: 2000});
          } catch (err) {
            toast.error('Er was een probleem met het updaten van je profiel');
            console.warn(err);
          }
          setSubmitting(false);
        }}
        >
        {({
          touched,
          errors,
          values,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
          setFieldValue
        }) => (
          <FormContainer>
            <FormWrapper onSubmit={handleSubmit}>
              <FormTitle>Profiel instellingen</FormTitle>
              <Notice>Deze informatie is zichtbaar op jouw <Link to={`/maker/${profile.slug}`}>publieke Haagse Makers profiel</Link>.</Notice>
              <FormRowWrapper>
                <FormLabelWrapper>Avatar</FormLabelWrapper>
                <Thumbwrapper><Thumb file={values.file} picture={user.picture}/></Thumbwrapper>
                <InputWrapper
                  id="file"
                  name="file"
                  type="file"
                  accept="image/*"
                  onChange={(event) => {
                    console.log('ProfileForm - onChange event', event);
                    const file = event.currentTarget.files[ 0];
                    if (!file) { return null; }
                    console.log('ProfileForm - onChange file', file);
                    if (file.size < 1*1024*1024) {
                      setFieldValue('file', file);
                      updateAvatar(file)
                        .then(result => {
                          console.log('Update user result', result)
                          toast.success('Je profielfoto is bijgewerkt', { autoClose: 2000});
                        })
                        .catch(err => {
                          toast.error('Er was een probleem met het updaten van je profielfoto');
                          setFieldValue('file', null);
                          console.warn(err);
                        });
                    }
                  }}
                  className="form-control"
                />
              </FormRowWrapper>

              <FormRowWrapper>
                <FormLabelWrapper>Gebruikersnaam *</FormLabelWrapper>
                <InputWrapper
                  type="text"
                  name="nickname"
                  id="nickname"
                />
                <ErrorMsgWrapper name="nickname" component="div" />
              </FormRowWrapper>

              <FormRowWrapper>
                <FormLabelWrapper>Voornaam</FormLabelWrapper>
                <InputWrapper type="text" name="firstname" />
                <ErrorMsgWrapper name="firstname" component="div" />
              </FormRowWrapper>

              <FormRowWrapper>
                <FormLabelWrapper>Achternaam</FormLabelWrapper>
                <InputWrapper type="text" name="lastname" />
                <ErrorMsgWrapper name="lastname" component="div" />
              </FormRowWrapper>

              <FormRowWrapper>
                <FormLabelWrapper>Bio</FormLabelWrapper>
                <InputWrapper component="textarea" rows="5" id="bio" name="bio" />
                <ErrorMsgWrapper name="bio" component="div" />
              </FormRowWrapper>

              <FormRowWrapper style={{position: 'relative'}}>
                <FormLabelWrapper>Zichtbaarheid van je profiel</FormLabelWrapper>
                <SelectWrapper component="select" name="visibility">
                  <option key={2} value="public">Publiek zichtbaar</option>
                  <option key={1} value="authenticated">Alleen zichtbaar voor geregistreerde gebruikers</option>
                  <option key={3} value="club">Alleen zichtbaar voor club-leden</option>
                  <option key={4} value="hidden">Niet zichtbaar</option>
                </SelectWrapper>
                <SelectArrow>
                  <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
                </SelectArrow>
                <ErrorMsgWrapper name="visibility" component="div" />
              </FormRowWrapper>

              <Button
                type="submit"
                disabled={isSubmitting}>
                Profiel instellingen updaten
              </Button>
            </FormWrapper>
          </FormContainer>
        )}
      </Formik>
    </React.Fragment>
  )
}

export default ProfileForm;

const FormContainer    = styled.div`${tw`w-full`}`
const FormTitle        = styled.h3`${tw`pl-0`}`
const Notice           = styled.p`${tw`mb-8` }`
const FormWrapper      = styled(Form)`${tw`block bg-white shadow-md rounded px-8 pt-2 mb-16 border border-solid border-gray-200`}`
const FormRowWrapper   = styled.div`${tw`flex flex-wrap -mx-3 mb-6`}`
const FormLabelWrapper = styled.label`${tw`block text-gray-700 text-sm font-bold mb-2`}`
const Thumbwrapper     = styled.div`${tw`block w-full mb-4`}`
const InputWrapper     = styled(Field)`${tw`bg-gray-200 appearance-none border-0 border-gray-200 rounded w-full py-2 px-2 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-blue-500`}`
const SelectWrapper    = styled(Field)`${tw`block appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-4 py-2 pr-8 rounded shadow leading-tight focus:outline-none focus:shadow-outline`}`
const SelectArrow    = styled.div`${tw`pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700`}`
const ErrorMsgWrapper  = styled(ErrorMessage)`${tw`text-red`}`
const Button = styled.button`${tw`py-2 px-4 font-bold bg-white cursor-pointer my-8 border-2 border-solid border-black hover:bg-gray-400`}`
