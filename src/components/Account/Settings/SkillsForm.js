import React from 'react';
import styled from 'styled-components';
import tw from 'tailwind.macro';
// import { updateSkills } from '../../../utils/user.service'
// import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css';

import TagsInput from 'react-tagsinput'
// import Autosuggest from 'react-autosuggest'
// import theme from './skillsform.css';


class SkillsForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      skill: '',
      skills: []
    }
  }

  // Validate input
  handleChangeInput = (skill) => {
    // Set max length
    if (skill.length>20) {
      this.setState({
        errMsg: 'max 20 tekens'
      })
      skill = skill.substring(0, 20);
    }
    // Replace special characted
    skill = skill.replace(/[^\w\s]/gi, '')
    // Add to state
    this.setState({ skill })
  }

  // Handle tags change
  handleChange = (skills) => {
    console.log('handle change', skills)
    // Save new tag
    this.setState({skills})
  }

  handleRemove = (skills) => {
    console.log('handle change', skills)
    // Save new tag
    this.setState({skills})
  }

  render() {

    return (
      <FormContainer>
        <FormWrapper>
          <FormTitle>Laat jouw skills zien! </FormTitle>
          <TagsInput
            value={this.state.skills}
            inputValue={this.state.skill}
            onChange={this.handleChange}
            onChangeInput={this.handleChangeInput}
            onRemove={this.handleRemove}
          />
        </FormWrapper>
      </FormContainer>
    )
  }
}

export default SkillsForm

const FormContainer    = styled.div`${tw`w-full max-w-md m-auto`}`
const FormWrapper      = styled.div`${tw`block bg-white shadow-md rounded px-8 py-6 mb-4 border border-solid border-gray-200`}`
const FormTitle        = styled.h3`${tw`pl-0`}`
