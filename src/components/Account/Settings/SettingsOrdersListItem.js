import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import tw from 'tailwind.macro';
import * as moment from 'moment';
import TicketBuyButton from '../../tickets/TicketBuyButton';
import TicketCancelOrderBtn from '../../tickets/TicketCancelOrderBtn';
import { getOrder } from '../../../utils/api.service';
import ModalOrderOverview from '../../tickets/modals/ModalOrderOverview';

class SettingsOrderListItem extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      order: props.order,
      payment: props.payment,
      showModalOrderOverview: false
    }
  }

  componentDidMount() {
    this.getActivePayment();
  }

  getActivePayment = () => {
    console.log('getActivePayment')
    if (this.state && (!this.state.order || !this.state.order.payments)) { return null; }

    const statusList = ['draft', 'open']
    const activePayments = this.state.order.payments.filter(payment => (statusList.indexOf(payment.status) !== -1));
    if (activePayments.length>0) {
      this.setState({ activePayment: activePayments[ 0]})
    }
    console.log('getActivePayment result: ', this.state.activePayment);
  }

  // Get the order status from the API
  fetchOrderStatus = (status) => {
    console.log(`[ SettingsOrderListItem ] fetchOrderStatus()`);
    getOrder(this.state.order.id)
      .then(order => {
        console.log(`[ SettingsOrderListItem ] fetchOrderStatus result`, order);
        if (order) {
          this.setState({ order: order.data });
        }
      })
      .catch(err => {
        console.warn(`[ SettingsOrderListItem ] fetchOrderStatus result`, err);
      })
      .finally(evt => {
        this.getActivePayment();
      });
  }

  // { moment(order.event.start).format('D MMMM YYYY - kk:mm') }
  render() {

    return (
      <TR>
        <TD><OrderDate>{ moment(this.state.order.order_date).format('D MMMM YYYY - kk:mm') }</OrderDate></TD>
        <TD><a href={this.state.order.event.url}>{ this.state.order.event.name }</a></TD>
        <TD>€ { ((this.state.order.ticket.cost + this.state.order.ticket.fee)/100).toFixed(2) }</TD>
        <TD><OrderStatus order={ this.state.order } payment={ this.state.activePayment } /></TD>
        <TD>
          <TicketBuyButton
            order={ this.state.order }
            event={ this.state.order.event }
            ticket={ this.state.order.ticket}
            payment={ this.state.activePayment }
            updateStatus={ this.fetchOrderStatus }
            buttonText="Betalen"
          />
          <ModalOrderOverview
            show={ this.state.showModalOrderOverview }
            order={ this.state.order }
          />
          <TicketCancelOrderBtn
            order={ this.state.order }
            payment={ this.state.activePayment }
            updateStatus={ this.fetchOrderStatus }
          />
        </TD>
      </TR>
    )
  }
}

SettingsOrderListItem.propTypes = {
  order: PropTypes.object.isRequired
}

export default SettingsOrderListItem;

const TR = styled.tr` ${ tw`text-sm hover:bg-gray-100` }`
const TD = styled.td`${tw`px-4 py-2 text-left`}`
const OrderDate = styled.div`${ tw`text-sm` }`



class OrderStatus extends React.Component {

  render() {
    const { order, payment } = this.props;
    if (!order) { return null; }

    let result = '';

    if (order.status === 'canceled') { return 'Geannuleerd'; }
    if (order.status === 'completed') { return <StatusCompleted>Voltooid</StatusCompleted> }
    if (order.status === 'pending') {
      if (!payment) { return 'Wachtend op nieuwe betaling'; }
      // Handle current payment status
      const { status, id } = payment;
      if (status === 'paid') {
        return <p>Betaling geslaagd (Payment id: { id })</p>
      } else if (status === 'open') {
        return <p>Wachtend op betaling (Payment id: { id })</p>
      } else if (status === 'expired') {
        return <p>Betaling verlopen (Payment id: { id })</p>
      } else if (status === 'canceled') {
        return <p>Betaling geannuleerd (Payment id: { id })</p>
      } else {
        return <p>onbekend (Payment id: { id })</p>
      }
    }

    return (
      <React.Fragment>
        { result }
      </React.Fragment>
    )
  }
}
OrderStatus.propTypes = {
  order: PropTypes.object.isRequired,
  payment: PropTypes.object
}

const StatusCompleted = styled.span`${ tw`block rounded border border-solid border-green-300 text-center text-green-900 bg-green-300 px-4 py-2` }`
