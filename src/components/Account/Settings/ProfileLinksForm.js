import React from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup'
import styled from 'styled-components'
import tw from 'tailwind.macro'
import { updateSocials } from './../../../utils/user.service'
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css';

const ProfileLinksSchema = Yup.object().shape({
  website: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!'),
  linkedin: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!'),
  twitter: Yup.string()
    .min(5, 'Too Short')
    .max(500, 'Too Long!'),
  instagram: Yup.string()
    .min(5, 'Too Short')
    .max(500, 'Too Long!')
});

const ProfileSocialLinksForm = ({
  profile
}) => {
  // restucture social
  let socials = {}
  profile.socials.map(item => {
    return socials[ item.title] = { url: item.url }
  })

  return (
    <React.Fragment>
      <Formik
        initialValues={{
          website: socials.website ? socials.website.url : '',
          linkedin: socials.linkedin ? socials.linkedin.url : '',
          twitter: socials.twitter ? socials.twitter.url : '',
          instagram: socials.instagram ? socials.instagram.url : ''
        }}
        validationSchema={ ProfileLinksSchema }
        onSubmit={async (values, { setSubmitting }) => {
          console.log('Sending profile values: ', values);
          try {
            const result = await updateSocials(values);
            console.log('Update user result', result)
            toast.success('Je profiel is bijgewerkt', { autoClose: 2000});
          } catch (err) {
            toast.error('Er was een probleem met het updaten van je profiel');
            console.warn(err);
          }
          setSubmitting(false);

        }}
      >
        {({
          isSubmitting,
          handleSubmit,
        }) => (
          <FormContainer>
            <FormWrapper onSubmit={handleSubmit}>
              <FormTitle>Social links</FormTitle>

              <FormLabelWrapper>Website</FormLabelWrapper>
              <InputWrapper type="text" name="website" />
              <ErrorMessage name="website" component="div" />

              <FormLabelWrapper>LinkedIn Public Profile URL</FormLabelWrapper>
              <InputWrapper type="text" name="linkedin" />
              <ErrorMessage name="website" component="div" />

              <FormLabelWrapper>Twitter</FormLabelWrapper>
              <InputWrapper type="text" name="twitter" />
              <ErrorMessage name="twitter" component="div" />

              <FormLabelWrapper>Instagram</FormLabelWrapper>
              <InputWrapper type="text" name="instagram" />
              <ErrorMessage name="instagram" component="div" />

              <Button
                type="submit"
                disabled={isSubmitting}>
                Social links updaten
              </Button>
            </FormWrapper>
          </FormContainer>
        )}
      </Formik>
    </React.Fragment>
  );
}

export default ProfileSocialLinksForm;

const FormContainer = styled.div`${tw``}`
const FormTitle = styled.h3`${tw`pl-0`}`
const FormWrapper = styled(Form)`${tw`block bg-white shadow-md rounded px-8 pt-6 mb-4 border border-solid border-gray-200`}`
const FormLabelWrapper = styled.label`${tw`block text-gray-700 text-sm font-bold mb-2`}`
const InputWrapper = styled(Field)`${tw`bg-gray-200 appearance-none border-0 border-gray-200 rounded w-full py-2 px-2 mb-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-solid focus:border focus:border-gray-200`}`
const Button = styled.button`${tw`py-2 px-4 font-bold bg-white cursor-pointer my-8 border-2 border-solid border-black hover:bg-gray-400`}`
