import React from 'react';

class Thumb extends React.Component {
  state = {
    loading: false,
    thumb: undefined,
    picture: null
  };

  componentWillReceiveProps(nextProps) {
    if (!nextProps.file) { return; }

    this.setState({ loading: true }, () => {
      let reader = new FileReader();

      reader.onloadend = () => {
        this.setState({ loading: false, thumb: reader.result });
      };

      reader.readAsDataURL(nextProps.file);
    });
  }

  render() {
    const { file, picture } = this.props;
    const { loading, thumb } = this.state;
    if (!file && !picture) { return null; }

    let imgSrc = thumb || picture;

    if (!imgSrc) { return null; }

    if (loading) { return <p>loading...</p>; }

    return (<img src={imgSrc}
      alt="avatar"
      height={100}
      width={100}
      style={{
        borderRadius: '50%'
      }} />);
  }
}

export default Thumb;
