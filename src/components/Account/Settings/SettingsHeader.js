import React from 'react'
import styled from 'styled-components';
import tw from 'tailwind.macro';
import SettingsMenu from './SettingsMenu';

const AccountHeader = ({ title, subtitle }) => {

  return (
    <WrapperStyle>
      <TitleStyle>{ title }</TitleStyle>
      <SettingsMenu />
    </WrapperStyle>
  )
}

export default AccountHeader;

// Component styles
const WrapperStyle = styled.header` ${tw`mb-4`}`
const TitleStyle   = styled.h1` ${tw``}`
