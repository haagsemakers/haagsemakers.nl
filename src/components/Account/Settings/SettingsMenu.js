import React, { Component } from 'react';
import { Link } from 'gatsby';
import styled from 'styled-components';
import tw from 'tailwind.macro';

class SettingsMenu extends Component {
  render() {
    const activeStyle = {
      color: 'black',
      borderLeft: '1px solid #efefef',
      borderTop: '1px solid #efefef',
      borderRight: '1px solid #efefef',
      borderTopLeftRadius: '3px',
      borderTopRightRadius: '3px',
      padding: '8px 16px',
      fontWeight: 600,
    };

    return (
      <Menu style={{ borderBottom: '1px solid #efefef' }}>
        <MenuItem>
          <MenuLink href="/settings" activeStyle={activeStyle}>
            Account
          </MenuLink>
        </MenuItem>
        <MenuItem>
          <MenuLink href="/settings/profile" activeStyle={activeStyle}>
            Profiel
          </MenuLink>
        </MenuItem>
        <MenuItem>
          <MenuLink href="/settings/orders" activeStyle={activeStyle}>
            Bestellingen
          </MenuLink>
        </MenuItem>
      </Menu>
    );
  }
}

export default SettingsMenu;

const Menu = styled.ul`
  ${tw`flex border-gray-200 p-0`}
`;
const MenuItem = styled.li`
  ${tw`-mb-px mr-1 list-none`}
`;
const MenuLink = styled(Link)`
  ${tw`bg-white inline-block py-2 px-4 text-blue-500 hover:text-blue-800 font-semibold`}
`;
