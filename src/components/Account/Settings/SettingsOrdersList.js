import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import tw from 'tailwind.macro';
import SettingsOrdersListItem from './SettingsOrdersListItem';

class SettingsOrderList extends React.Component {

  // constructor(props) {
  //   super(props);
  // }

  render() {
    console.log('[ SettingsOrderList ] props', this.props)
    if (!this.props.orders) { return null; }
    if (this.props.orders.length < 1) {
      return (
        <React.Fragment>
          <h3>Bestellingen</h3>
          <p>Je hebt nog geen bestellingen geplaatst. </p>
        </React.Fragment>
      )
    }
    return (
      <Wrapper>
        <h2>Bestellingen</h2>
        <Table>
          <thead>
            <tr>
              <TH>Besteldatum</TH>
              <TH>Event</TH>
              <TH>Prijs</TH>
              <TH>Bestelstatus</TH>
              <TH>Acties</TH>
            </tr>
          </thead>
          <tbody>
            { this.props.orders.map((order, key) =>
              <SettingsOrdersListItem
                order={ order }
                key={ key } />
            )}
          </tbody>
        </Table>
      </Wrapper>
    )
  }
}

SettingsOrderList.propTypes = {
  orders: PropTypes.array
}

export default SettingsOrderList;

const Wrapper = styled.div` ${ tw`pb-4` }`
const Table = styled.table`${tw`table-auto w-full border-collapse border-2 border-gray-500`}`
const TH = styled.th`${tw`px-4 py-2 text-left`}`
