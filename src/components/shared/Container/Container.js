import React from 'react';
import styled from 'styled-components'
import tw from 'tailwind.macro'

const Container = props => {
  return <Wrapper>{props.children}</Wrapper>
}

const Wrapper = styled.section`${tw`m-auto max-w-3xl`}`

export default Container
