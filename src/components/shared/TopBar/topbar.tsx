import { useFetchUser } from 'lib/user';
import React from 'react';

export const TopBar: React.FC = () => {
  const { user, loading } = useFetchUser();

  console.log('topbar', user);
  return (
    <div className="fixed top-0 left-0 right-0 text-xs text-white topbar bg-primary z-10">
      <div className="container">
        <div className="flex flex-row">
          <div className="flex flex-grow py-2 slogan">Voor vindingrijk Den Haag, wij gaan het maken!</div>
          <div className="flex py-2 userMenu">
            {!user && !loading && <a href="/api/login">Inloggen</a>}
            {user && !loading && <a href="/profile">Hoi {user.nickname}!</a>}
          </div>
        </div>
      </div>

      <style jsx>{`
        .topbar {
          font-size: 10px;
        }
      `}</style>
    </div>
  );
};
