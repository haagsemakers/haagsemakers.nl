// https://codeburst.io/creating-a-modal-dialog-with-tailwind-css-42722c9aea4f

import React from 'react';
import styled from 'styled-components';
import tw from 'tw.macro';

export default (data) => {

  return (
    <React.Fragment>
      <Backdrop />
      <Dialog />
    </React.Fragment>
  )
}

const Backdrop = styled.div`${tw`fixed pin z-50 overflow-auto bg-smoke-light flex`}`
const Dialog = styled.div`${tw`relative p-8 bg-white w-full max-w-md m-auto flex-col flex`}`
