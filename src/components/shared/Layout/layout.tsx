import { UserProvider } from 'lib/user';
import Head from 'next/head';
import React from 'react';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.minimal.css';
import { Footer } from '../Footer';
import { Header } from '../Header';
import { TopBar } from '../TopBar';

export const siteTitle = 'Haagse Makers';

interface LayoutProps {
  user?: any;
  loading?: boolean;
  children: React.ReactNode;
}

export const Layout: React.FC<LayoutProps> = ({ user, loading = false, children }: LayoutProps) => {
  return (
    <UserProvider value={{ user, loading }}>
      <Head>
        <link rel="icon" href="/favicon.ico" />
        <meta name="description" content="Learn how to build a personal website using Next.js" />
        <meta
          property="og:image"
          content={`https://og-image.now.sh/${encodeURI(
            siteTitle
          )}.png?theme=light&md=0&fontSize=75px&images=https%3A%2F%2Fassets.zeit.co%2Fimage%2Fupload%2Ffront%2Fassets%2Fdesign%2Fnextjs-black-logo.svg`}
        />
        <meta name="og:title" content={siteTitle} />
        <meta name="twitter:card" content="summary_large_image" />
      </Head>
      <div>
        <ToastContainer />
        <TopBar />
        <Header />
        <main className="main pb-16">{children}</main>
        <Footer />
      </div>

      <style jsx>{`
        .main {
          padding-top: 70px;
        }
      `}</style>
    </UserProvider>
  );
};
