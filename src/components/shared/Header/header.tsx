// import { useUser } from 'lib/user';
import Link from 'next/link';
import React, { useState } from 'react';
import { FaDiscourse } from 'react-icons/fa';

export const Header: React.FC = () => {
  // const { user, loading } = useUser();
  const [open, setOpen] = useState(false);

  const navItems = [
    //   // {
    //   //   title: 'Updates',
    //   //   href: '/updates',
    //   // },
    {
      title: 'Agenda',
      href: '/agenda',
    },
    //   // {
    //   //   title: 'Tools',
    //   //   href: '/advies/tools',
    //   // },
  ];

  return (
    <div className="fixed left-0 right-0 shadow-md main-nav bg-white z-10">
      <div className="container">
        <nav className="flex flex-wrap items-center justify-between py-2">
          <div className="flex flex-no-shrink">
            <Link href="/">
              <a className="font-bold text-md">
                <svg className="inline h-6 mr-2" viewBox="0 0 180 180" xmlns="http://www.w3.org/2000/svg">
                  <path
                    id="hm-vignet"
                    d="M62.3,45.8c-0.4-0.4-1-1-2.3-1c-1.7,0-3.6,1.7-3.6,6.3v12.2c6.2-6.9,7.2-10.5,7.2-12.2C63.7,48.8,63.2,46.9,62.3,45.8L62.3,45.8z M145.3,91c0,2.2-1.8,4-4,4H121c-1.4,0-2.6-0.7-3.4-1.9l-4.9-7.7L104,99c-0.7,1.2-2,1.9-3.4,1.9s-2.6-0.7-3.4-1.9l-8.7-13.6L79.9,99c-0.7,1.2-2,1.9-3.4,1.9s-2.6-0.7-3.4-1.9l-8.7-13.6L55.8,99c-0.7,1.2-2,1.9-3.4,1.9c-0.4,0-0.8-0.1-1.1-0.2c-1.7-0.5-2.9-2.1-2.9-3.8V82c-3.9,3.3-7.9,6.6-11.9,9.7c-1.1,0.9-2.2,1.7-3.1,2.4c-1.7,1.4-4.2,1.1-5.6-0.6s-1.1-4.2,0.6-5.6c0.9-0.7,1.9-1.6,3.1-2.5c4.5-3.6,10.9-8.7,16.9-14.1V51.2c0-4.1,1.1-7.6,3.1-10.2c2.1-2.7,5.2-4.2,8.5-4.2c5.4,0,11.6,3.8,11.6,14.3c0,7-6.4,15.3-15.2,23.7v8.3l4.7-7.3c0.7-1.2,2-1.9,3.4-1.9s2.6,0.7,3.4,1.9l8.7,13.6l8.7-13.6c0.7-1.2,2-1.9,3.4-1.9s2.6,0.7,3.4,1.9l8.7,13.6l8.7-13.6c0.7-1.2,2-1.9,3.4-1.9s2.6,0.7,3.4,1.9l7.1,11.2h18.1C143.5,87,145.3,88.8,145.3,91L145.3,91z M118.4,128.2c-8.3,9.2-20.1,14.5-32.5,14.5s-24.2-5.3-32.5-14.5c-1.5-1.6-1.4-4.2,0.3-5.7c1.6-1.5,4.2-1.4,5.7,0.3c6.8,7.5,16.4,11.8,26.5,11.8s19.8-4.3,26.5-11.8c1.5-1.6,4-1.8,5.7-0.3S119.9,126.6,118.4,128.2L118.4,128.2z M85.5,4c-21.8,0-42.2,8.5-57.6,23.9S4,63.7,4,85.5s8.5,42.2,23.9,57.6S63.7,167,85.5,167s42.2-8.5,57.6-23.9S167,107.2,167,85.5c0-21.8-8.5-42.2-23.9-57.6S107.3,4,85.5,4L85.5,4z"
                  />
                </svg>
                Haagse Makers
              </a>
            </Link>
          </div>
          <div className="block lg:hidden">
            <button
              onClick={() => setOpen(!open)}
              className="flex items-center px-3 py-2 border rounded text-grey border-teal-light hover:text-black hover:border-black"
            >
              <svg className="w-3 h-3" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                <title>Menu</title>
                <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z" />
              </svg>
            </button>
          </div>

          <div className={`w-full bg-white lg:flex lg:w-auto ${open ? ' block' : ' hidden'}`}>
            <div className={`text-sm flex items-center ${open ? ' mb-2' : ''}`}>
              {navItems.map((item, index) => (
                <Link key={index} href={item.href}>
                  <a className="block mt-2 mr-4 text-md lg:inline-block lg:mt-0 hover:text-black hover:underline">
                    {item.title}
                  </a>
                </Link>
              ))}
            </div>

            <a
              className="inline-block px-2 text-sm leading-loose border-2 border-black hover:no-underline hover:text-white lg:inline-block lg:mt-0 hover:bg-black"
              href="https://community.haagsemakers.nl"
            >
              <FaDiscourse className="inline-block mr-1 mb-1" />
              Community
            </a>
          </div>
        </nav>
      </div>
      <style jsx>{`
        .main-nav {
          top: 30px;
        }
      `}</style>
    </div>
  );
};
