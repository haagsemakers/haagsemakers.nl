import React, { Component } from 'react'
import Helmet from 'react-helmet'
import { isBrowser } from './../../utils/auth.service'

export default class DiscourseComments extends Component {

  render() {

    // Check if production environment
    const { NODE_ENV } = process.env
    if (NODE_ENV !== 'production') { return null; }

    // Get current path
    if (!isBrowser) { return null; }
    
    const embedUrl = window.location.href;

    const js = `
      DiscourseEmbed = {
        discourseUrl: 'https://community.haagsemakers.nl/',
        discourseEmbedUrl: '${ embedUrl }'
      };

      (function() {
        var d = document.createElement('script'); d.type = 'text/javascript'; d.async = true;
        d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d);
      })();
    `
    return (
      <div className="discourse-comments-wrapper">
        <h3>Reacties</h3>
        <div id='discourse-comments'></div>

        <Helmet>
          <script type="text/javascript">{ js }</script>
        </Helmet>
      </div>
    )
  }
}
