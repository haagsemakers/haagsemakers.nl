import React from 'react';
import { FaDiscourse, FaGitlab, FaInstagram, FaTelegram, FaTwitter } from 'react-icons/fa';

export const Footer: React.FC = () => {
  return (
    <footer className="py-8 text-sm text-center bg-blue-100">
      <div className="container">
        {/* <img src={logo} alt="Haagse Makers" /> */}
        <p className="mb-4 font-bold">
          Onafhankelijk platform voor vindingrijkheid, inspiratie en creatie in Den Haag.
        </p>
        <p className="mb-4 text-sm">
          <a className="underline hover:text-primary" href="mailto:hoemaaktuhet@haagsemakers.nl">
            hoemaaktuhet@haagsemakers.nl
          </a>
        </p>
        <p className="text-xs">Blijf op de hoogte</p>
        <div className="inline-block mt-4 text-center">
          <a
            className="inline-block mr-4 hover:text-primary"
            aria-label="instagram"
            href="https://instagram.com/haagsemakers"
            target="_blank"
            rel="noopener noreferrer"
          >
            <FaInstagram size={20} />
          </a>
          <a
            className="inline-block mr-4 hover:text-primary"
            aria-label="gitlab"
            href="https://gitlab.com/haagsemakers"
            target="_blank"
            rel="noopener noreferrer"
          >
            <FaGitlab size={20} />
          </a>
          <a
            className="inline-block mr-4 hover:text-primary"
            aria-label="twitter"
            href="https://twitter.com/haagsemakers"
            target="_blank"
            rel="noopener noreferrer"
          >
            <FaTwitter size={20} />
          </a>

          <a
            className="inline-block mr-4 hover:text-primary"
            aria-label="discourse"
            href="https://community.haagsemakers.nl"
            target="_blank"
            rel="noopener noreferrer"
          >
            <FaDiscourse size={20} />
          </a>
          <a
            className="inline-block mr-4 hover:text-primary"
            aria-label="discourse"
            href="https://t.me/haagsemakers"
            target="_blank"
            rel="noopener noreferrer"
          >
            <FaTelegram size={20} />
          </a>
        </div>

        {/* <a>
          <nav className="level">
            <Link href="/">Home</Link>&nbsp;&bull;&nbsp;
            <Link href="/contact">Contact</Link>&nbsp;&bull;&nbsp;
            <Link href="/algemene-voorwaarden">Algemene voorwaarden</Link>
            &nbsp;&bull;&nbsp;
            <Link href="/privacy">Privacy</Link>&nbsp;&bull;&nbsp;
            <Link href="/about">About</Link>&nbsp;&bull;&nbsp;
            <Link href="/manifest">Manifest</Link>&nbsp;&bull;&nbsp;
            <Link href="/presskit">Presskit</Link>
          </nav>
        </a> */}
      </div>
    </footer>
  );
};
