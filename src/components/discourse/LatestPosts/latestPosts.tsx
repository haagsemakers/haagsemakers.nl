import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import React, { useEffect, useState } from 'react';
import { FaDiscourse } from 'react-icons/fa';

dayjs.extend(relativeTime);

interface LatestPostsProps {
  limit?: number;
}

export const LatestPosts: React.FC<LatestPostsProps> = ({ limit = 10 }) => {
  const [loading, setLoading] = useState<boolean>(true);
  const [posts, setPosts] = useState<any>();

  useEffect(() => {
    const fetchPosts = async () => {
      try {
        const res = await fetch('https://community.haagsemakers.nl/posts.json?per_page=10', {
          method: 'get',
          headers: {
            'Content-Type': 'application/json',
          },
        });
        const resJson = await res.json();
        // console.log(resJson);
        const posts = resJson.latest_posts.slice(0, limit);
        setPosts(posts);
        setLoading(false);
      } catch (err) {
        console.warn(err);
      }
    };

    fetchPosts();
  }, []);

  return (
    <div>
      <div className="pb-2 border-b-2 border-b-primary ">
        <h3 className="text-xl mb-2">
          <FaDiscourse size="24" color="#0C5D9A" className="inline-block" /> Community
        </h3>
        <p className="text-gray-600">De laatste posts op het Haagse Makers Community Forum:</p>
      </div>
      {loading && <div>loading</div>}
      <div>
        {posts &&
          posts.map((post: any) => (
            <div key={post.id} className="py-4 border-b border-b-primary">
              <a className="hover:underline" href={`https://community.haagsemakers.nl/t/${post.topic_slug}`}>
                <div className="">{post.topic_title}</div>
              </a>
              <img
                className="inline-block mr-2"
                src={`https://community.haagsemakers.nl${post.avatar_template.replace('{size}', 16)}`}
              />
              <div className="inline-block text-xs font-bold">{dayjs(post.updated_at).fromNow()}</div>
            </div>
          ))}
      </div>
    </div>
  );
};
