import React, { Component } from 'react'
import styled from 'styled-components'
import { Link } from 'gatsby'

class MakersListingItem extends Component {

  render() {

    const UserItem = styled.div`
      padding: 10px;
      margin: 10px;
      text-align: center;
      grid-column-end: span 6;
    `
    const UserPicture = styled.img`
      height: 130px;
      width: auto;
      border-radius: 50%;
      margin-bottom: 0;
    `
    const Nickname = styled.div`
      text-transform: uppercase;
      font-weight: 600;
      font-size: 0.9rem;
    `
    const { maker } = this.props;

    if (!maker) { return null }

    return (

      <UserItem>
        <Link to={ `/maker/${maker.slug}` }>
          <UserPicture src={maker.picture} alt={ maker.nickname } />
          <Nickname>{maker.nickname}</Nickname>
        </Link>
      </UserItem>
    )
  }
}

export default MakersListingItem
