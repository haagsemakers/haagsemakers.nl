import React, { Component } from 'react';
import { Link } from 'gatsby';
import styled from 'styled-components';
import Container from './../../components/shared/Container';
import { globalHistory as history } from '@reach/router';
import { searchUsers, getUserInfo } from './../../utils/user.service';
import { isAuthenticated } from './../../utils/auth.service';
import MakersListingPermission from './MakersListingPermission';
import MakersListingError from './MakersListingError';
import MakerProfileHeader from './MakerProfileHeader';
import MakerProfileBody from './MakerProfileBody';
import MakerProfileSocials from './MakerProfileSocials';

class MakerProfilePage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isAuthenticated: isAuthenticated(),
      user: getUserInfo(),
      slug: null,
      profile: null,
      error: null,
    };
  }

  // Fetch profile from server
  async fetchProfile() {
    const result = await searchUsers({ qs: { slug: this.state.slug } });
    if (result && result[0]) {
      this.setState({ profile: result[0] });
    }
  }

  // Fetch maker profile dynamically from API
  componentDidMount() {
    const { location } = history;
    try {
      const slug = location.pathname.split('/')[2];
      this.setState({ slug: slug });
    } catch (err) {
      console.warn(err);
    }
  }

  render() {
    const BackLink = styled.div`
      font-size: 12px;
      font-weight: 600;
      text-transform: uppercase;
    `;

    // Permission helper function
    const hasPermission = () => {
      return !(
        !this.state.isAuthenticated ||
        !this.state.user ||
        this.state.user.account_plan !== 'club'
      );
    };

    const Loader = () => {
      if (hasPermission() && !this.state.profile) {
        return (
          <div style={{ textAlign: 'center', padding: '100px', color: '#888' }}>
            Profiel wordt geladen...
          </div>
        );
      }
      return null;
    };

    // Initiate search if has permission
    if (hasPermission && !this.state.profile) {
      this.fetchProfile();
    }

    return (
      <Container>
        <BackLink>
          <Link href="/de-makers">&larr; De makers</Link>
        </BackLink>
        <MakersListingPermission permission={hasPermission()} />
        <MakersListingError error={this.state.error} />
        <MakerProfileHeader maker={this.state.profile} />
        <MakerProfileSocials maker={this.state.profile} />
        <MakerProfileBody maker={this.state.profile} />
        <Loader />
      </Container>
    );
  }
}

export default MakerProfilePage;
