import React, { Component } from 'react'

class MakersListingPermission extends Component {

  render() {

    const { permission } = this.props;

    if (!permission) {
      return (
        <p>You have to be a Haagse Makers Club Member to view maker profiles.</p>
      )
    }

    return null;

  }
}

export default MakersListingPermission
