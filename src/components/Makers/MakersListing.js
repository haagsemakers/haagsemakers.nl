import React, { Component } from 'react'
import styled from 'styled-components'
import MakersListingItem from './MakersListingItem'

class MakersListing extends Component {

  render() {

    const { makers } = this.props;
    if (!makers) { return null; }

    const MakersListing = styled.div`
      display: grid;
      grid-gap: 10px;
      width: auto;
      grid-template-columns: repeat(12, minmax(0, 1fr));
    `

    return (
      <MakersListing>
        { makers.map(data => (
          <MakersListingItem key={data.id} maker={ data }/>
         )) }
      </MakersListing>
    )
  }
}

export default MakersListing
