import React, { Component } from 'react'
import styled from 'styled-components'

class MakerProfileHeader extends Component {

  render() {

    const Header = styled.header`
      padding: 30px 0;
      text-align: center;
      border-bottom: 1px solid #efefef;
      border-top: 1px solid #efefef;
      margin-bottom: 40px;
      margin-top: 40px;
    `
    const UserPicture = styled.div`
      width: 150px;
      img {
        height: 100px;
        width: auto;
        border-radius: 50%;
        margin-bottom: 0;
      }
      display: inline-block;
    `
    const Nickname = styled.div`
      text-transform: uppercase;
      font-weight: 600;
      font-size: 0.9rem;
      display: inline-block;
      line-height: 40px;
      width: 300px;
      text-align: left;
      vertical-align: top;
    `
    const { maker } = this.props;

    if (!maker) { return null }

    return (

      <Header>
        <UserPicture><img src={maker.picture} alt={ maker.nickname } /></UserPicture>
        <Nickname>
          <div style={{}}>{maker.nickname}</div>
          <div style={{fontWeight: '400', textTransform: 'initial' }}>{ maker.firstname } { maker.lastname }</div>
        </Nickname>
      </Header>
    )
  }
}

export default MakerProfileHeader
