import React, { Component } from 'react'

class MakersListingError extends Component {

  render() {

    const { error } = this.props;

    if (error) {
      return (
        <>
          <p>There was an error fetching maker profiles.</p>
          <pre>{ JSON.stringify(error) }</pre>
        </>
      )
    }

    return null;

  }
}

export default MakersListingError
