import React, { Component } from 'react'
import styled from 'styled-components'
import { FaLinkedin, FaInstagram, FaTwitter, FaGlobeEurope } from 'react-icons/fa';

class MakerProfileBody extends Component {

  render() {

    const { maker } = this.props;
    if (!maker || !maker.socials || (maker.socials.length<1)) {
      return null
    }

    const MakerSocialsStyle = styled.div`
      text-align: center;
      margin-bottom: 20px;
    `

    return (
      <MakerSocialsStyle>
        { maker.socials.map(item => (
          <MakerProfileSocialLink item={item} />
        ))}
      </MakerSocialsStyle>
    )
  }
}

export default MakerProfileBody

export class MakerProfileSocialLink extends Component {

  render() {

    const { item } = this.props;

    const SocialLink = styled.a`
      margin: 5px;
      color: #333;
    `

    if (!item) { return null; }

    let r;
    switch(item.title) {
      case 'website':
        r = <SocialLink href={ item.url } alt={item.title}><FaGlobeEurope /></SocialLink>;
        break;
      case 'twitter':
        r = <SocialLink href={ item.url }  alt={item.title}><FaTwitter /></SocialLink>
        break;
      case 'linkedin':
        r = <SocialLink href={ item.url }  alt={item.title}><FaLinkedin /></SocialLink>
        break;
      case 'instagram':
        r = <SocialLink href={ item.url }  alt={item.title}><FaInstagram /></SocialLink>
        break;
      default:
        return null;
    }
    return r;
  }
}
