import React, { Component } from 'react'
import styled from 'styled-components'

class MakerProfileBody extends Component {

  render() {

    const { maker } = this.props;
    if (!maker) { return null }

    const MakerBodyStyle = styled.div`
      text-align: center;
    `

    return (
      <MakerBodyStyle>
        { maker.bio }
      </MakerBodyStyle>
    )
  }
}

export default MakerProfileBody
