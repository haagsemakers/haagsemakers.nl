import React from 'react';
import { Link } from 'gatsby';
import styled from 'styled-components';
import tw from 'tailwind.macro';
import {
  // Tags,
  CalendarAlt,
} from '@styled-icons/fa-solid';
class ArticleFrontListing extends React.Component {
  render() {
    const { articles } = this.props;

    return (
      <Wrapper>
        <h2>Nieuw op Haagse Makers</h2>
        <ItemsWrapper>
          <Items>
            {articles.edges.map(({ node }, ...index) => (
              <Item key={index}>
                <ItemInner to={node.frontmatter.path}>
                  {node.frontmatter.featuredImage && (
                    <ItemImgWrapper>
                      <ItemImg
                        src={
                          node.frontmatter.featuredImage.childImageSharp.resize
                            .src
                        }
                        alt={
                          node.frontmatter.featuredImage.childImageSharp.resize
                            .originalImg
                        }
                      />
                    </ItemImgWrapper>
                  )}
                  <ItemPreview>
                    <ItemMeta>
                      <DateWrapper>
                        <CalendarAltIcon /> {node.frontmatter.date}
                      </DateWrapper>
                      {node.frontmatter.sections && (
                        <SectionsWrapper>
                          {node.frontmatter.sections.map((section, index) => (
                            <SectionItem key={index}>{section}</SectionItem>
                          ))}
                        </SectionsWrapper>
                      )}
                    </ItemMeta>
                    <ItemTitle>{node.frontmatter.title}</ItemTitle>
                    {/* {node.frontmatter.tags && (
                    <TagsWrapper>
                      <TagsIcon />
                      { node.frontmatter.tags.join(', ')}
                    </TagsWrapper>
                  )} */}
                    <ItemExcerpt>{node.frontmatter.excerpt}</ItemExcerpt>
                  </ItemPreview>
                </ItemInner>
              </Item>
            ))}
          </Items>
        </ItemsWrapper>

        <ButtonWrapper>
          <Button href="/artikelen">Alle artikelen &rarr;</Button>
        </ButtonWrapper>
      </Wrapper>
    );
  }
}

const Wrapper = styled.div`
  ${tw`my-8 py-8`};
  border-top: 1px solid #ddd;
  border-bottom: 1px solid #ddd;
`;
const ItemsWrapper = styled.div`
  ${tw``}
`;
const Items = styled.ul`
  ${tw`flex flex-wrap list-none pl-0`}
`;
const Item = styled.li`
  ${tw`w-full md:w-1/3 mb-4`};
  // a:hover img {
  //   transform: translateZ(0) scale3d(1.04,1.04,1.04);
  // }
`;
const ItemInner = styled(Link)`
  ${tw`text-black hover:no-underline block p-2 overflow-hidden`};
`;
const ItemPreview = styled.div`
  ${tw``}
`;
const ItemTitle = styled.h4`
  ${tw`mt-2`}
`;
const ItemMeta = styled.div`
  ${tw`flex`}
`;
const DateWrapper = styled.div`
  ${tw`text-gray-500 text-sm`}
`;
const CalendarAltIcon = styled(CalendarAlt)`
  ${tw`text-gray-500 text-xs w-3 mr-1`}
`;
const SectionsWrapper = styled.div`
  ${tw`text-sm`}
`;
const SectionItem = styled.span`
  ${tw`text-gray-500 px-2 py-1 bg-gray-300 rounded ml-1 text-gray-600`}
`;
const ItemExcerpt = styled.p`
  ${tw`mt-1`}
`;
const ItemImgWrapper = styled.div`
  ${tw`overflow-hidden inline-block mb-2`}
`;
const ItemImg = styled.img`
  ${tw`w-full`};
  -webkit-transition: all 0.2s ease;
  -moz-transition: all 0.2s ease;
  -ms-transition: all 0.2s ease;
  -o-transition: all 0.2s ease;
  transition: all 0.3s ease;
  vertical-align: middle;

  &:hover {
    -webkit-transform: scale(1.1); /* Safari and Chrome */
    -moz-transform: scale(1.1); /* Firefox */
    -ms-transform: scale(1.1); /* IE 9 */
    -o-transform: scale(1.1); /* Opera */
    transform: scale(1.1);
  }
`;
const ButtonWrapper = styled.div`
  ${tw`text-center`}
`;
const Button = styled(Link)`
  ${tw`border-2 border-solid border-black px-4 py-2 m-2 inline-block cursor-pointer text-black hover:bg-blue-500 hover:border-blue-500 hover:text-white`}
`;

export default ArticleFrontListing;
