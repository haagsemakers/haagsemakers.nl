import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'gatsby'
import styled from 'styled-components'
import tw from 'tailwind.macro'
import { Tags } from '@styled-icons/fa-solid/Tags'
import { kebabCase } from 'lodash'
import { CalendarAlt } from '@styled-icons/fa-solid/CalendarAlt'

class ArticleDetails extends React.Component {
  render() {
    const { article } = this.props
    if (!article) {
      return null
    }

    return (
      <MetaBox>
        {article.frontmatter.date && (
          <Date>
            <CalendarAltIcon /> {article.frontmatter.date}
          </Date>
        )}
        {article.frontmatter.tags && (
          <TagsList tags={article.frontmatter.tags} />
        )}
        {article.frontmatter.timeToRead && (
          <TTR>
            Leestijd: {this.props.ttr}{' '}
            {this.props.ttr === 1 ? 'minuut' : 'minuten'}
          </TTR>
        )}
        {article.frontmatter.author && (
          <Author>Door: {article.frontmatter.author.name}</Author>
        )}
      </MetaBox>
    )
  }
}

const Author = styled.div`
  ${tw`mb-1`}
`
const TTR = styled.div`
  ${tw`mb-1`}
`
const Date = styled.div`
  ${tw`mb-1`}
`
const CalendarAltIcon = styled(CalendarAlt)`
  ${tw`text-gray-800 text-sm w-3 mr-1`}
`

ArticleDetails.propTypes = {
  article: PropTypes.object.isRequired,
}

export default ArticleDetails

const MetaBox = styled.div`
  ${tw`text-sm mb-4 text-gray-800`}
`

class TagsList extends React.Component {
  render() {
    const { tags } = this.props

    if (!tags) {
      return null
    }

    return (
      <TagsWrapper>
        <TagsIcon />
        {tags.map((tag, index) => (
          <Link key={index} to={`/tags/${kebabCase(tag)}`}>
            <Tag>{tag}</Tag>
          </Link>
        ))}
      </TagsWrapper>
    )
  }
}
const TagsWrapper = styled.div`
  ${tw`mb-1`}
`
const TagsIcon = styled(Tags)`
  ${tw`w-3 mr-1 text-gray-800`}
`
const Tag = styled.span`
  ${tw`text-sm mr-2 text-gray-800 hover:underline`}
`
