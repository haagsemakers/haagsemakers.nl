import React, { Component } from 'react';
import { Link } from 'gatsby';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import tw from 'tailwind.macro';
import ArticleDetails from './ArticlePageDetails';

class ArticleHeader extends Component {
  render() {
    const {
      article,
      article: { frontmatter },
    } = this.props;

    return (
      <HeaderStyle>
        <BacklinkStyle href="/artikelen">&larr; Artikelen</BacklinkStyle>
        {frontmatter.sections && (
          <SectionsWrapper>
            {frontmatter.sections.map((section, index) => (
              <SectionItem key={index}>{section}</SectionItem>
            ))}
          </SectionsWrapper>
        )}
        <TitleStyle>{frontmatter.title}</TitleStyle>
        <ExcerptStyle>{frontmatter.excerpt}</ExcerptStyle>
        <ArticleDetails article={article} />
      </HeaderStyle>
    );
  }
}

export default ArticleHeader;

ArticleHeader.propTypes = {
  article: PropTypes.object.isRequired,
};

// Styles
const HeaderStyle = styled.header`
  ${tw`py-8`}
`;
const BacklinkStyle = styled(Link)`
  ${tw`text-sm font-bold uppercase hover:underline`}
`;
const SectionsWrapper = styled.div`
  ${tw`text-lg mb-4 mt-8`}
`;
const SectionItem = styled.span`
  ${tw`text-gray-500 py-1 px-2 bg-gray-300 rounded text-gray-800`}
`;
const TitleStyle = styled.h1``;
const ExcerptStyle = styled.h2`
  ${tw`text-2xl font-light`}
`;
