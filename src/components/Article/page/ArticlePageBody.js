import React, { Component } from 'react'
import styled from 'styled-components'
import tw from 'tailwind.macro'
import Img from 'gatsby-image'

class ArticleBody extends Component {

  render() {

    const {article} = this.props;

    return(
      <Wrapper>
        {article.frontmatter.featuredImage && (
          <FeaturedImage fluid={article.frontmatter.featuredImage.childImageSharp.fluid } />
        )}
        <InnerStyle dangerouslySetInnerHTML={{ __html: article.html }} />
      </Wrapper>
    )
  }
}
export default ArticleBody

// Styles
const Wrapper = styled.div`${tw``}`
const FeaturedImage = styled(Img)`${tw`mb-8`}`
const InnerStyle = styled.div`${tw``}`
