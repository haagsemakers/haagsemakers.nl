import React, { Component } from 'react'
import { Link } from 'gatsby'
import PropTypes from 'prop-types';
import styled from 'styled-components'
import tw from 'tailwind.macro'

class ArticleFooter extends Component {

  render() {

    const { context: { previous, next }} = this.props;
    
    return (
      <FooterStyle>
        <Wrapper
          style={{
            justifyContent: `space-between`,
            listStyle: `none`
          }}
        >
          <Item>
            {next && (
              <InnerLeft>
                <LinkWrapper to={next.frontmatter.path} rel="prev">
                  ← Volgend artikel<br />
                  <strong>{next.frontmatter.title}</strong>
                </LinkWrapper>
              </InnerLeft>
            )}
          </Item>
          <Item>
            {previous && (
              <InnerRight>
                <LinkWrapper to={previous.frontmatter.path} rel="next">
                  Vorig artikel →<br />
                  <strong>{previous.frontmatter.title}</strong>
                </LinkWrapper>
              </InnerRight>
            )}
          </Item>
        </Wrapper>
      </FooterStyle>
    )
  }
}

export default ArticleFooter;

ArticleFooter.propTypes = {
  article: PropTypes.object.isRequired,
  context: PropTypes.object.isRequired
}

// Styles
const FooterStyle = styled.footer`${tw``}`
const Wrapper = styled.div`
  ${tw`flex flex-wrap my-8 p-0`};
  border-top: 1px solid #edf2f7;
  border-bottom: 1px solid #edf2f7;
`
const Item = styled.div`${tw`w-1/2`}`
const InnerLeft = styled.div`${tw`mr-2 h-full`}`
const InnerRight = styled.div`${tw`ml-2 h-full text-right`}`
const LinkWrapper = styled(Link)`${tw`block w-full h-full p-1 text-sm text-black`}`
