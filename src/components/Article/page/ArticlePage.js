import React, { Component } from 'react'
// import styled from 'styled-components'
// import tw from 'tailwind.macro'
import PropTypes from 'prop-types'
import Container from '../../../components/shared/Container'
import ArticleHeader from './ArticlePageHeader'
import ArticleBody from './ArticlePageBody'
import ArticleFooter from './ArticlePageFooter'
import DiscourseComments from '../../../components/shared/DiscourseComments'

class EventPage extends Component {

  render() {

    const { article, context } = this.props;
    
    return (
      <Container>
        <ArticleHeader article={ article } />
        <ArticleBody article={ article } />
        <ArticleFooter article={article} context={context} />
        <DiscourseComments />
      </Container>
    )
  }
}

EventPage.propTypes = {
  article: PropTypes.object.isRequired
}

export default EventPage;
