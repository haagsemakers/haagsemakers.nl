import React, { useState } from 'react';
import {
  Configure,
  Highlight,
  Hits,
  InstantSearch,
  Pagination,
  // RefinementList,
  SearchBox,
} from 'react-instantsearch-dom';
import { indexName, searchClient } from './AlgoliaSearch/instantsearch';

const HitComponent = ({ hit }: any) => {
  return (
    <div className="hit">
      <div className="hit-content">
        <div className="">
          <a href={`https://community.haagsemakers.nl${hit.url}`} className="hover:bg-blue-100 block p-4">
            <Highlight attribute="name" hit={hit} />
            <span className="font-bold mr-4 text-lg mb-2">{hit.topic.title}</span>

            <div className="hit-description">
              <Highlight attribute="content" hit={hit} />
            </div>
          </a>
        </div>
      </div>
    </div>
  );
};

export const InstantSearchFront: any = () => {
  const [searchState, setSearchState] = useState<any>();

  // useEffect(() => {
  //   console.log('useeffect', searchState);
  // }, [searchState]);

  return (
    <InstantSearch
      indexName={indexName}
      searchClient={searchClient}
      onSearchStateChange={(searchState: any) => setSearchState(searchState)}
    >
      <Configure
        hitsPerPage={10}
        attributesToSnippet={['description:10']}
        snippetEllipsisText="…"
        removeWordsIfNoResults="allOptional"
      />

      <SearchBox
        autoFocus={true}
        translations={{
          placeholder: 'Wat heb je nodig om het te gaan maken? ',
        }}
      />
      {searchState && searchState?.query !== '' && (
        <>
          <div className="bg-white mt-4 text-left rounded">
            <Hits hitComponent={HitComponent} />
          </div>
          <footer>
            <Pagination />
          </footer>
        </>
      )}
    </InstantSearch>
  );
};
