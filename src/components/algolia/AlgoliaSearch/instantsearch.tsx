import algoliasearch from 'algoliasearch/lite';
import { findResultsState } from 'react-instantsearch-dom/server';

const indexName = 'discourse-posts';

const searchClient = algoliasearch('S8FXEJCKZY', 'c5df60ca649b2e15f0beae22155825eb');

export { findResultsState, indexName, searchClient };
