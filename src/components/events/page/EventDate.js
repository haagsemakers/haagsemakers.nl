import React from 'react';
import styled from 'styled-components'
import tw from 'tailwind.macro'
import moment from 'moment'

class EventDate extends React.Component {

  render() {
    const {start, end} = this.props; 
    if (!start) { return null; }
    
    let s = moment(start);
    if (!end) {
      return (
        <DateWrapper>
          <DateItem>
            <Day>{s.format('dddd D MMMM YYYY - kk:mm')}</Day>
            <Time>{s.format('kk:mm')}</Time>
          </DateItem>
        </DateWrapper>
      )
    }
    
    let e = moment(end);
    const isSameDay = moment(s).isSame(e, 'day'); 

    if (isSameDay) {
      return (
        <DateWrapper>
          <DateItem>
            <Day>{s.format('dddd D MMMM YYYY')}<br/></Day>
            <Time>{s.format('kk:mm')}{e.format(' - kk:mm')}</Time>
          </DateItem>
        </DateWrapper>
      )
    }
    
    return (
      <DateWrapper>
        <DateItem>
          <Label>van</Label>
          <Day>{s.format('dddd D MMMM YYYY')}</Day>
          <Time>{s.format('kk:mm')}</Time>
        </DateItem>
        <DateItem>
          <Label>tot</Label>
          <Day>{e.format('dddd D MMMM YYYY')}</Day>
          <Time>{s.format('kk:mm')}</Time>
        </DateItem>
      </DateWrapper>
    )
  }
}

const DateWrapper = styled.div`${tw`text-xl font-bold pb-4`}`
const DateItem = styled.div`${tw`mb-2`}`
const Label = styled.div`${tw`font-normal lowercase text-sm text-gray-600`}`
const Day = styled.div`${tw``}`
const Time = styled.span`${tw`font-normal`}`

export default EventDate
