import React from 'react'
import Img from 'gatsby-image'
import styled from 'styled-components'
import tw from 'tailwind.macro'

class EventOrganizerLogo extends React.Component {

  render() {
    
    const { organizer, showLink } = this.props;
    const hasLink = organizer.website; 
    const hasLogo = organizer.logo.childImageSharp; 
    
    if (hasLogo && hasLink && showLink) {

      return (
        <a href={organizer.website} target="_blank" rel="noopener noreferrer">
          <LogoContainer>
            <Logo 
              style={{ maxHeight: "100%" }}
              imgStyle={{ 
                objectFit: "contain",
                objectPosition: "center left"
              }}
              fluid={organizer.logo.childImageSharp.fluid} alt={organizer.name} />
          </LogoContainer>
        </a>
      )
    }

    if (hasLogo) {
      return (
        <LogoContainer>
          <Logo fluid={organizer.logo.childImageSharp.fluid} alt={organizer.name} />
        </LogoContainer>
      )
    }

    return null;
  }
}
const LogoContainer = styled.div`${tw`h-12 text-left`}`
const Logo = styled(Img)`${tw``}`

export default EventOrganizerLogo;
