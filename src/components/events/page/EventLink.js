import React from 'react';
import styled from 'styled-components'
import tw from 'tailwind.macro'

class EventLinkComponent extends React.Component {
  
  render() {
    
    const { url } = this.props;
    if (!url) { return null; }
    
    return (
      <EventLinkWrapper>
        <EventLink href={url} target="_blank" rel="noopener noreferrer">Bekijk dit event op de pagina van de organisator<br/>&rarr;</EventLink>
      </EventLinkWrapper>
    )
  }
}

const EventLinkWrapper = styled.div`${tw`my-8`}`
const EventLink = styled.a`${tw`py-4 px-4 block text-black text-center border-3 border-blue-500 border-solid rounded hover:bg-blue-500 hover:text-white`}`

export default EventLinkComponent; 
