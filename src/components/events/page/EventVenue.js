import React, { Component } from 'react';
import styled from 'styled-components'

class EventVenue extends Component {

  render() {

    const { venue } = this.props;

    if (!venue) { return null; }

    const VenuesStyle = styled.div`
      font-size: 14px;
      margin-bottom: 1.5rem;
      display: block;
    `
    const VenueStyle = styled.span`
      text-transform: uppercase;
      font-size: 12px;
      font-weight: 600;
    `

    return (
      <VenuesStyle>
        Locatie: <br />
        <VenueStyle>{ venue.name }</VenueStyle>
      </VenuesStyle>
    )
  }
}

export default EventVenue
