import React, { Component } from 'react'
import styled from 'styled-components'
import tw from 'tailwind.macro'
import OrganizationLogo from './EventOrganizerLogo'

class EventOrganizer extends Component {

  render() {

    const { organizer } = this.props;
    if (!organizer) { return null; }

    if (organizer.logo) {
      return (<OrganizationLogo organizer={organizer} showLink={true} />)
    }
    return (
      <OrganizerStyle>{ organizer.name }</OrganizerStyle>
    )
  }
}

const OrganizerStyle = styled.div`${tw`uppercase text-sm text-bold bg-gray-200 rounded py-4 pr-2 mb-2 text-center`}`

export default EventOrganizer; 
