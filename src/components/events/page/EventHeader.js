import React, { Component } from 'react'
import styled from 'styled-components'
class EventHeader extends Component {

  render() {

    const { event } = this.props;

    const Cell = styled.div`
      grid-column-end: span 12;
    `

    return (
      <Cell>
        <header>
          <h1>{event.frontmatter.title}</h1>
          <h2>{event.frontmatter.description}</h2>
        </header>
      </Cell>
    )
  }
}

export default EventHeader;
