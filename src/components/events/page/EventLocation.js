import React from 'react'
import styled from 'styled-components'
import tw from 'tailwind.macro'
import EventLocationLogo from './EventLocationLogo' 

class EventLocation extends React.Component {

  render() {
    const { location } = this.props;

    if (!location) { return null; }

    if (location.logo) {
      return (
        <EventLocationLogo location={location} />
      )
    }
    return (
      <Location>{ location.name }</Location>
    )
  }
}

const Location = styled.div`${tw``}`

export default EventLocation;