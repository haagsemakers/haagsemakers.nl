import React from 'react'
import Img from 'gatsby-image'
import styled from 'styled-components'
import tw from 'tailwind.macro'

class EventLocationLogo extends React.Component {

  render() {
    
    const { location } = this.props;
    const hasLogo = location.logo.childImageSharp; 
    
    if (!hasLogo) { return null; }

    return (
      <LogoWrapper>
        {/* <Logo 
            style={{ maxHeight: "100%" }}
            imgStyle={{ 
              objectFit: "contain",
              objectPosition: "center left"
            }}
            fluid={location.logo.childImageSharp.fluid} alt={location.name} /> */}
        <Logo fluid={location.logo.childImageSharp.fluid} alt={location.name} /> 
      </LogoWrapper>
    )
  }
}
const LogoWrapper = styled.div`${tw``}`
const Logo = styled(Img)`${tw``}`

export default EventLocationLogo;
