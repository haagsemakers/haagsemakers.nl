import React, { Component } from 'react'
import styled from 'styled-components'
import tw from 'tailwind.macro'
import EventLocation from './EventLocation'

class EventLocations extends Component {

  render() {
    
    const { locations } = this.props;

    if (!locations) { return null; }

    return (
      <LocationsWrapper>
        <Label>Locatie: </Label>
        { locations.map(location =>
          <EventLocation location={location} key={location.id}></EventLocation>
        )}
      </LocationsWrapper>
    )
  }
}

// Styles
const LocationsWrapper = styled.div`${tw`py-4 text-sm `}`
const Label = styled.div`${tw`font-bold`}`

export default EventLocations
