import React, { Component } from 'react';
import { Link } from 'gatsby';
import Container from '../../../components/shared/Container';
import EventHeader from './EventHeader';
import EventDetails from './EventDetails';
import TicketsListing from '../../tickets/TicketsListing';
import EventBody from './EventBody';
import Attendees from '../../tickets/Attendees';
import styled from 'styled-components';
import tw from 'tailwind.macro';
import DiscourseComments from '../../../components/shared/DiscourseComments';

class EventPage extends Component {
  render() {
    const { event } = this.props;
    if (!event) {
      return null;
    }

    return (
      <Container>
        <Backlink href="/agenda">&larr; Makers agenda</Backlink>

        <HeaderWrapper event={event}></HeaderWrapper>

        <EventBodyWrapper>
          <Left>
            <EventBody event={event}></EventBody>
          </Left>
          <Right>
            <RightInner>
              <EventDetails event={event}></EventDetails>
              <TicketsListing event={event} />
              <Attendees event={event}></Attendees>
            </RightInner>
          </Right>
        </EventBodyWrapper>

        <DiscourseComments style={{ width: '600px' }} />
      </Container>
    );
  }
}

export default EventPage;

const Backlink = styled(Link)`
  ${tw`text-xs font-bold uppercase hover:underline`}
`;
const EventBodyWrapper = styled.div`
  ${tw`flex flex-wrap`}
`;
const HeaderWrapper = styled(EventHeader)`
  ${tw`w-full`}
`;
const Left = styled.div`
  ${tw`w-full md:w-3/5`}
`;
const Right = styled.div`
  ${tw`w-full md:w-2/5`}
`;
const RightInner = styled.div`
  ${tw`ml-4`}
`;
