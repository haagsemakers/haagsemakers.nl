import React, { Component } from 'react'
import styled from 'styled-components'
import tw from 'tailwind.macro'
import Organizer from './EventOrganizer'

class EventOrganizers extends Component {

  render() {

    const { organizers } = this.props;

    if (!organizers) { return false; }

    return (
      <OrganizersWrapper>
        <Label>Georganiseerd door: </Label>
        { organizers.map((organizer, key) =>
          <Organizer organizer={organizer} key={key}></Organizer>
        )}
      </OrganizersWrapper>
    )
  }
}

// Styles
const OrganizersWrapper = styled.div`${tw`py-8 text-sm`}`
const Label = styled.div`${tw`font-bold`}`

export default EventOrganizers
