import React, { Component } from 'react';
import EventDate from './EventDate';
import EventVenue from './EventVenue';
import styled from 'styled-components'
import tw from 'tailwind.macro'
import EventOrganizers from './EventOrganizers'
import EventLink from './EventLink' 
import EventLocations from './EventLocations'

class EventDetails extends Component {

  render() {
    const { event: {frontmatter }} = this.props;

    return (
      <Wrapper>
        <EventDate start={ frontmatter.start } end={ frontmatter.end } />
        <EventOrganizers organizers={ frontmatter.organizers } />
        <EventVenue venue={ frontmatter.venue } />
        <EventLocations locations={frontmatter.locations} />
        <EventLink url={ frontmatter.event_url } />
      </Wrapper>
    )
  }
}

export default EventDetails

const Wrapper = styled.div`${tw`px-2`}`
