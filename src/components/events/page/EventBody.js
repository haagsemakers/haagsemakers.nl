import React, { Component } from 'react';
import styled from 'styled-components';
import Img from 'gatsby-image';
import tw from 'tailwind.macro';

class EventBody extends Component {

  render() {

    const { event } = this.props;

    return (
      <BodyStyle>

        {event.frontmatter.featuredImage && (
          <Img fluid={event.frontmatter.featuredImage.childImageSharp.fluid} alt={event.frontmatter.featuredImage.name} />
        )}

        <InnerStyle dangerouslySetInnerHTML={{ __html: event.html }} />

      </BodyStyle>
    )
  }
}
const BodyStyle = styled.div``
const InnerStyle = styled.div` ${tw`mt-4 text-base`}`

export default EventBody
