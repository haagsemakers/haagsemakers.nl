import React from 'react';
import { Link } from 'gatsby';
import moment from 'moment';
import styled from 'styled-components';
import tw from 'tailwind.macro';
import { CalendarAlt } from '@styled-icons/fa-solid';

class UpcomingEvents extends React.Component {
  formatDate = (date) => {
    return moment(date).format('D MMMM YYYY kk:mm');
  };

  render() {
    const { events } = this.props;
    if (!events) {
      return null;
    }

    return (
      <EventsWrapper>
        <h3>Makers Agenda</h3>
        <EventsContainer>
          {events.edges.map(({ node }) => (
            <EventItem key={node.id}>
              <EventItemInner to={node.frontmatter.path}>
                {node.frontmatter.featuredImage && (
                  <ImgWrapper
                    src={
                      node.frontmatter.featuredImage.childImageSharp.resize.src
                    }
                    alt={node.frontmatter.title}
                  />
                )}
                <DetailsWrapper>
                  <EventDate>
                    <CalendarAltIcon />
                    {this.formatDate(node.frontmatter.start)}
                  </EventDate>
                  <EventItemTitle>{node.frontmatter.title}</EventItemTitle>
                </DetailsWrapper>
              </EventItemInner>
            </EventItem>
          ))}
        </EventsContainer>
        <ButtonWrapper>
          <Button href="/agenda">
            Bekijk de volledige makers agenda &rarr;
          </Button>
        </ButtonWrapper>
      </EventsWrapper>
    );
  }
}

export default UpcomingEvents;

// Component styling
const EventsWrapper = styled.section`
  ${tw`py-16`}
`;
const EventsContainer = styled.div`
  ${tw`flex flex-wrap mb-16`}
`;
const EventItem = styled.div`
  ${tw`relative w-full sm:w-1/2 md:w-1/3 lg:w-1/4 xl:w-1/4`}
`;
const EventItemInner = styled(Link)`
  ${tw`w-full h-full block border border-gray-400 border-solid hover:bg-gray-200`}
`;
const ImgWrapper = styled.img`
  ${tw`w-full `}
`;
const DetailsWrapper = styled.div`
  ${tw`text-black overflow-hidden p-4 text-md`}
`;
const EventDate = styled.div`
  ${tw`text-sm`}
`;
const CalendarAltIcon = styled(CalendarAlt)`
  ${tw`text-gray-600 text-xs w-3 mr-1`}
`;
const EventItemTitle = styled.h4`
  ${tw`font-bold mb-2`}
`;
const ButtonWrapper = styled.div`
  ${tw`text-center`}
`;
const Button = styled(Link)`
  ${tw`border-2 border-solid border-black px-4 py-2 m-2 inline-block cursor-pointer text-black hover:bg-blue-500 hover:border-blue-500 hover:text-white`}
`;
