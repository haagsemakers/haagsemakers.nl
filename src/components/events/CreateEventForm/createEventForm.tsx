import { yupResolver } from '@hookform/resolvers/yup';
import Axios from 'axios';
import React from 'react';
import DatePicker from 'react-datepicker';
// import { createEventService } from '../../../../_services/event.service';
import 'react-datepicker/dist/react-datepicker.css';
import { Controller, useForm } from 'react-hook-form';
import { toast } from 'react-toastify';
import * as yup from 'yup';

interface IFormInputs {
  title: string;
  description: string;
  startdate: Date;
  enddate: Date;
}

const schema = yup.object().shape({
  title: yup.string().required('Please enter a title'),
  description: yup.string().required('Please enter a description'),
  startdate: yup.date().required('Please enter a start date'),
  enddate: yup.date().required('Please enter an end dates'),
});

export const CreateEventForm: React.FC = () => {
  const { register, handleSubmit, errors, control } = useForm<IFormInputs>({
    mode: 'onChange',
    resolver: yupResolver(schema),
  });

  const createEvent = async (data: any) => {
    console.log('Create a new event', data);
    try {
      const r = await Axios({
        method: 'post',
        url: process.env.NEXT_PUBLIC_API_URL + '/v1/event',
        data,
      });
      console.log(r);
      toast('Event created');
    } catch (err) {
      // Error
      if (err.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        console.log(err.response.status);
        if (err.response.status === 500) {
          toast('There was an error at the server (500)');
        }
        if (err.response.status === 400) {
          if (err.response.data && err.response.data.message) {
            toast('Error creating event: ' + err.response.data.message.join(', '));
          }
        }
        // console.log(err.response.data.message);
        // console.log(error.response.status);
        // console.log(error.response.headers);
      } else if (err.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the
        // browser and an instance of
        // http.ClientRequest in node.js
        console.log(err.request);
      } else {
        // Something happened in setting up the request that triggered an Error
        console.log('Error', err.message);
      }
      // console.log(err.config);
    }
  };

  return (
    <div className="overflow-hidden px-3 py-10 mb-16 flex justify-center">
      <div className="w-full max-w-md">
        <h1 className="text-2xl font-bold mb-4">
          Voeg een nieuwe activiteit toe aan de Haagse Makers community agenda:
        </h1>
        <form className="bg-gray-200 shadow-md rounded px-8 pt-6 pb-8 mb-4" onSubmit={handleSubmit(createEvent)}>
          <div className="mb-6">
            <label className="" htmlFor="title">
              Titel
            </label>
            <input
              name="title"
              type="text"
              placeholder="Titel van de activiteit"
              autoFocus={true}
              ref={register({ required: true, maxLength: 255 })}
              className={`${errors.title ? 'border-red-500' : ''}`}
            />
            {errors && errors.title && <p className="text-red-500 text-xs italic">{errors.title.message}</p>}
          </div>

          <div className="mb-6">
            <label className="" htmlFor="dateStart">
              Startdatum
            </label>
            <Controller
              render={({ onChange, value }) => (
                <DatePicker
                  className={errors.startdate ? 'border-red-500' : ''}
                  selected={value}
                  showTimeSelect
                  dateFormat="d MMMM yyyy HH:mm"
                  onChange={(d: Date) => onChange(d)}
                />
              )}
              control={control}
              // rules={{ required: true }}
              // valueName="selected"
              name="startdate"
              placeholderText="Select date"
              defaultValue={new Date()}
            />
            {errors.startdate && <p className="text-red-500 text-xs italic">Please add a start date.</p>}
          </div>

          <div className="mb-6">
            <label className="" htmlFor="dateEnd">
              Einddatum
            </label>
            <Controller
              render={({ onChange, value }) => (
                <DatePicker
                  className={`w-full ${errors.enddate ? 'border-red-500' : ''}`}
                  selected={value}
                  showTimeSelect
                  dateFormat="d MMMM yyyy HH:mm"
                  onChange={(d: Date) => onChange(d)}
                />
              )}
              control={control}
              // rules={{ required: true }}
              // valueName="selected"
              name="enddate"
              placeholderText="Select end date"
              defaultValue={new Date().setHours(new Date().getHours() + 4)}
            />
            {errors.enddate && <p className="text-red-500 text-xs italic">Please add an end date.</p>}
          </div>

          <div className="mb-6">
            <label className="" htmlFor="description">
              Beschrijving
            </label>
            <textarea
              name="description"
              placeholder="Beschrijving van het event"
              ref={register}
              rows={5}
              className={`shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline ${
                errors.description ? 'border-red-500' : ''
              }`}
            />
            {errors.description && <p className="text-red-500 text-xs italic">Please add a description.</p>}
          </div>
          <div className="mb-6">
            <label className="" htmlFor="location">
              Locatie
            </label>
            <input
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              id="location"
              type="text"
              placeholder="location"
            />
          </div>
          <div className="mb-6">
            <label className="" htmlFor="location">
              Link
            </label>
            <input
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              id="location"
              type="text"
              placeholder="Link naar meer informatie over het event"
            />
          </div>
          <div className="flex items-center justify-between pt-8">
            <button
              className="bg-primary hover:bg-blue-700 w-full text-white py-2 px-4 rounded text-xl font-bold focus:outline-none focus:shadow-outline"
              type="submit"
            >
              Activiteit toevoegen &rarr;
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};
