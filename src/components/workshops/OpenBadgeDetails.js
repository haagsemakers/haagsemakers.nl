import React from 'react'
import styled from 'styled-components'
import tw from 'tailwind.macro';
import openbadge_logo from './../../../content/images/logos/openbadge_insignia_banner2.png';

export default ({ badgelink }) => {

  if (!badgelink) { return null; }

  return(
    <OpenBadge>
      <Text>Na succesvol afronden van deze workshop ontvang je een certificaat op basis van openbadges</Text>
      <a href="https://badgr.io/issuer/issuers/ufWlxU94TBynX3jwQivfHA/badges/qOTCCRTyQZW6x6scCNtJLg" target="_blank" rel="noopener noreferrer">
        <img src={ openbadge_logo} alt="openbadge"/>
      </a>
    </OpenBadge>
  )
}

const OpenBadge = styled.span `${ tw`pt-4` }`
const Text      = styled.p    `${ tw`text-sm` }`
