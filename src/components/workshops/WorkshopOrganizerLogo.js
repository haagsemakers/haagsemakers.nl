import React from 'react'
import Img from 'gatsby-image'
import styled from 'styled-components'
import tw from 'tailwind.macro'

class WorkshopLocationLogo extends React.Component {

  render() {
    
    const { organizer } = this.props;
    const hasLogo = organizer.logo.childImageSharp; 
    
    if (!hasLogo) { return null; }

    return (
      <LogoContainer>
        <Logo 
          style={{ maxHeight: "100%" }}
          imgStyle={{ 
            objectFit: "contain",
            objectPosition: "center left"
          }}
          fluid={organizer.logo.childImageSharp.fluid} alt={organizer.name} />
      </LogoContainer>
    )
  }
}
const LogoContainer = styled.div`${tw`h-12 text-left`}`
const Logo = styled(Img)`${tw``}`

export default WorkshopLocationLogo;
