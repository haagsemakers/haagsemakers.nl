import React from 'react'
import styled from 'styled-components'
import tw from 'tailwind.macro'
import Organizers from './WorkshopOrganizers';
import Locations from './WorkshopLocations';
import WorkshopEvents from './WorkshopEvents';
import OpenBadgeDetails from './OpenBadgeDetails';

export default ({ frontmatter: {organizers, locations, events, openbadge_link, tickets} }) => {

  return (
    <Wrapper>
      <Organizers organizers={ organizers } />
      <Locations locations={ locations } />

      <WorkshopEvents events={ events } />

      <OpenBadgeDetails badgelink={ openbadge_link} />
    </Wrapper>
  )
}

const Wrapper = styled.div`${tw`pl-4`}`
