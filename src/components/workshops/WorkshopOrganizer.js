import React from 'react'
import styled from 'styled-components'
import tw from 'tailwind.macro';
import WorkshopOrganizerLogo from './WorkshopOrganizerLogo'

class WorkshopOrganizer extends React.Component {

  render() {
    const {organizer} = this.props;

    if (!organizer) { return null; }

    if (organizer.logo) {
      return (
        <WorkshopOrganizerLogo organizer={organizer} />
      )
    }
    return (
      <Organizer>{ organizer.name }</Organizer>
    )
  }
}

// Component styles
const Organizer = styled.span`${tw`font-bold inline-block`}`

export default WorkshopOrganizer; 