import React from 'react'
import styled from 'styled-components'
import WorkshopEvent from './WorkshopEvent';
import tw from 'tailwind.macro';
import { isAuthenticated } from '../../utils/auth.service';
import { getEvent, getEventOrder } from '../../utils/event.service';

export default class WorkshopEvents extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isAuthenticated: isAuthenticated(),
      event_ids: props.events,
      events: []
    }
  }

  // Get all events from the api
  fetchEvents() {

    // Fetch event information.
    this.state.event_ids.map( async(event_id) => {
      try {
        // Get the event data
        const eventResult = await getEvent(parseInt(event_id));
        // Add to state if there is a result
        if (eventResult.data) {
          // Create a copy of current state
          let events = this.state.events.slice();
          // Get event data
          let e = eventResult.data;
          // Get order status if user is authenticated
          if (this.state.isAuthenticated) {
            const order = await getEventOrder(parseInt(event_id));
            // Add the order to the event if exists
            if (order && order.data) {
              console.log('Order data: ', order.data);
              e.order = order.data;
            }
          }
          // Add the event to the events list
          events.push(e);
          // Set the new state
          this.setState({ events: events });
        }
      } catch(err) {
        console.warn(err);
      }
    });
  }

  componentDidMount() {
    // Fetch events from the api if available
    if (this.state.event_ids) {
      this.fetchEvents();
    }
  }

  render() {

    if (!this.state.events) { return null; }

    return (
      <Wrapper>
        <Heading>Data: </Heading>
        <EventList>
          { this.state.events.map((event, key) =>
            <WorkshopEvent event={ event } key={ key } />
          )}
        </EventList>
      </ Wrapper>
    )
  }
}

// Component styles
const Wrapper   = styled.div` ${ tw`mb-4 pb-4`}`
const Heading   = styled.div` ${ tw`font-bold pb-2`}`
const EventList = styled.div` ${ tw``}`
