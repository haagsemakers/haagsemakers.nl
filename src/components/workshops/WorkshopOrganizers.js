import React from 'react';
import styled from 'styled-components';
import Organizer from './WorkshopOrganizer';
import tw from 'tailwind.macro';

export default ({ organizers }) => {

  if (!organizers) { return null; }

  return (
    <Wrapper>
      <First>Aangeboden door: </First>
      <Second>
        { organizers.map((organizer, key) =>
          <Organizer organizer={ organizer } key={ key } />
        )}
      </Second>
    </Wrapper>
  )
}

const Wrapper = styled.div` ${ tw`pb-4` }`
const First   = styled.div` ${ tw`font-bold pb-2`}`
const Second  = styled.div` ${ tw``}`
