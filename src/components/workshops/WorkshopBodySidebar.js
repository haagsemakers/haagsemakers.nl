import React from 'react';
import styled from 'styled-components';
import tw from 'tailwind.macro';
import WorkshopDetails from './WorkshopDetails';

export default ({ frontmatter }) => {

  return (
    <SidebarWrapper>
      <WorkshopDetails frontmatter={ frontmatter } />
    </SidebarWrapper>
  )
}

const SidebarWrapper = styled.div`${ tw`w-full md:w-2/5` } `;
