import React from 'react'
import styled from 'styled-components'

export default ({ date }) => {

  return(
    <Date>{ date }</Date>
  )
}

// Component styles 
const Date = styled.span`font-size: 14px;`
