import React from 'react'
import styled from 'styled-components'
import tw from 'tailwind.macro'
import WorkshopFeaturedImage from './WorkshopFeaturedImage';

export default ({ workshop }) => {

  return(
    <WorkshopBody>
      {workshop.frontmatter.featuredImage && (
        <WorkshopFeaturedImage imgSrc={ workshop.frontmatter.featuredImage } />
      )}
      <div dangerouslySetInnerHTML={{ __html: workshop.html }} />
    </ WorkshopBody>
  )
}

const WorkshopBody = styled.div` ${ tw`w-full md:w-3/5 text-lg` } `;
