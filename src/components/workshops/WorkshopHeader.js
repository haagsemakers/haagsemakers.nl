import React from 'react';
import styled from 'styled-components';
import { Link } from 'gatsby';
import tw from 'tailwind.macro';

export default ({ frontmatter: { title, excerpt, intro } }) => {
  return (
    <Header>
      <Backlink href="/workshops">&larr; Workshops</Backlink>
      <WorkshopTitle>{title}</WorkshopTitle>
      <WorkshopExcerpt>{excerpt}</WorkshopExcerpt>
      <Intro>{intro}</Intro>
    </Header>
  );
};

// Component styling
const Header = styled.header`
  ${tw`pb-4`}
`;
const Backlink = styled(Link)`
  ${tw`text-blue p-2 text-xs subpixel-antialiased uppercase font-thin`}
`;
const Intro = styled.div`
  ${tw`font-bold text-lg`}
`;
const WorkshopTitle = styled.h1`
  ${tw``}
`;
const WorkshopExcerpt = styled.h2`
  ${tw``}
`;
