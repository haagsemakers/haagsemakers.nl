import React from 'react';
import styled from 'styled-components'
import tw from 'tailwind.macro';
import Container from '../../components/shared/Container'
import WorkshopHeader from './WorkshopHeader';
import WorkshopBodyMain from './WorkshopBodyMain';
import WorkshopBodySidebar from './WorkshopBodySidebar';
import DiscourseComments from './../../components/shared/DiscourseComments';

export default ({ workshop }) => {

  return (
    <Container>
      <WorkshopHeader frontmatter={ workshop.frontmatter } />
      <Wrapper>
        <WorkshopBodyMain workshop={ workshop } />
        <WorkshopBodySidebar frontmatter={ workshop.frontmatter } />
      </Wrapper>
      <DiscourseComments />
    </Container>
  )
}

const Wrapper = styled.div`${tw`flex flex-wrap`}`;
