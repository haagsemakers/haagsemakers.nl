import React from 'react'
import styled from 'styled-components'
import Location from './WorkshopLocation';
import tw from 'tailwind.macro';

// Locations Component
const Locations = (props) => {

  const { locations } = props;
  if (!locations) { return null; }

  return (
    <Wrapper>
      <First>Locatie(s): </First>
      <Second>
        { locations.map((location, key) =>
          <Location location={location} key={key} />
        )}
      </Second>
    </ Wrapper>
  )
}

export default Locations;

const Wrapper = styled.div` ${ tw`mb-4 pb-4`}`
const First   = styled.div` ${ tw`font-bold pb-2`}`
const Second  = styled.div` ${ tw``}`
