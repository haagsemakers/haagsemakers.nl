import React from 'react';
import Img from 'gatsby-image';

export default ({ imgSrc: { childImageSharp: { fluid }}}) => {

  if (!fluid) { return null; }
  return (
    <Img fluid={ fluid } />
  )
}
