import React from 'react';
import { Link } from 'gatsby';
import styled from 'styled-components';
import tw from 'tailwind.macro';
import moment from 'moment';
import PropTypes from 'prop-types';
import TicketsListing from '../tickets/TicketsListing';

class WorkshopEvent extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      showTicketForm: false,
    };

    this.showTicketForm = this.showTicketForm.bind(this);
  }

  formatDate = (date) => {
    return moment(date).format('D MMMM YYYY - kk:mm');
  };

  showTicketForm = () => {
    this.setState({ showTicketForm: true });
  };
  closeTicketForm = () => {
    this.setState({ showTicketForm: false });
  };

  render() {
    return (
      <EventWrapper>
        <EventDate>{this.formatDate(this.props.event.start)}</EventDate>

        {!this.props.event.order && (
          <Button onClick={this.showTicketForm}>Aanmelden & Details</Button>
        )}

        {this.props.event.order && (
          <OrderedNotice>
            Je hebt een ticket voor dit event! Ga naar je{' '}
            <Link href="/settings/orders">ticketoverzicht</Link>
          </OrderedNotice>
        )}

        <TicketsAvailable>
          Nog{' '}
          <em>
            <strong>{this.props.event.tickets_available}</strong>
          </em>{' '}
          plaatsen beschikbaar
        </TicketsAvailable>

        <TicketsListing
          show={this.state.showTicketForm}
          handleClose={this.closeTicketForm}
          event={this.props.event}
        />
      </EventWrapper>
    );
  }
}

WorkshopEvent.propTypes = {
  event: PropTypes.object.isRequired,
};

export default WorkshopEvent;

// Component Styles
const EventWrapper = styled.div`
  ${tw`border-solid border-gray-400 border p-2`}
`;
const EventDate = styled.p`
  ${tw`font-bold text-lg`}
`;
const TicketsAvailable = styled.p`
  ${tw`text-md italic text-gray-600`}
`;
const Button = styled.div`
  ${tw`block px-2 py-4 mb-4 text-center bg-white text-black border-2 border-solid border-black cursor-pointer hover:bg-gray-400`}
`;
const OrderedNotice = styled.p`
  ${tw`text-md`}
`;
