import React from 'react'
import styled from 'styled-components'
import tw from 'tailwind.macro'
import WorkshopLocationLogo from './WorkshopLocationLogo'

class WorkshopLocation extends React.Component {

  render() {
    const { location } = this.props;

    if (!location) { return null; }

    if (location.logo) {
      return (
        <WorkshopLocationLogo location={location} />
      )
    }
    return (
      <Location>{ location.name }</Location>
    )
  }
}

const Location = styled.div`${tw``}`

export default WorkshopLocation;