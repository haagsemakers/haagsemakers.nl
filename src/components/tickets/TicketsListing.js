import React from 'react';
import styled from 'styled-components'
import tw from 'tailwind.macro';
import { isBrowser, isAuthenticated } from '../../utils/auth.service';
import TicketOrderStatus from './TicketOrderStatus';
import TicketList from './TicketList';

class TicketsListing extends React.Component {

  constructor(props) {

    super(props);

    this.state = {
      show: props.show,
      isAuthenticated: isAuthenticated(),
      tickets: null,
      order: null,
      modalConfig: {
        active: false,
        type: null,
        title: null,
        content: null
      }
    };
  }

  // Show the form
  show() {
    this.setState({ active: true })
  }
  // Hide the form
  hide() {
    this.setState({ active: false })
  }

  //
  render() {
    if (!isBrowser) { return null; }
    const  { show, handleClose, event } = this.props;
    if (!show) { return null; }

    return (
      <TicketsModal show={ show }>
        <h1>Tickets</h1>
        <h2>{ event.name }</h2>
        <p>{ event.description }</p>
        <TicketOrderStatus order={ event.order } />
        <TicketList event={ event } tickets={ event.tickets } />
        {/* Attendees */}
        <CloseBtn onClick={ handleClose }>
          <CloseSVG role="button" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><title>Close</title><path d="M14.348 14.849a1.2 1.2 0 0 1-1.697 0L10 11.819l-2.651 3.029a1.2 1.2 0 1 1-1.697-1.697l2.758-3.15-2.759-3.152a1.2 1.2 0 1 1 1.697-1.697L10 8.183l2.651-3.031a1.2 1.2 0 1 1 1.697 1.697l-2.758 3.152 2.758 3.15a1.2 1.2 0 0 1 0 1.698z"/></CloseSVG>
        </CloseBtn>
        <BottomCloseBtn onClick={ handleClose }>[x] Sluiten</BottomCloseBtn>
      </TicketsModal>
    )
  }
}

export default TicketsListing;

const TicketsModal = styled.div`
  ${tw`fixed inset-0 bg-white p-4 md:p-16 overflow-y-auto`};
  display: ${props => (props.show ? 'block' : 'none')};
`
const BottomCloseBtn = styled.a` ${ tw`block text-black p-4 mt-12 text-center cursor-pointer hover:underline`}`
const CloseBtn = styled.span`${tw`absolute top-0 right-0 p-4 hover:cursor-pointer`}`
const CloseSVG = styled.svg` ${tw`h-12 w-12 fill-current text-grey hover:text-grey-darkest`}`
