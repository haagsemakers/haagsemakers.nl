import React from 'react';
import styled from 'styled-components'
import tw from 'tailwind.macro';

// Show notice if user has a ticket for the event.
export default ({order}) => {
  
  if (!order) { return null; }

  return (
    <Wrapper>
      <Notice>Je hebt een ticket voor dit event!</Notice>
    </Wrapper>
  )
}

// Component Styles
const Wrapper = styled.div`${ tw`` }`
const Notice  = styled.p`${ tw`` }`
