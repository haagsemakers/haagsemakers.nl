import React from 'react';
import styled from 'styled-components';
import tw from 'tailwind.macro';

class ModalResultCancel extends React.Component {

  render() {
    return (
      <Wrapper show={ this.props.show}>
        <Backdrop>
          <Dialog>
            <h3>Je bestelling is geannuleerd. </h3>
            <p>Je ontvangt hierover een bericht per email.</p>
            <ActionsWrapper>
              <ActionsLeft>

              </ActionsLeft>
              <ActionsRight>
                <ConfirmBtn onClick={ this.props.confirm }>OK</ConfirmBtn>
              </ActionsRight>
            </ActionsWrapper>
          </Dialog>
          <CloseBtn onClick={ this.close }>
            <CloseSVG role="button" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><title>Close</title><path d="M14.348 14.849a1.2 1.2 0 0 1-1.697 0L10 11.819l-2.651 3.029a1.2 1.2 0 1 1-1.697-1.697l2.758-3.15-2.759-3.152a1.2 1.2 0 1 1 1.697-1.697L10 8.183l2.651-3.031a1.2 1.2 0 1 1 1.697 1.697l-2.758 3.152 2.758 3.15a1.2 1.2 0 0 1 0 1.698z"/></CloseSVG>
          </CloseBtn>
        </Backdrop>
      </Wrapper>
    )
  }
}

export default ModalResultCancel;

// Styles
const Wrapper = styled.div`display: ${props => (props.show ? 'flex' : 'none')};`
const Backdrop = styled.div`${ tw`fixed inset-0 z-50 overflow-auto bg-smoke-darker flex` }`
const Dialog   = styled.div` ${ tw`relative p-8 bg-white w-full max-w-md m-auto flex-col flex` }`
const CloseBtn = styled.span`${tw`absolute top-0 right-0 p-4 hover:cursor-pointer`}`
const CloseSVG = styled.svg` ${tw`h-12 w-12 fill-current text-grey hover:text-grey-darkest`}`
const ActionsWrapper = styled.div` ${ tw`flex pt-8` }`
const ActionsLeft = styled.div` ${tw`w-1/2`}`
const ActionsRight = styled.div` ${ tw`w-1/2 text-right` }`
const ConfirmBtn = styled.button` ${ tw`bg-white mb-2 text-black p-4 text-center cursor-pointer uppercase font-bold border-2 border-black border-solid hover:no-underline hover:bg-gray-400 hover:border-solid hover:text-black` }`
