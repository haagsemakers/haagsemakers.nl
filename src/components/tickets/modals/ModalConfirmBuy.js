import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import tw from 'tailwind.macro';

class ModalConfirmBuy extends React.Component {

  // constructor(props) {
  //   super(props);
  // }


  render() {
    console.log('[ ModalConfirmBuy ] props', this.props);

    const { show, event, ticket} = this.props;
    if (!event || !show || !ticket) { return null; }

    let total =  {
      cost: (ticket.cost/100) - (ticket.cost/100)*0.21,
      fee: (ticket.fee/100) - (ticket.fee/100)*0.21,
      btw: (ticket.cost*0.21 + ticket.fee*0.21)/100,
    }
    total.totalEx = total.cost + total.fee;
    total.totalIn = total.totalEx + total.btw;

    return (
      <Wrapper show={ show }>
        <Backdrop>
          <Dialog>
            <h3>Weet je zeker dat je dit ticket wilt bestellen?</h3>
            <h4>{ event.name } </h4>
            <div style={{textAlign: 'right'}}>
              Ticketprijs: € { total.cost.toFixed(2) } <br/>
              Boekingskosten: € { total.fee.toFixed(2) } <br/>
              <hr />
              Totaal (excl. BTW): € {total.totalEx.toFixed(2) }<br/>
              BTW (21%): € {total.btw.toFixed(2) }<br/>
              <strong>TOTAAL (incl. BTW): € {total.totalIn.toFixed(2) }</strong><br/>
            </div>
            <ActionsWrapper>
              <ActionsLeft>
                <CancelBtn onClick={ this.props.cancel }>Annuleren</CancelBtn>
              </ActionsLeft>
              <ActionsRight>
                <ConfirmBtn onClick={ this.props.confirm }>Plaats bestelling</ConfirmBtn>
              </ActionsRight>
            </ActionsWrapper>
          </Dialog>
          <CloseBtn onClick={ this.close }>
            <CloseSVG role="button" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><title>Close</title><path d="M14.348 14.849a1.2 1.2 0 0 1-1.697 0L10 11.819l-2.651 3.029a1.2 1.2 0 1 1-1.697-1.697l2.758-3.15-2.759-3.152a1.2 1.2 0 1 1 1.697-1.697L10 8.183l2.651-3.031a1.2 1.2 0 1 1 1.697 1.697l-2.758 3.152 2.758 3.15a1.2 1.2 0 0 1 0 1.698z"/></CloseSVG>
          </CloseBtn>
        </Backdrop>
      </Wrapper>
    )
  }
}

ModalConfirmBuy.propTypes = {
  show: PropTypes.bool,
  confirm: PropTypes.func,
  close: PropTypes.func,
  event: PropTypes.object,
  ticket: PropTypes.object
}

export default ModalConfirmBuy;

// Styles
const Wrapper = styled.div`display: ${props => (props.show ? 'flex' : 'none')};`
const Backdrop = styled.div`${ tw`fixed inset-0 z-50 overflow-auto bg-smoke-darker flex` }`
const Dialog   = styled.div` ${ tw`relative p-8 bg-white w-full max-w-md m-auto flex-col flex` }`
const CloseBtn = styled.span`${tw`absolute top-0 right-0 p-4 hover:cursor-pointer`}`
const CloseSVG = styled.svg` ${tw`h-12 w-12 fill-current text-grey hover:text-grey-darkest`}`
const ActionsWrapper = styled.div` ${ tw`flex pt-8` }`
const ActionsLeft = styled.div` ${tw`w-1/2`}`
const CancelBtn = styled.button` ${ tw`border-0 p-4 cursor-pointer hover:underline` }`
const ActionsRight = styled.div` ${ tw`w-1/2 text-right` }`
const ConfirmBtn = styled.button` ${ tw`bg-white mb-2 text-black p-4 text-center cursor-pointer uppercase font-bold border-2 border-black border-solid hover:no-underline hover:bg-gray-400 hover:border-solid hover:text-black` }`
