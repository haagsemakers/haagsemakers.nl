import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import tw from 'tailwind.macro';
import moment from 'moment';

class ModalOrderOverview extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      show: false
    }
  }

  componentDidMount() {

  }

  toggleShow = () => {
    this.setState({
      show: !this.state.show
    })
  }

  render() {

    if (!this.state.show) {
      return <button onClick={this.toggleShow}>Details</button>
    }

    const { order } = this.props;

    return (
      <Wrapper show={this.state.show}>
        <Backdrop>
          <Dialog>

            <h3>Details van je bestelling</h3>
            <h4>Event</h4>
            { order.event.name }<br />
            { moment(order.event.start).format('D MMMM YYYY - kk:mm') }<br />
            Order status: { order.status }<br />
            <h4>Ticket</h4>
            { order.ticket.name }<br />
            € { ((order.ticket.cost/100) + (order.ticket.fee/100)).toFixed(2)}
            <h4>Betaling</h4>
            { order.payments[ 0].status }<br />
            <ActionsWrapper>
              <ActionsLeft>

              </ActionsLeft>
              <ActionsRight>
                <ConfirmBtn onClick={ this.toggleShow }>Sluiten</ConfirmBtn>
              </ActionsRight>
            </ActionsWrapper>
          </Dialog>
          <CloseBtn onClick={ this.toggleShow }>
            <CloseSVG role="button" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><title>Close</title><path d="M14.348 14.849a1.2 1.2 0 0 1-1.697 0L10 11.819l-2.651 3.029a1.2 1.2 0 1 1-1.697-1.697l2.758-3.15-2.759-3.152a1.2 1.2 0 1 1 1.697-1.697L10 8.183l2.651-3.031a1.2 1.2 0 1 1 1.697 1.697l-2.758 3.152 2.758 3.15a1.2 1.2 0 0 1 0 1.698z"/></CloseSVG>
          </CloseBtn>
        </Backdrop>
      </Wrapper>
    )
  }
}

ModalOrderOverview.propTypes = {
  order: PropTypes.object,
  ticket: PropTypes.object
}

export default ModalOrderOverview;

// Styles
const Wrapper = styled.div`display: ${props => (props.show ? 'flex' : 'none')};`
const Backdrop = styled.div`${ tw`fixed inset-0 z-50 overflow-auto bg-smoke-darker flex` }`
const Dialog   = styled.div` ${ tw`relative p-8 bg-white w-full max-w-md m-auto flex-col flex` }`
const CloseBtn = styled.span`${tw`absolute top-0 right-0 p-4 hover:cursor-pointer`}`
const CloseSVG = styled.svg` ${tw`h-12 w-12 fill-current text-grey hover:text-grey-darkest`}`
const ActionsWrapper = styled.div` ${ tw`flex pt-8` }`
const ActionsLeft = styled.div` ${tw`w-1/2`}`
const ActionsRight = styled.div` ${ tw`w-1/2 text-right` }`
const ConfirmBtn = styled.button` ${ tw`bg-white mb-2 text-black p-4 text-center cursor-pointer uppercase font-bold border-2 border-black border-solid hover:no-underline hover:bg-gray-400 hover:border-solid hover:text-black` }`
