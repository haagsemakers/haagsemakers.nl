import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import tw from 'tailwind.macro';
import TicketListItem from './TicketListItem';

class TicketList extends React.Component {

  // constructor(props) {
  //   super(props);
  // }

  render() {
    // No tickets available
    const { tickets, event } = this.props;

    if (!tickets || tickets.length<1) {
      return (
        <EmptyTickets>Er zijn nog geen tickets beschikbaar voor dit event. </EmptyTickets>
      )
    }

    return (
      <Wrapper>
        { tickets.map((ticket, itemKey) => (
          <TicketListItem ticket={ ticket } event={ event } key={ itemKey } />
        ))}
      </Wrapper>
    )
  }
}

TicketList.propTypes = {
  tickets: PropTypes.array,
  event: PropTypes.object
}

export default TicketList;

// Component Styles
const Wrapper = styled.div`${tw`my-12`}`
const EmptyTickets = styled.div`${ tw`text-lg text-center font-bold bg-gray-100 p-32`}`
