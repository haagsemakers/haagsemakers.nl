import React from 'react';
import styled from 'styled-components'
import tw from 'tailwind.macro';
import TicketBuyButton from './TicketBuyButton';

export default ({ event, ticket, itemKey }) => {

  if (!ticket) { return null; }

  return (
    <Wrapper key={ itemKey }>
      <DetailsWrapper>
        <strong>{ ticket.name }</strong>
        <Available>Nog { ticket.tickets_available } tickets beschikbaar</Available>
      </DetailsWrapper>
      <CostsWrapper>
        <Amount>€ { (ticket.cost/100) + (ticket.fee/100) },- </Amount>
        <CostsDetails>(incl. BTW & boekingskosten)</CostsDetails>
      </CostsWrapper>
      <OrderWrapper>
        <TicketBuyButton ticket={ ticket } event={ event }/>
      </OrderWrapper>
    </Wrapper>
  )
}

// Component Styles
const Wrapper        = styled.div`${ tw`border rounded border-solid border-gray-400 mb-4 p-4 flex flex-wrap` }`
const DetailsWrapper = styled.div`${ tw`w-full md:w-1/3` }`
const Available      = styled.div`${ tw`text-sm text-gray-600 mt-4`}`
const CostsWrapper   = styled.div`${ tw`w-full md:w-1/3 mb-4` }`
const Amount         = styled.div`${ tw`text-xl mt-8 pb-8 md:mt-0 md:text-center font-bold` }`
const CostsDetails   = styled.div`${ tw`text-sm md:text-center text-gray-600`}`
const OrderWrapper   = styled.div`${ tw`w-full md:w-1/3` }`
