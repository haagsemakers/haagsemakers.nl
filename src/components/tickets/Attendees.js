import React, { Component } from 'react'
import { isAuthenticated } from './../../utils/auth.service'
import ApiService from './../../utils/api.service'
import styled from 'styled-components'

class EventAttendees extends Component {

  constructor(props) {
    super(props);

    this.state = {
      isAuthenticated: isAuthenticated(),
      attendees: []
    };
  }

  componentDidMount() {

    // Don't get attendees is user is not logged in
    if (!this.state.isAuthenticated) { return; }
    // Don't fetch attendees if there are no tickets available
    if ((!this.props.event.tickets) || (this.props.event.tickets.length <1)) { return; }

    // Fetch attendees from API
    ApiService.getEventAttendees(this.props.event.internalId).then(result => {
      this.setState({ attendees: result })
    }).catch(err => {
      console.warn(err);
    });
  }

  render() {

    const ContainerStyle = styled.div`
      font-size: 14px;
      padding: 15px 0;
    `

    // Show attendees list
    if (this.state.attendees.length >0) {

      return (
        <ContainerStyle>
          <p>Deze mensen zijn aanwezig: </p>
          {this.state.attendees.map((attendee, key) =>
            <EventAttendeeItem attendee={attendee} key={key} />
          )}
        </ContainerStyle>
      )
    } else {
      return null;
    }
  }
}

export default EventAttendees


export class EventAttendeeItem extends Component {

  render() {
    const { key, attendee } = this.props;

    const Item = styled.div`
      display: inline-block;
    `

    const Avatar = styled.img`
      height: 50px;
      width: 50px;
      borderRadius: '50%';
    `

    return(
      <Item key={ key }>
        <Avatar src={ attendee.picture } alt={ attendee.name }></Avatar>
        <div>{attendee.nickname}</div>
      </Item>
    )
  }
}
