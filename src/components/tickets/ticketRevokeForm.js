//
handleUnregister() {
  const url = `${process.env.GATSBY_HAAGSEMAKERS_API_URL}/order/${this.state.order.id}`;
  const bearer = 'Bearer ' + getAccessToken();
  fetch(url, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': bearer
    },
    body: JSON.stringify({ status: 'canceled' })
  }).then(res => res.json()).then(data => {
    this.setState({
      order: data,
      modalConfig: {
        active: 'is-active',
        content: 'You unregistered successfully!',
        title: 'Unregister succesful'

      }
    });
  }).catch(err => {
    console.warn('unregister error', err);
    this.setState({
      modalConfig: {
        active: 'is-active',
        title: 'There was an error unregistering for this event.',
        content: 'Error'
      }
    });
  })
}
