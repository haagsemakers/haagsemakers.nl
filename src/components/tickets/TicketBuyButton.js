import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import tw from 'tailwind.macro';
import { Link } from 'gatsby';
import { placeOrder } from '../../utils/event.service';
import { isAuthenticated } from '../../utils/auth.service';
import { checkPaymentStatus } from '../../utils/api.service';
import ModalConfirmBuy from './modals/ModalConfirmBuy';

class TicketBuyButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isAuthenticated: isAuthenticated(),
      isLoading: false,
      showConfirmModal: false,
    };
  }

  // Show confirm modal after buy button click
  handleBuyBtnClicked = () => {
    console.log('handleBuyBtnClicked');
    this.setState({
      isLoading: true,
      showConfirmModal: true,
    });
  };

  // Hide the confirm buy modal
  handleModalCancel = () => {
    this.setState({
      isLoading: false,
      showConfirmModal: false,
      errorMsg: null,
    });
  };

  // Confirm the buy
  handleModalConfirm = () => {
    console.log('confirm');
    this.setState({
      showConfirmModal: false,
    });
    if (this.props.payment && this.props.payment.status === 'open') {
      console.log(
        `[ TicketBuyButton ] Payment status is ${this.props.payment.status}, to resumePayment. `
      );
      this.resumePayment();
    } else {
      console.log(
        `[ TicketBuyButton ] No active payment, creating a new payment. `
      );
      this.newOrder();
    }
  };

  // Initiate the order process
  newOrder = () => {
    console.log('[ TicketBuyButton ] newOrder for ticket: ', this.props.ticket);
    this.setState({
      isLoading: true,
    });
    placeOrder(this.props.ticket.id)
      .then((result) => {
        this.setState({ isLoading: false });
        const { paymentUrl } = result.data.payment;
        if (paymentUrl) {
          window.location.replace(paymentUrl);
        }
      })
      .catch((err) => {
        this.setState({
          isLoading: false,
          errorMsg: err.response.data.message,
        });
      });
  };

  // Continue the payment process
  resumePayment = async () => {
    console.log(
      '[ TicketBuyButton ] resumePayment',
      this.props.payment,
      this.props.order
    );
    this.setState({ isLoading: true });

    // Get mollie status from API
    console.log(
      '[ TicketBuyButton ] resumePayment: Fetching latest mollie status'
    );
    try {
      // Get status from mollie
      const status = await checkPaymentStatus(this.props.payment.id);
      this.setState({ isLoading: false });
      console.log('[ TicketBuyButton ] mollie status result: ', status.data);
      const { paymentUrl } = status.data;
      if (paymentUrl) {
        window.location.replace(paymentUrl);
      } else {
        console.log('somehting is wrong with this payment. reload');
        this.props.updateStatus();
      }
    } catch (err) {
      this.setState({ isLoading: false });
      console.warn(err);
    }
  };

  render() {
    console.log('[ TicketBuyButton ] props', this.props);

    // Show login button if not authenticated
    if (!this.state.isAuthenticated) {
      return <LoginButton href="/login">Login om te bestellen</LoginButton>;
    }

    // Only show the button for pending orders
    const { ticket, order, payment } = this.props;
    if (!ticket) {
      return null;
    }
    if (order && order.status !== 'pending') {
      return null;
    }

    let buttonText = this.props.buttonText;

    if (payment && payment.status === 'expired') {
      buttonText = 'Opnieuw betalen';
    }
    if (payment && payment.status === 'open') {
      buttonText = 'Betaling afronden';
    }

    return (
      <React.Fragment>
        <Button
          disabled={this.state.isLoading}
          onClick={this.handleBuyBtnClicked}
        >
          {buttonText} <MollieBtn src="/ideal.gif" />
        </Button>
        {this.state.errorMsg && (
          <p style={{ color: 'red' }}>Error: {this.state.errorMsg}</p>
        )}
        <ModalConfirmBuy
          event={this.props.event}
          ticket={this.props.ticket}
          show={this.state.showConfirmModal}
          cancel={this.handleModalCancel}
          confirm={this.handleModalConfirm}
        />
      </React.Fragment>
    );
  }
}

TicketBuyButton.defaultProps = {
  buttonText: 'Bestellen',
};

TicketBuyButton.propTypes = {
  order: PropTypes.object,
  event: PropTypes.object,
  ticket: PropTypes.object.isRequired,
  payment: PropTypes.object,
  buttonText: PropTypes.string,
  updateStatus: PropTypes.func,
};

export default TicketBuyButton;

// Component Styles
const LoginButton = styled(Link)`
  ${tw`bg-black text-white block rounded p-4 text-center cursor-pointer hover:no-underline hover:bg-gray-800`}
`;
const Button = styled.button`
  ${tw`w-full bg-white mb-2 text-black p-4 text-center cursor-pointer uppercase font-bold border-2 border-black border-solid hover:no-underline hover:bg-gray-400 hover:border-solid hover:text-black`}
`;
const MollieBtn = styled.img`
  ${tw`h-4 ml-2 align-text-bottom`}
`;
