import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components'
import tw from 'tailwind.macro';
import { isAuthenticated } from '../../utils/auth.service';
import { revokeOrder } from '../../utils/event.service';
import ModalConfirmCancel from './modals/ModalConfirmCancel';
import ModalResultCancel from './modals/ModalResultCancel';

class TicketCancelOrderBtn extends React.Component{

  constructor(props) {
    super(props);

    this.state = {
      isAuthenticated: isAuthenticated(),
      isLoading: false,
      showConfirmModal: false,
      showResultModal: false
    }
  }

  handleCancelClicked = () => {
    this.setState({
      isLoading: true,
      showConfirmModal: true
    })
  }

  handleModalCancel =() => {
    this.setState({
      isLoading: false,
      showConfirmModal: false
    });
  }

  handleModalCancelConfirm = () => {
    revokeOrder(this.props.order.id)
      .then(result => {
        console.log('ticketcancelorder result', result)
        this.setState({
          isLoading: true,
          showConfirmModal: false,
          showResultModal: true
        });
      })
      .catch(err => {
        console.warn('ticketcancelorder', err)
        this.setState({
          isLoading: false,
          showConfirmModal: false,
          showResultModal: false
        });
      });
  }

  handleModalResultConfirm = () => {
    console.log('handleModalResultConfirm');
    // Fetch the order status at parent;
    this.props.updateStatus();
    this.setState({
      showResultModal: false,
      showConfirmModal: false
    })
  }

  render() {
    console.log('[ TicketCancelOrderBtn ] props', this.props)

    if (!this.state.isAuthenticated) { return null; }
    // Don't show if order is canceled
    if (this.props.order.status === 'canceled') { return null; }

    // Order can be canceled, show cancel button
    return(
      <React.Fragment>
        <Button
          disabled={this.state.isLoading}
          onClick={ this.handleCancelClicked }>{ this.props.buttonText }</Button>
        <ModalConfirmCancel
          show={ this.state.showConfirmModal }
          cancel={ this.handleModalCancel }
          confirm={ this.handleModalCancelConfirm }
        />
        <ModalResultCancel
          show={ this.state.showResultModal }
          confirm={ this.handleModalResultConfirm }
        />
      </React.Fragment>
    )
  }
}

TicketCancelOrderBtn.defaultProps = {
  buttonText: `Bestelling annuleren`
}

TicketCancelOrderBtn.propTypes = {
  order: PropTypes.object.isRequired,
  payment: PropTypes.object,
  updateStatus: PropTypes.func
};

export default TicketCancelOrderBtn;

// Component Styles
const Button = styled.button`${ tw`bg-transparent text-center border-0 cursor-pointer hover:underline` }`
