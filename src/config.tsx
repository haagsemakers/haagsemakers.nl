export default {
  title: 'Haagse Makers',
  author: {
    name: 'Arn van der Pluijm',
    summary: 'maker',
  },
  description: 'Platform voor Haagse makers',
  social: {
    twitter: 'haagsemakers',
  },
};
